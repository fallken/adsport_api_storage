<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/12/2019
 * Time: 3:52 PM
 */
return [
       'ageRange'=>'\\Google\\AdsApi\\AdWords\\v201809\\cm\\AgeRange',
       'language'=>'\\Google\\AdsApi\\AdWords\\v201809\\cm\\Language',
       'platform'=>'\\Google\\AdsApi\\AdWords\\v201809\\cm\\Platform',
       'gender'=>'\\Google\\AdsApi\\AdWords\\v201809\\cm\\Gender',
       'location'=>'\\Google\\AdsApi\\AdWords\\v201809\\cm\\Location'
];