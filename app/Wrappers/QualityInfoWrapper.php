<?php

namespace App\Wrappers;

class QualityInfoWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'qualityScore' => $data->getQualityScore(),
        ];
    }
}
