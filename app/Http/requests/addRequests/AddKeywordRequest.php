<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests\addRequests;


use Anik\Form\FormRequest;

class AddKeywordRequest extends FormRequest
{
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'adGroupId' => 'required',
            'name' => 'required|string',
            'status' => 'required|in:ENABLED,REMOVED,PAUSED',
            'amount' => 'string',
            'matchType' => 'string|in:EXACT,PHRASE,BROAD'
        ];
    }
}
