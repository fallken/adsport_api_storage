<?php

namespace App\Wrappers;

class AdGroupExtensionSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $setting = $data->getExtensionSetting();

        return (object)[
            'adGroupId' => $data->getAdGroupId(),
            'extensionType' => $data->getExtensionType(),
            'extensionSetting' => WrapperBridge::toObject($setting),
        ];
    }
}
