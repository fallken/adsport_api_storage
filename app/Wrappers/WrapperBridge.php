<?php

namespace App\Wrappers;

use App\Exceptions\EndPointRequestException;

use Exception;
use Illuminate\Support\Facades\Log;

class WrapperBridge
{
    /**
     * Base directory for wrappers.
     */
    private const WRAPPER_DIRECTORY = '\\App\\Wrappers\\';

    /**
     * Array of wrappers.
     */
    private const WRAPPERS = [
        'Null' => 'NullWrapper',

        'Google\AdsApi\AdWords\v201809\cm\Ad' => 'AdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Bid' => 'BidWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Label' => 'LabelWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Asset' => 'AssetWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Money' => 'MoneyWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CpcBid' => 'CpcBidWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CpmBid' => 'CpmBidWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Budget' => 'BudgetWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CpaBid' => 'CpaBidWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Address' => 'AddressWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Setting' => 'SettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\UrlList' => 'UrlListWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroup' => 'AdGroupWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Keyword' => 'KeywordWrapper',
        'Google\AdsApi\AdWords\v201809\cm\GeoPoint' => 'GeoPointWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Campaign' => 'CampaignWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Platform' => 'PlatformWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Language' => 'LanguageWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Location' => 'LocationWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroupAd' => 'AdGroupAdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AssetLink' => 'AssetLinkWrapper',
        'Google\AdsApi\AdWords\v201809\cm\Proximity' => 'ProximityWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AppUrlList' => 'AppUrlListWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CallOnlyAd' => 'CallOnlyAdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdSchedule' => 'AdScheduleWrapper',
        'Google\AdsApi\AdWords\v201809\cm\QualityInfo' => 'QualityInfoWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AppFeedItem' => 'AppFeedItemWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CallFeedItem' => 'CallFeedItemWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdStrengthInfo' => 'AdStrengthInfoWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ListOperations' => 'ListOperationsWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ExpandedTextAd' => 'ExpandedTextAdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\NetworkSetting' => 'NetworkSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\UniversalAppAd' => 'UniversalAppAdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\LabelAttribute' => 'LabelAttributeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CustomParameter' => 'CustomParameterWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CalloutFeedItem' => 'CalloutFeedItemWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroupCriterion' => 'AdGroupCriterionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemSchedule' => 'FeedItemScheduleWrapper',
        'Google\AdsApi\AdWords\v201809\cm\PolicyTopicEntry' => 'PolicyTopicEntryWrapper',
        'Google\AdsApi\AdWords\v201809\cm\SitelinkFeedItem' => 'SitelinkFeedItemWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ExtensionSetting' => 'ExtensionSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\TargetingSetting' => 'TargetingSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CustomParameters' => 'CustomParametersWrapper',
        'Google\AdsApi\AdWords\v201809\cm\PolicySummaryInfo' => 'PolicySummaryInfoWrapper',
        'Google\AdsApi\AdWords\v201809\cm\MoneyWithCurrency' => 'MoneyWithCurrencyWrapper',
        'Google\AdsApi\AdWords\v201809\cm\PromotionFeedItem' => 'PromotionFeedItemWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CampaignCriterion' => 'CampaignCriterionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemScheduling' => 'FeedItemSchedulingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ResponsiveSearchAd' => 'ResponsiveSearchAdWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CallConversionType' => 'CallConversionTypeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ManagedCustomerLink' => 'ManagedCustomerLinkWrapper',
        'Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSetting' => 'GeoTargetTypeSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemPolicySummary' => 'FeedItemPolicySummaryWrapper',
        'Google\AdsApi\AdWords\v201809\cm\SelectiveOptimization' => 'SelectiveOptimizationWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroupAdRotationMode' => 'AdGroupAdRotationModeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ManualCpcBiddingScheme' => 'ManualCpcBiddingSchemeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\OperatingSystemVersion' => 'OperatingSystemVersionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroupAdPolicySummary' => 'AdGroupAdPolicySummaryWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemAttributeError' => 'FeedItemAttributeErrorWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemGeoRestriction' => 'FeedItemGeoRestrictionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\TargetingSettingDetail' => 'TargetingSettingDetailWrapper',
        'Google\AdsApi\AdWords\v201809\cm\TargetCpaBiddingScheme' => 'TargetCpaBiddingSchemeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AssetPolicySummaryInfo' => 'AssetPolicySummaryInfoWrapper',
        'Google\AdsApi\AdWords\v201809\cm\DynamicSearchAdsSetting' => 'DynamicSearchAdsSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSetting' => 'AdGroupExtensionSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\BiddableAdGroupCriterion' => 'BiddableAdGroupCriterionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSetting' => 'CampaignExtensionSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemDevicePreference' => 'FeedItemDevicePreferenceWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemAdGroupTargeting' => 'FeedItemAdGroupTargetingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\NegativeAdGroupCriterion' => 'NegativeAdGroupCriterionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\TargetSpendBiddingScheme' => 'TargetSpendBiddingSchemeWrapper',
        'Google\AdsApi\AdWords\v201809\cm\FeedItemCampaignTargeting' => 'FeedItemCampaignTargetingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\NegativeCampaignCriterion' => 'NegativeCampaignCriterionWrapper',
        'Google\AdsApi\AdWords\v201809\cm\UniversalAppCampaignSetting' => 'UniversalAppCampaignSettingWrapper',
        'Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration' => 'BiddingStrategyConfigurationWrapper',
        'Google\AdsApi\AdWords\v201809\cm\CampaignCriterionReturnValue' => 'CampaignCriterionReturnValueWrapper',
        'Google\AdsApi\AdWords\v201809\cm\ConversionOptimizerEligibility' => 'ConversionOptimizerEligibilityWrapper',
        'Google\AdsApi\AdWords\v201809\cm\UniversalAppCampaignAdsPolicyDecisions' => 'UniversalAppCampaignAdsPolicyDecisionsWrapper',
    ];

    /**
     * Get wrapper class of received google ad words api response data.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    private static function getWrapperClass(&$data)
    {
        $className = 'Null';

        if ($data) {
            $className = get_class($data) ?? null;
        }

        try {
            $className = static::WRAPPER_DIRECTORY . static::WRAPPERS[$className];

            return new $className;
        }
        catch (Exception $exception) {

            $message = 'The wrapper helper for ' . $className . ' class not implemented.';

            Log::info("WrapperBridge: " . $message);

            throw new EndPointRequestException($message, 400);
        }
    }

    /**
     * Convert class data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public static function toObject(&$data)
    {
        return static::getWrapperClass($data)->toObject($data);
    }
}
