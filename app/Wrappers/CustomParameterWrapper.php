<?php

namespace App\Wrappers;

class CustomParameterWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'key' => $data->getKey(),
            'value' => $data->getValue(),
            'isRemove' => $data->getIsRemove(),
        ];
    }
}
