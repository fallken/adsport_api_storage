<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/26/2019
 * Time: 3:20 PM
 */

namespace App\interfaces;


interface CurrencyServiceInterface {
    /**
     * @return mixed
     */
    public function getCurrencies();
}