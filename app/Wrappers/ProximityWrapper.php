<?php

namespace App\Wrappers;

class ProximityWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $geoPoint = $data->getGeoPoint();
        $address = $data->getAddress();

        return (object)[
            'radiusInUnits' => $data->getRadiusInUnits(),
            'radiusDistanceUnits' => $data->getRadiusDistanceUnits(),

            'address' => WrapperBridge::toObject($address),
            'geoPoint' => WrapperBridge::toObject($geoPoint),
        ];
    }
}
