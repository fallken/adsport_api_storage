<?php

namespace App\Wrappers;

class ExpandedTextAdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $expandedTextAd = AdWrapper::toObject($data);

        $expandedTextAd->path1 = $data->getPath1();
        $expandedTextAd->path2 = $data->getPath2();
        $expandedTextAd->description = $data->getDescription();
        $expandedTextAd->description2 = $data->getDescription2();
        $expandedTextAd->headlinePart1 = $data->getHeadlinePart1();
        $expandedTextAd->headlinePart2 = $data->getHeadlinePart2();
        $expandedTextAd->headlinePart3 = $data->getHeadlinePart3();

        return $expandedTextAd;
    }
}
