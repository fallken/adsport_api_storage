<?php

namespace App\Wrappers;

trait PolicySummaryInfoTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function append(&$data)
    {
        $entries = [];
        $policyTopicEntries = $data->getPolicyTopicEntries();

        if (is_array($policyTopicEntries)) {

            foreach ($policyTopicEntries as $entry) {

                $entries[] = WrapperBridge::toObject($entry);
            }
        }

        return [
            'policyTopicEntries' => $entries,

            'reviewState' => $data->getReviewState(),
            'denormalizedStatus' => $data->getDenormalizedStatus(),
            'combinedApprovalStatus' => $data->getCombinedApprovalStatus(),
            'policySummaryInfoType' => $data->getPolicySummaryInfoType(),
        ];
    }
}