<?php

namespace App\Wrappers;

trait CriterionTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     */
    public static function append(&$data)
    {
        return [
            'id' => $data->getId(),
            'type' => $data->getType(),
            'criterionType' => $data->getCriterionType(),
        ];
    }
}