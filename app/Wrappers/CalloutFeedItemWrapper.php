<?php

namespace App\Wrappers;

class CalloutFeedItemWrapper implements AdWordsWrapperInterface
{
    use ExtensionFeedItemTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        return (object)array_merge(static::append($data),
            [
                'calloutText' => $data->getCalloutText(),
            ]
        );
    }
}
