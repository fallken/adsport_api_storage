<?php

namespace App\Http\Controllers;

use App\repos\KeywordsRepository;
use App\Http\Interfaces\KeywordsControllerInterface;
use App\Http\requests\setRequests\SetKeywordsSettingRequest;

class KeywordsController extends Controller implements KeywordsControllerInterface
{
    /**
     * @var KeywordsRepository
     */
    protected $keywordsRepository;

    /**
     * AdGroupController constructor.
     */
    public function __construct()
    {
        $this->keywordsRepository = new KeywordsRepository();
    }

    /**
     * Set keyword setting.
     *
     * @param SetKeywordsSettingRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setKeywordsSetting(SetKeywordsSettingRequest $request)
    {
        return $this->keywordsRepository->setKeywordsSetting(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('keywordId'),
            $request->input('operator') ?? '',
            $request->input('matchType') ?? '',
            $request->input('status') ?? '',
            $request->input('finalUrl') ?? '',
            $request->input('bidType') ?? '',
            $request->input('microAmount') ?? ''
        );
    }
}
