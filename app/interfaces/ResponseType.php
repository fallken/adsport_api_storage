<?php

namespace App\Interfaces;

interface ResponseType
{
    const RESPONSE_TYPE_JSON = 'json';
    const RESPONSE_TYPE_PURE = 'pure';
    const RESPONSE_TYPE_INSTANCE = 'instance';
    const RESPONSE_TYPE_OPERATION = 'operation';
}