<?php

use App\repos\AdGroupAdRepository;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdStatus;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;

class AdGroupAdTest extends TestCase
{
    /**
     * @var AdGroupAdRepository
     */
    private $adGroupAdRepository;

    /**
     * AdGroupTest constructor.
     *
     * @param string|null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adGroupAdRepository = resolve(AdGroupADRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetExpandedDynamicSearchAd()
    {
        $sourceCustomer = '261-911-0558';
        $adGroupId = '66844116696';
        $adId = '332591138289';

        $result = $this->adGroupAdRepository->setExpandedDynamicSearchAd(
            $sourceCustomer,
            $adGroupId,
            $adId,
            'Test 1',
            'Test 2'
        );

        $this->assertIsArray($result);

        print_r($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAds()
    {
        $sourceCustomer = '261-911-0558';
        $adGroupId = '66844116696';
        $predicates = [];

        $result = $this->adGroupAdRepository->getAds($sourceCustomer, $adGroupId, $predicates);

        print_r($result);

        $this->assertIsArray($result);

        $predicates = [
            (object)['field' => 'Status', 'operator' => PredicateOperator::IN, 'values' => [AdGroupAdStatus::ENABLED, AdGroupAdStatus::PAUSED]],
        ];

        $result = $this->adGroupAdRepository->getAds($sourceCustomer, $adGroupId, $predicates);

        print_r($result);

        $this->assertIsArray($result);

        $response = $this->post('/get/getAdgroupAds', [
            'customerId' => $sourceCustomer,
            'adgroupId' => $adGroupId,
            'predicates' => $predicates
        ]);

        print_r($response->response->getContent());

        $response->assertResponseOk();

        $response = $this->post('/get/getAdgroupAds', [
            'customerId' => $sourceCustomer,
            'predicates' => $predicates
        ]);

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }
}
