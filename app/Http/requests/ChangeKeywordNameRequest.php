<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class ChangeKeywordNameRequest extends FormRequest
{

    //some comment
    protected function rules()
    {
        return [
            'adgroupId'=>'required',
            'customerId'=>'required|string',
            'keywordId'=>'required|string',
            'name'=>'required|string'
        ];
    }

    protected function messages () {
        return [];
    }
}
