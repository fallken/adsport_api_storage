<?php

namespace App\Wrappers;

class BiddingStrategyConfigurationWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $bids = null;

        $pureBids = $data->getBids();

        if (is_array($pureBids)) {

            $bids = [];

            foreach ($pureBids as $bid) {

                $bids[] = WrapperBridge::toObject($bid);
            }
        }

        $biddingScheme = $data->getBiddingScheme();

        return (object)[
            'bids' => $bids,

            'biddingStrategyId' => $data->getBiddingStrategyId(),
            'targetRoasOverride' => $data->getTargetRoasOverride(),
            'biddingStrategyName' => $data->getBiddingStrategyName(),
            'biddingStrategyType' => $data->getBiddingStrategyType(),
            'biddingStrategySource' => $data->getBiddingStrategySource(),

            'biddingScheme' => WrapperBridge::toObject($biddingScheme),
        ];
    }
}
