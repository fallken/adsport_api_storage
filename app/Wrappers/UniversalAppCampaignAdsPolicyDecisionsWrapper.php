<?php

namespace App\Wrappers;

class UniversalAppCampaignAdsPolicyDecisionsWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $policyTopicEntries = null;

        $entries = $data->getPolicyTopicEntries();

        if (is_array($entries)) {

            $policyTopicEntries = [];

            foreach ($entries as $entry) {

                $policyTopicEntries[] = WrapperBridge::toObject($entry);
            }
        }

        return (object)[
            'assetId' => $data->getAssetId(),
            'universalAppCampaignAsset' => $data->getUniversalAppCampaignAsset(),

            'policyTopicEntries' => $policyTopicEntries,
        ];
    }
}
