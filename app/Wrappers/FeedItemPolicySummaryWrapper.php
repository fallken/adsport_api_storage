<?php

namespace App\Wrappers;

class FeedItemPolicySummaryWrapper implements AdWordsWrapperInterface
{
    use PolicySummaryInfoTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @uses PolicySummaryInfo
     *
     */
    public static function toObject(&$data)
    {
        $errors = null;
        $validationErrors = $data->getValidationErrors();

        if (is_array($validationErrors)) {

            $errors = [];

            foreach ($validationErrors as $error) {

                $errors[] = WrapperBridge::toObject($error);
            }
        }

        return (object)array_merge(
            [
                'validationErrors' => $errors,

                'feedMappingId' => $data->getFeedMappingId(),
                'validationStatus' => $data->getValidationStatus(),
                'qualityApprovalStatus' => $data->getQualityApprovalStatus(),
                'qualityDisapprovalReasons' => $data->getQualityDisapprovalReasons(),
            ],
            static::append($data)
        );
    }
}
