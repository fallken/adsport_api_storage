<?php

namespace App\interfaces;

interface AdWordsSettingsInterface extends ResponseType
{
    /**
     * Initialize AdWords service.
     *
     * @param string $customerClientId
     * @param string $serviceType
     * @param string $settingType
     *
     * @return void
     */
    public function initialize(string $customerClientId, string $serviceType, string $settingType): void;

    /**
     * Handle requests for specified object.
     *
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     */
    public function handle(array $predicates = [], string $responseType = self::RESPONSE_TYPE_PURE);

    /**
     * Get default fields for current object.
     *
     * @return array
     */
    public function getDefaultFields(): array;

    /**
     * Create default predicates.
     *
     * @return array
     */
    public function generateDefaultPredicates(): array;

    /**
     * Set default object id.
     *
     * @param string $objectId
     *
     * @return mixed
     */
    public function setCurrentObjectId(string $objectId);
}
