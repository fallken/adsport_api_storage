<?php

namespace App\Copier\Interfaces;

interface CopierBridgeInterface extends CopierBridgeTypes
{
    /**
     * Initialize requirements.
     *
     * @param string $destinationCustomerId
     */
    public function initialize(string $destinationCustomerId): void;

    /**
     * Handle campaign transfer process.
     *
     * @param $campaign
     *
     * @return mixed
     */
    public function handleCampaigns($campaign);

    /**
     * Handle ad group transfer process.
     *
     * @param $adGroup
     *
     * @return mixed
     */
    public function handleAdGroups($adGroup);

    /**
     * Handle ad group ads transfer process.
     *
     * @param $adGroupAd
     * @param $newAdGroup
     *
     * @return mixed
     *
     */
    public function handleAds($adGroupAd, $newAdGroup);

    /**
     * Handle conversion tracker.
     *
     * @param $conversionTracker
     *
     * @return mixed
     */
    public function handleConversionTracker($conversionTracker);

    /**
     * Get response data of ids.
     *
     * @return array
     */
    public function getResults(): array;

    /**
     * Set source customer id.
     *
     * @param string $customerId
     */
    public function setSourceCustomerId(string $customerId);

    /**
     * Set source object id.
     *
     * @param string $objectId
     */
    public function setSourceObjectId(string $objectId);

    /**
     * Set source campaign id.
     *
     * @param string $objectId
     */
    public function setObjectId(string $objectId);
}