<?php

namespace App\Http\Controllers;

use App\repos\CustomerAccountRepository;
use App\interfaces\CustomerAccountInterface;
use App\Http\Interfaces\CustomerControllerInterface;
use App\Http\requests\addRequests\AddCustomerAccount;
use App\Http\requests\accountRequests\SendInvitationLink;
use App\Http\requests\setRequests\SetSharedBiddingStrategyRequest;

use Laravel\Lumen\Routing\Controller as BaseController;

class AccountController extends BaseController implements CustomerControllerInterface
{
    /**
     * @var CustomerAccountRepository
     */
    protected $customerAccount;

    /**
     * AccountController constructor.
     * @param CustomerAccountInterface $customerAccount
     */
    public function __construct(CustomerAccountInterface $customerAccount)
    {
        $this->customerAccount = $customerAccount;
    }

    /**
     * Set shared bidding strategy.
     *
     * @param SetSharedBiddingStrategyRequest $request
     *
     * @return mixed|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setSharedBiddingStrategy(SetSharedBiddingStrategyRequest $request)
    {
        return $this->customerAccount->SetBiddingStrategy(
            $request->input('customerClientId'),
            $request->input('operator'),
            $request->input('name'),
            $request->input('status'),
            $request->input('strategyType'),
            $request->input('microAmount'),
            $request->input('bidCeilingAmount') ?? '',
            $request->input('bidFloorAmount') ?? '',
            $request->input('strategyId') ?? ''
        );
    }

    /**
     * @OA\Post(
     *     path="/add/addCustomer",
     *     operationId="/addCustomer",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="accountName",
     *         in="query",
     *         description="name of the customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="someone"
     *          )
     *     ),
     *   @OA\Parameter(
     *         name="currencyCode",
     *         in="query",
     *         description="currency which the customer is going to use ",
     *         required=true,
     *         @OA\Schema(type="string",default="TRY")
     *     ),
     *   @OA\Parameter(
     *         name="timeZone",
     *         in="query",
     *         description="time zone for the customer",
     *         required=true,
     *         @OA\Schema(type="string",default="Europe/Istanbul")
     *  ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="add customer to the mcc account : return will be the customer id for the caller to set the customer",
     *     ),
     * )
     *
     * Create google ads account.
     *
     * @param AddCustomerAccount $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function createCustomerAccount(AddCustomerAccount $request)
    {
        return $this->customerAccount->createAccount([
            'accountName' => $request->input('accountName'),
            'currencyCode' => $request->input('currencyCode'),
            'timeZone' => $request->input('timeZone')
        ]);
    }


    /**
     * @OA\Post(
     *     path="/add/sendLink",
     *     operationId="/sendLink",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of customer which is going to be invited",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default=""
     *          )
     *     ),
     *   @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status for the account ",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ACTIVE","INACTIVE","PENDING","REFUSED","CANCELLED","UNKNOWN"})
     *     ),
     *   @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="operator for the account invitation setting.",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ADD","REMOVE","SET"})
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="add customer to the mcc account : return success message if the link invitation was successfull",
     *     ),
     * )
     *
     * Send invitation link to customer.
     *
     * @param SendInvitationLink $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function sendJoinLinkToCutomerAccount(SendInvitationLink $request)
    {
        $data = [
            'customerId' => $request->input('customerId'),
            'status' => $request->input('status'),
            'operator' => $request->input('operator')
        ];

        return $this->customerAccount->sendInvitationLinkToCustomer($data);
    }

    /*  public function setBudgetOrder(AddBudgetOrderRequest $request) {
          $data = json_decode($request->all(),true);
          //trying to set budget order using the repository
          $result = $this->customerAccount->setBudgetOrder($data);

          return $result;
      }*/
}
