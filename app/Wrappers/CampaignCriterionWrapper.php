<?php

namespace App\Wrappers;

class CampaignCriterionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $criterion =  $data->getCriterion();

        return (object)[
            'campaignId' => $data->getCampaignId(),
            'isNegative' => $data->getIsNegative(),
            'bidModifier' => $data->getBidModifier(),
            'baseCampaignId' => $data->getBaseCampaignId(),
            'campaignCriterionType' => $data->getCampaignCriterionType(),
            'campaignCriterionStatus' => $data->getCampaignCriterionStatus(),

            'criterion' => WrapperBridge::toObject($criterion),
        ];
    }
}
