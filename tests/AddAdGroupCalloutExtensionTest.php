<?php

use App\repos\AdGroupRepository;

class AddAdGroupCalloutExtensionTest extends TestCase
{
    /**
     * @var adGroupRepository
     */
    private $adGroupRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adGroupRepository = resolve(AdGroupRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testCalloutExtension()
    {
        $customerClientId = '261-911-0558';
        $groupId = '64912739685';

        $callouts = [
            ['value' => 'Test', 'feedId' => null],
            ['value' => 'Test 1', 'feedId' => null],
        ];

        $result = $this->adGroupRepository->addCalloutExtension($customerClientId, $groupId, $callouts);

        $this->assertIsArray($result);

        $response = $this->post('/add/addAdGroupCalloutExtension', [
            'customerClientId' => $customerClientId,
            'groupId' => $groupId,
            'callouts' => $callouts,
        ]);

        print_r($response->response->getContent());

        $response->assertResponseStatus(200);
    }
}
