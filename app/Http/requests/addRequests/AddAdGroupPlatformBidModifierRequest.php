<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddAdGroupPlatformBidModifierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'adGroupId' => ['required', 'string'],
            'campaignId' => ['nullable', 'string'],
            'operator' => ['required', 'string'],
            'platformId' => ['required', 'string'],
            'bidModifier' => ['required', 'numeric'],
        ];
    }
}
