<?php

namespace App\Http\Requests\getRequests;

use Anik\Form\FormRequest;
use App\Exceptions\EndPointRequestException;
use stdClass;

class GetAdsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        $predicatesHolder = @json_decode($this->input('predicates'));

        if (!$predicatesHolder) {
            $predicatesHolder = $this->input('predicates');
        }

        if (!is_array($predicatesHolder)) {
            $predicatesHolder = [];
        }

        $predicates = [];

        foreach ($predicatesHolder as $predicate) {

            if (!($predicate instanceof stdClass)) {
                $predicate = json_decode($predicate);
            }

            if (!isset($predicate->field) || !isset($predicate->operator) || !isset($predicate->values)) {
                throw new EndPointRequestException("Bad predicate exception. Payload data is:" . @json_encode($predicate), 422);
            }

            if (!is_array($predicate->values)) {
                throw new EndPointRequestException("Bad predicate values exception. Payload data is:" . @json_encode($predicate->values), 422);
            }

            $predicates[] = $predicate;
        }

        $this->merge(['predicates' => $predicates]);

        $this->merge(['adGroupId' => $this->input('adgroupId') ?? '']);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerId' => ['required', 'string'],
        ];
    }
}
