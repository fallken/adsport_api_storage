<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetBudgetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has('isExplicitlyShared')) {
            $this->merge(['isExplicitlyShared' => $this->input('isExplicitlyShared') === "true"]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'name' => 'required|string',
            'budgetId' => 'nullable|string',
            'campaignId' => 'nullable|string',
            'microAmount' => 'required|string',
            'deliveryMethod' => 'required|string',
            'isExplicitlyShared' => 'required|boolean',
        ];
    }
}
