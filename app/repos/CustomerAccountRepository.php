<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 4/6/2019
 * Time: 1:32 PM
 */

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\interfaces\CustomerAccountInterface;
use App\Wrappers\ManagedCustomerLinkWrapper;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyOperation;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyService;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\billing\BudgetOrder;
use Google\AdsApi\AdWords\v201809\billing\BudgetOrderOperation;
use Google\AdsApi\AdWords\v201809\billing\BudgetOrderService;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\SharedBiddingStrategy;
use Google\AdsApi\AdWords\v201809\cm\TargetCpaBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\TargetRoasBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\TargetSpendBiddingScheme;
use Google\AdsApi\AdWords\v201809\mcm\CustomerService;
use Google\AdsApi\AdWords\v201809\mcm\LinkOperation;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomer;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerLink;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerOperation;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;
use Google\AdsApi\AdWords\v201809\mcm\PendingInvitationSelector;
use Google\AdsApi\AdWords\v201809\mcm\ServiceLink;
use Google\AdsApi\AdWords\v201809\mcm\ServiceLinkLinkStatus;
use Google\AdsApi\AdWords\v201809\mcm\ServiceLinkOperation;
use Google\AdsApi\AdWords\v201809\mcm\ServiceType;
use LaravelGoogleAds\Services\AdWordsService;

class CustomerAccountRepository implements CustomerAccountInterface
{
    use RetryCallTrait;

    protected $adWordsService;

    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * Set bidding strategy for portfolio.
     *
     * @param string $customerClientId
     * @param string $operator
     * @param string $name
     * @param string $status
     * @param string $strategyType
     * @param string $microAmount
     * @param string $bidCeilingAmount
     * @param string $bidFloorAmount
     * @param string $strategyId
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function SetBiddingStrategy(string $customerClientId, string $operator, string $name, string $status, string $strategyType, string $microAmount, string $bidCeilingAmount = '', string $bidFloorAmount = '', string $strategyId = '')
    {
        $service = $this->adWordsService->getService(BiddingStrategyService::class, $customerClientId);

        $strategy = new SharedBiddingStrategy();
        $strategy->setType($strategyType); // Reference: BiddingStrategyType::class
        $strategy->setStatus($status); // Reference: SharedBiddingStrategyBiddingStrategyStatus::class
        $strategy->setName($name);

        if ($operator == Operator::SET) {
            $strategy->setId($strategyId);
        }

        $biddingScheme = null;

        switch ($strategyType) {
            case BiddingStrategyType::TARGET_CPA:
                $money = new Money();
                $money->setMicroAmount($microAmount);

                $biddingScheme = new TargetCpaBiddingScheme();
                $biddingScheme->setTargetCpa($money);

                break;
            case BiddingStrategyType::TARGET_SPEND;
                $money = new Money();
                $money->setMicroAmount($microAmount);

                $biddingScheme = new TargetSpendBiddingScheme();
                $biddingScheme->setSpendTarget($money);

                break;
            case BiddingStrategyType::TARGET_ROAS;
                $bidCeiling = new Money();
                $bidCeiling->setMicroAmount($bidCeilingAmount);

                $bidFloor = new Money();
                $bidFloor->setMicroAmount($bidFloorAmount);

                $biddingScheme = new TargetRoasBiddingScheme();
                $biddingScheme->setBidCeiling($bidCeiling);
                $biddingScheme->setBidFloor($bidFloor);
                $biddingScheme->setTargetRoas($microAmount);

                break;
        }

        $strategy->setBiddingScheme($biddingScheme);

        $operation = new BiddingStrategyOperation();
        $operation->setOperand($strategy);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
            'type' => $result->getType(),
            'status' => $result->getStatus(),
        ];
    }

    /**
     * @param $data
     * @return mixed
     * desc: this function will a budget order for the customer account with start , end and budget value for it .
     */
    public function setBudgetOrder($data)
    {
        $customerId = $data['customerId'];
        $startTime = $data['startTime'];
        $endTime = $data['endTime'];
        $microAmount = $data['microAmount'];
        //getting corresponding service : budget order service interface.
        $budgetOrderService = $this->adWordsService->getService(BudgetOrderService::class, $customerId);

        //todo:Billing account goes here .
        $billingAccount = [];

        $order = new BudgetOrder();
        $order->setBillingAccountId($billingAccount['id']);
        $order->setPrimaryBillingId($billingAccount['primaryBillingId']);
        $order->setStartDateTime($startTime);
        $order->setEndDateTime($endTime);

        $money = new Money();
        $money->setMicroAmount($microAmount);
        $order->setSpendingLimit($money);

        $budgeOrderOp = new BudgetOrderOperation();
        $budgeOrderOp->setOperator(Operator::ADD);

        $budgeOrderOp->setOperand($order);

        try {
            $result = $budgetOrderService->mutate();
            $campaign = $result->getValue()[0];
            //todo:return the corresponding parameters to user.
            $resultArr = $campaign;
            return response()->json($resultArr, 200);
        }
        catch (\Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    /**
     * @param $data
     * @return mixed
     * desc: returns the main budget and remaining customer budget by the customer id .
     */
    public function getCustomerBudget($data)
    {
        // TODO: Implement getCustomerBudget() method.
    }

    /**
     * Create customer account using the provided information
     *
     * @param $params
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function createAccount($params)
    {
        $accountName = $params['accountName'];
        $accountCurrency = $params['currencyCode'];
        $accountTimeZone = $params['timeZone'];

        //for creating customer we dont need to add session to the server.
        $managedCustomerService = $this->adWordsService->getService(ManagedCustomerService::class);

        // Create a managed customer.
        $customer = new ManagedCustomer();
        $customer->setName($accountName);
        $customer->setCurrencyCode($accountCurrency);
        $customer->setDateTimeZone($accountTimeZone);

        // Create a managed customer operation and add it to the list.
        $operation = new ManagedCustomerOperation();
        $operation->setOperator(Operator::ADD);
        $operation->setOperand($customer);

        $result = $this->call(function () use ($managedCustomerService, $operation) {
            return $managedCustomerService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'id' => $result->getCustomerId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * @param $params
     * @return mixed
     * desc:accepts a pedning invitation to link adwords account to a google manager account .
     */
    public function acceptAccountInvitationLink($params)
    {
        $serviceLinkId = $params['serviceLinkId'];

        //for creating cusotmer we dont need to add session to the server.
        $customerService = $this->adWordsService->getService(CustomerService::class);

        // Create service link.
        $serviceLink = new ServiceLink();
        $serviceLink->setServiceLinkId($serviceLinkId);
        $serviceLink->setServiceType(ServiceType::MERCHANT_CENTER);
        $serviceLink->setLinkStatus(ServiceLinkLinkStatus::ACTIVE);

        // Create a service link operation and add it to the list.
        $operations = [];
        $operation = new ServiceLinkOperation();
        $operation->setOperator(Operator::SET);
        $operation->setOperand($serviceLink);
        $operations[] = $operation;

        // Accept service links on the server and print out some information about
        // accepted service links.
        try {
            $serviceLinks = $customerService->mutateServiceLinks($operations);
            $result = [
                'id' => $serviceLinks->getServiceLinkId(),
                'type' => $serviceLinks->getServiceType(),
                'status' => $serviceLinks->getLinkStatus()
            ];
            return response()->json($result, 200);
        }
        catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 430);
        }
    }

    /**
     * This function will send an invitation to the user to join to the manager account.
     *
     * @param $params
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function sendInvitationLinkToCustomer($params)
    {
        $managedCustomerService = $this->adWordsService->getService(ManagedCustomerService::class);
        $clientId = str_replace('-', '', $params['customerId']);
        $managerId = str_replace('-', '', config('google-ads.ADWORDS.clientCustomerId'));

        $status = $params['status'] ?? "PENDING";
        $operator = $params['operator'] ?? "ADD";

        // Create link.
        $link = new ManagedCustomerLink();
        $link->setManagerCustomerId($managerId);
        $link->setClientCustomerId($clientId);
        $link->setLinkStatus($status);
//        $link->setPendingDescriptiveName('this is some pending descriptive name');

        $linkOperation = new LinkOperation();
        $linkOperation->setOperand($link);
        $linkOperation->setOperator($operator);

        $result = $this->call(function () use ($managedCustomerService, $linkOperation) {
            return $managedCustomerService->mutateLink([$linkOperation])->getLinks()[0];
        });

        return (array)ManagedCustomerLinkWrapper::toObject($result);
    }

    /**
     * desc: will reutnr list of pending operations
     */
    public function getPendingInvitations()
    {
        $managedCustomerService = $this->adWordsService->getService(ManagedCustomerService::class);
        $pendingSelector = new PendingInvitationSelector();
        $pendingSelector->getClientCustomerIds();

        $managedCustomerService->getSoapLogMessageFormatter();
    }

}