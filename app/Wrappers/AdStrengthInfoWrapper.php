<?php

namespace App\Wrappers;

class AdStrengthInfoWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        if ($data === null) {
            return null;
        }

        return (object)[
            'adStrength' => $data->getAdStrength(),
            'actionItems' => $data->getActionItems(),
        ];
    }
}
