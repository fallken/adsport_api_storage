<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddAdGroupAppExtensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $apps = @json_decode($this->input('apps'));

        if (!$apps) {
            $apps = $this->input('apps');
        }

        $this->merge(['apps' => $apps]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'adGroupId' => ['required', 'string'],
            'apps' => ['required', 'array', 'min:1', 'max:4']
        ];
    }
}
