<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Copier extends Model
{
    const STATUS_DONE = 'done';
    const STATUS_FAILED = 'failed';
    const STATUS_RUNNING = 'running';
    const STATUS_PENDING = 'pending';
    const STATUS_SUCCESS = 'success';
    const STATUS_CANCELED = 'canceled';

    const TYPE_FRESH = 'fresh';
    const TYPE_RESUME = 'resume';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token',
        'status',
        'request_id',
        'callback_url',
        'source_customer_id',
        'destination_customer_id',
    ];

    protected $casts = [
        'results' => 'object',
        'last_exception' => 'object',
    ];

    public function logs()
    {
        return $this->hasMany(CopierLogs::class);
    }
}
