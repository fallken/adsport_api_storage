<?php

namespace App\Copier\Repositories;

use App\Copier\Abstracts\AccountCopierAbstract;

class AccountCopierRepository extends AccountCopierAbstract
{
    /**
     * @var bool
     */
    private $debugMode = false;

    /**
     * @var CopierBridgeRepository
     */
    private $copierBridge;

    /**
     * Last results.
     *
     * @var array
     */
    private $lastResults;

    /**
     * Initialize account transfer requirements.
     *
     * @return void
     */
    public function initialize()
    {
        /**
         * Creating an instance of CopierBridgeRepository.
         */
        $this->copierBridge = new CopierBridgeRepository();

        /**
         * Initial copier bridge.
         */
        $this->copierBridge->initialize($this->destinationCustomerId);
    }

    /**
     * Handle account copier.
     *
     * @return bool
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function handle(): bool
    {
        $this->copierBridge->setSourceCustomerId($this->sourceCustomerId);

        /**
         * Start copy conversion trackers.
         */
        foreach ($this->getConversionTrackers() as $conversionTracker) {

            $this->copierBridge->handleConversionTracker($conversionTracker);
        }

        /**
         * Start copy campaigns.
         */
        foreach ($this->getCampaigns() as $campaign) {

            /**
             * Start copy single Campaign.
             */
            $this->copierBridge->setSourceObjectId($campaign->getId());

            $newCampaign = $this->copierBridge->handleCampaigns($campaign);

            /**
             * If campaign is expired, So we can skip it.
             */
            if ($newCampaign === false) {
                continue;
            }

            /**
             * Start campaign ad groups copy here.
             */
            $this->copierBridge->setObjectId($newCampaign->getId());

            foreach ($this->getAdGroups($campaign->getId()) as $adGroup) {

                /**
                 * Start copy single ad group.
                 */
                $this->copierBridge->setSourceObjectId($adGroup->getId());

                $newAdGroup = $this->copierBridge->handleAdGroups($adGroup);

                /**
                 * Start copying ads.
                 */
                if ($newAdGroup) {

                    foreach ($this->getAds($adGroup->getId()) as $ad) {
                        $this->copierBridge->handleAds($ad, $newAdGroup);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get account copier results.
     *
     * @return array
     */
    public function getResults(): array
    {
        $results = $this->copierBridge->getResults();

        if (!isset($results['conversionTrackers'])) {
            $results['conversionTrackers'] = [];
        }

        if (!isset($results['budgets'])) {
            $results['budgets'] = [];
        }

        if (!isset($results['campaigns'])) {
            $results['campaigns'] = [];
        }

        if (!isset($results['adGroups'])) {
            $results['adGroups'] = [];
        }

        if (!isset($results['ads'])) {
            $results['ads'] = [];
        }

        return [
            'sourceCustomerId' => $this->sourceCustomerId,
            'destinationCustomerId' => $this->destinationCustomerId,

            'results' => $results,
        ];
    }

    public function setResults($results): void
    {
        $this->lastResults = $results->results;
    }

    /**
     * Set debug mode.
     *
     * @param bool $status
     *
     * @return bool
     */
    public function setDebugMode(bool $status): bool
    {
        return $this->debugMode = $status;
    }

    /**
     * Get debug mode.
     *
     * @return bool
     */
    public function getDebugMode(): bool
    {
        return $this->debugMode;
    }
}
