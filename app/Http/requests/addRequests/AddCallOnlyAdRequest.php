<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddCallOnlyAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has('disableCallConversion')) {
            $this->merge(['disableCallConversion' => $this->input('disableCallConversion') === "true"]);
        }

        if ($this->has('callTracked')) {
            $this->merge(['callTracked' => $this->input('callTracked') === "true"]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'adGroupId' => ['required', 'string'],
            'countryCode' => ['required', 'string'],
            'phone' => ['required', 'string'],
            'businessName' => ['required', 'string'],
            'description1' => ['required', 'string'],
            'description2' => ['nullable', 'string'],
            'callTracked' => ['nullable', 'bool'],
            'conversionTypeId' => ['nullable', 'string'],
            'disableCallConversion' => ['nullable', 'bool'],
            'phoneNumberVerificationUrl' => ['nullable', 'string'],
        ];
    }
}
