<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddAdGroupCallExtensionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'adGroupId' => ['required', 'string'],
            'phoneNumber' => ['required', 'string'],
            'countryCode' => ['required', 'string']
        ];
    }
}
