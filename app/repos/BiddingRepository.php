<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 7/2/2019
 * Time: 12:14 PM
 */

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\interfaces\BiddingInterface;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\ManualCpcBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\TargetCpaBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\TargetRoasBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\TargetSpendBiddingScheme;
use Google\AdsApi\AdWords\v201809\mcm\ConversionTrackingSettings;
use LaravelGoogleAds\Services\AdWordsService;

class BiddingRepository implements BiddingInterface {
    protected $adWordsService, $oAuth2Credential;
    use RetryCallTrait;
    /**
     * BiddingRepository constructor.
     * setting Adwords service
     */
    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\EndPointRequestException
     * desc: will set manual cpc scheme for a campaign for specific user .
     */
    public function setManualCPC($customerId, $data) {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];

        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
                BiddingStrategyType::MANUAL_CPC
        );//BiddingStrategyType::MANUAL_CPC

        if (isset($data->enableEnhancedCpc))://setting enhanced cpc
            $scheme = new ManualCpcBiddingScheme();
            $scheme->setEnhancedCpcEnabled($data->enableEnhancedCpc);
            $biddingStrategyConfiguration->setBiddingScheme($scheme);
        endif;

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setMaximizeConversions($customerId, $data) {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];

        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
                BiddingStrategyType::MAXIMIZE_CONVERSIONS
        );//BiddingStrategyType::MANUAL_CPC
        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;


        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * desc: will set set Maximize Clicks scheme for a campaign for specific user .
     */
    public function setMaximizeClicks($customerId, $data) {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];

        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(
                BiddingStrategyType::TARGET_SPEND
        );//BiddingStrategyType::MANUAL_CPC
        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $bidCeilingMoney = new Money();
        $bidCeilingMoney->setMicroAmount($data->bidCeiling);

        $spenTargetMoney = new Money();
        $spenTargetMoney->setMicroAmount($data->spendTarget);

        $scheme = new TargetSpendBiddingScheme();
        $scheme->setBidCeiling($bidCeilingMoney);
        $scheme->setSpendTarget($spenTargetMoney);

        $biddingStrategyConfiguration->setBiddingScheme($scheme);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * @throws \App\Exceptions\EndPointRequestException
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetImpressionShare($customerId, $data) {//due date is not for now .
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];
        $targetImpressionScheme=null;
        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();

        if(isset($data->strategyId)):
            $biddingStrategyConfiguration->setBiddingStrategyId($data->strategyId);
        else:
            $biddingStrategyConfiguration->setBiddingStrategyType(
                    BiddingStrategyType::TARGET_ROAS
            );//BiddingStrategyType::MANUAL_CPC
            $targetImpressionScheme = new TargetRoasBiddingScheme();

            $bidFloor = new Money();
            $bidFloor->setMicroAmount($data->bidFloor);

            $bidCeiling = new Money();
            $bidCeiling->setMicroAmount($data->bidCeiling);

            $targetImpressionScheme->setTargetRoas($data->targetRoas);//will be a float number
            $targetImpressionScheme->setBidCeiling($bidCeiling);
            $targetImpressionScheme->setBidFloor($bidFloor);
        endif;

        $biddingStrategyConfiguration->setBiddingScheme($targetImpressionScheme);

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * lvl:campaign and portfolio
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetROAS(string $customerId, $data) {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];
        $roasScheme=null;
        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();

        if(isset($data->strategyId)):
            $biddingStrategyConfiguration->setBiddingStrategyId($data->strategyId);
        else:
            $biddingStrategyConfiguration->setBiddingStrategyType(
                    BiddingStrategyType::TARGET_ROAS
            );//BiddingStrategyType::MANUAL_CPC
            $roasScheme = new TargetRoasBiddingScheme();

            $bidFloor = new Money();
            $bidFloor->setMicroAmount($data->bidFloor);

            $bidCeiling = new Money();
            $bidCeiling->setMicroAmount($data->bidCeiling);

            $roasScheme->setTargetRoas($data->targetRoas);//will be a float number
            $roasScheme->setBidCeiling($bidCeiling);
            $roasScheme->setBidFloor($bidFloor);
        endif;

        $biddingStrategyConfiguration->setBiddingScheme($roasScheme);

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * lvl:campaign and portfolio
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetCPA($customerId, $data) {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);
        $operations=[];
        $cpaScheme=null;
        $campaign = new Campaign();
        $campaign->setId($data->campaignId);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        if(isset($data->strategyId)):

            $biddingStrategyConfiguration->setBiddingStrategyId($data->strategyId);
        else:
            $biddingStrategyConfiguration->setBiddingStrategyType(
                    BiddingStrategyType::TARGET_CPA
            );//BiddingStrategyType::MANUAL_CPC

            $targetCpa = new Money();
            $targetCpa->setMicroAmount($data->targetCpa);

            $maxCpcBidCeiling = new Money();
            $maxCpcBidCeiling->setMicroAmount($data->maxCpcBidCeiling);

            $maxCpcBidFloor = new Money();
            $maxCpcBidFloor->setMicroAmount($data->maxCpcBidFloor);

            $cpaScheme=new TargetCpaBiddingScheme();
            $cpaScheme->setTargetCpa($targetCpa);//will be a float number
            $cpaScheme->setMaxCpcBidCeiling($maxCpcBidCeiling);
            $cpaScheme->setMaxCpcBidFloor($maxCpcBidFloor);

        endif;

        $biddingStrategyConfiguration->setBiddingScheme($cpaScheme);
        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($data->operator??'SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }
}