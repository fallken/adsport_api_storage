<?php

namespace App\Wrappers;

trait SettingTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     */
    public static function append(&$data)
    {
        return [
            'settingType' => $data->getSettingType(),
        ];
    }
}