<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests\getRequests;


use Anik\Form\FormRequest;

class GetKeywordRecommendationRequest extends FormRequest
{

    //some comment
    protected function rules()
    {
        return [
            'adGroupId'=>'integer',
            'pageNumber'=>'integer',
            'keywords'=>'required|string',
            'customerId'=>'required|string',
            'type'=>'string'
        ];
    }

    protected function messages () {
        return [];
    }
}
