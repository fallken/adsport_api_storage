<?php

namespace App\Wrappers;

class PolicySummaryInfoWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        $policyTopicEntries = null;
        $policyTopicEntriesOriginal = $data->getPolicyTopicEntries();

        if (is_array($policyTopicEntriesOriginal)) {

            $policyTopicEntries = [];

            foreach ($policyTopicEntriesOriginal as $item) {

                $policyTopicEntries[] = PolicyTopicEntryWrapper::toObject($item);
            }
        }

        return (object)[
            'reviewState' => $data->getReviewState(),
            'denormalizedStatus' => $data->getDenormalizedStatus(),
            'combinedApprovalStatus' => $data->getCombinedApprovalStatus(),
            'policySummaryInfoType' => $data->getPolicySummaryInfoType(),

            'policyTopicEntries' => $policyTopicEntries,
        ];
    }
}
