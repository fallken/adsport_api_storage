<?php

namespace App\Wrappers;

class ResponsiveSearchAdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $headlines = null;
        $headlinesOriginal = $data->getHeadlines();

        if (is_array($headlinesOriginal)) {

            $headlines = [];

            foreach ($headlinesOriginal as $item) {
                $headlines[] = AssetLinkWrapper::toObject($item);
            }
        }

        $descriptions = null;
        $descriptionsOriginal = $data->getDescriptions();

        if (is_array($descriptionsOriginal)) {

            $descriptions = [];

            foreach ($descriptionsOriginal as $item) {
                $descriptions[] = AssetLinkWrapper::toObject($item);
            }
        }

        $responsiveSearchAd = AdWrapper::toObject($data);

        $responsiveSearchAd->path1 = $data->getPath1();
        $responsiveSearchAd->path2 = $data->getPath2();
        $responsiveSearchAd->headlines = $headlines;
        $responsiveSearchAd->descriptions = $descriptions;

        return $responsiveSearchAd;
    }
}
