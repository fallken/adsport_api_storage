<?php

namespace App\Http\Controllers;

use App\repos\ConversionTrackerRepository;
use App\Http\Interfaces\ConversionTrackerControllerInterface;
use App\Http\requests\setRequests\SetConversionTrackerRequest;

class ConversionTrackerController extends Controller implements ConversionTrackerControllerInterface
{
    protected $conversionTrackerRepository;

    /**
     * @param ConversionTrackerRepository $conversionTrackerRepository
     */
    public function __construct(ConversionTrackerRepository $conversionTrackerRepository)
    {
        $this->conversionTrackerRepository = $conversionTrackerRepository;
    }

    /**
     * Set conversion tracker.
     *
     * @param SetConversionTrackerRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setConversionTracking(SetConversionTrackerRequest $request)
    {
        return $this->conversionTrackerRepository->setConversionTracker(
            $request->input('customerClientId'),
            $request->input('trackerType'),
            $request->input('operator'),
            $request->input('name'),
            $request->input('category') ?? '',
            $request->input('currencyCode') ?? '',
            $request->input('status')
        );
    }
}

