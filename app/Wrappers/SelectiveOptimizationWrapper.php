<?php

namespace App\Wrappers;

class SelectiveOptimizationWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $conversionTypeIdsOps = $data->getConversionTypeIdsOps();

        return (object)[
            'conversionTypeIds' => $data->getConversionTypeIds(),
            'conversionTypeIdsOps' => WrapperBridge::toObject($conversionTypeIdsOps),
        ];
    }
}
