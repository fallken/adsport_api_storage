<?php

namespace App\Wrappers;

class AdGroupAdPolicySummaryWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        if ($data === null) {
            return null;
        }
        
        $policyTopicEntries = null;
        $policyTopicEntriesOriginal = $data->getPolicyTopicEntries(); // []

        if (is_array($policyTopicEntriesOriginal)) {

            $policyTopicEntries = [];

            foreach ($policyTopicEntriesOriginal as $item) {

                $policyTopicEntries[] = PolicyTopicEntryWrapper::toObject($item);
            }
        }

        return (object)[
            'reviewState' => $data->getReviewState(),
            'denormalizedStatus' => $data->getDenormalizedStatus(),
            'combinedApprovalStatus' => $data->getCombinedApprovalStatus(),

            'policyTopicEntries' => $policyTopicEntries,
        ];
    }
}
