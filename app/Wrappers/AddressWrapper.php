<?php

namespace App\Wrappers;

class AddressWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data)
    {
        return (object)[
            'cityName' => $data->getCityName(),
            'postalCode' => $data->getPostalCode(),
            'countryCode' => $data->getCountryCode(),
            'provinceCode' => $data->getProvinceCode(),
            'provinceName' => $data->getProvinceName(),
            'streetAddress' => $data->getStreetAddress(),
            'streetAddress2' => $data->getStreetAddress2(),
        ];
    }
}
