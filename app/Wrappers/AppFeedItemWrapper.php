<?php

namespace App\Wrappers;

class AppFeedItemWrapper implements AdWordsWrapperInterface
{
    use ExtensionFeedItemTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $appFinalUrls = $data->getAppFinalUrls();
        $appFinalMobileUrls = $data->getAppFinalMobileUrls();
        $appUrlCustomParameters = $data->getAppUrlCustomParameters();

        return (object)array_merge(static::append($data),
            [
                'appId' => $data->getAppId(),
                'appUrl' => $data->getAppUrl(),
                'appStore' => $data->getAppStore(),
                'appLinkText' => $data->getAppLinkText(),
                'appFinalUrlSuffix' => $data->getAppFinalUrlSuffix(),
                'appTrackingUrlTemplate' => $data->getAppTrackingUrlTemplate(),

                'appFinalUrls' => WrapperBridge::toObject($appFinalUrls),
                'appFinalMobileUrls' => WrapperBridge::toObject($appFinalMobileUrls),
                'appUrlCustomParameters' => WrapperBridge::toObject($appUrlCustomParameters),
            ]
        );
    }
}
