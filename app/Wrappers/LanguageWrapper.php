<?php

namespace App\Wrappers;

class LanguageWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'id' => $data->getId(),
            'type' => $data->getType(),
            'criterionType' => $data->getCriterionType(),

            'name' => $data->getName(),
            'code' => $data->getCode(),
        ];
    }
}
