<?php

use App\repos\CustomerAccountRepository;

use Illuminate\Support\Str;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\SharedBiddingStrategyBiddingStrategyStatus;

class SharedBiddingStrategyTest extends TestCase
{
    /**
     * @var CustomerAccountRepository
     */
    private $customer;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->customer = new CustomerAccountRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetTargetCpaStrategy()
    {
        $sourceCustomer = '541-403-9496';

        $result = $this->customer->SetBiddingStrategy(
            $sourceCustomer,
            Operator::ADD,
            'Target CPA strategy.' . Str::random(),
            SharedBiddingStrategyBiddingStrategyStatus::ENABLED,
            BiddingStrategyType::TARGET_CPA,
            150000000);

        print_r($result);

        $this->assertIsArray($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetTargetSpendStrategy()
    {
        $sourceCustomer = '541-403-9496';

        $result = $this->customer->SetBiddingStrategy(
            $sourceCustomer,
            Operator::ADD,
            'Target spend strategy.' . Str::random(),
            SharedBiddingStrategyBiddingStrategyStatus::ENABLED,
            BiddingStrategyType::TARGET_SPEND,
            150000000);

        print_r($result);

        $this->assertIsArray($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetTargetRoasStrategy()
    {
        $sourceCustomer = '541-403-9496';

        $result = $this->customer->SetBiddingStrategy(
            $sourceCustomer,
            Operator::ADD,
            'Target roas strategy.' . Str::random(),
            SharedBiddingStrategyBiddingStrategyStatus::ENABLED,
            BiddingStrategyType::TARGET_ROAS,
            150,
            10000,
            4500
        );

        print_r($result);

        $this->assertIsArray($result);
    }
}
