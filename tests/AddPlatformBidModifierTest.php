<?php

use App\repos\AdGroupRepository;
use App\repos\CampaignsRepository;

class AddPlatformBidModifierTest extends TestCase
{
    /**
     * @var \App\repos\AdGroupRepository
     */
    private $adGroupRepository;

    /**
     * @var \App\repos\CampaignsRepository;
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = new CampaignsRepository();

        $this->adGroupRepository = resolve(AdGroupRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddAdGroupPlatformBidModifier()
    {
        $result = $this->adGroupRepository->addPlatformBidModifier(
            '541-403-9496',
            '2045454855',
            '71237816126',
            'ADD',
            '30002',
            4.7
        );

        $this->assertIsArray($result);

        $result = $this->adGroupRepository->addPlatformBidModifier(
            '541-403-9496',
            '',
            '71237816126',
            'ADD',
            '30002',
            4.7
        );

        $this->assertIsArray($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddCampaignPlatformBidModifier()
    {
        $result = $this->campaignRepository->addPlatformBidModifier(
            '541-403-9496',
            '2045454855',
            '30000',
            4.7
        );

        $this->assertIsArray($result);

        $result = $this->campaignRepository->addPlatformBidModifier(
            '541-403-9496',
            '2045454855',
            '30001',
            7.3
        );

        $this->assertIsArray($result);
    }
}
