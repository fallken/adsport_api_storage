<?php

namespace App\Http\Interfaces;

use App\Http\requests\setRequests\SetAdGroupRequest;
use App\Http\requests\setRequests\SetAdGroupAdRequest;

interface AdsControllerInterface
{
    /**
     * @OA\Post(
     *     path="/add/setAd",
     *     operationId="/setAd",
     *     tags={"Set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="261-911-0558"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="ad group id",
     *         required=true,
     *         @OA\Schema(type="string",default="64912739685")
     *     ),
     *   @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the Ad . PAUSE , ENABLED , REMOVED",
     *         required=false,
     *         @OA\Schema(type="string", enum={"ENABLED", "REMOVED", "PAUSED"}, default="ENABLED")
     *     ),
     *   @OA\Parameter(
     *         name="adType",
     *         in="query",
     *         description="Ad type.",
     *         required=false,
     *         @OA\Schema(type="string", enum={"RESPONSIVE_SEARCH_AD"}, default="RESPONSIVE_SEARCH_AD")
     *     ),
     *   @OA\Parameter(
     *         name="finalUrl",
     *         in="query",
     *         description="final url of which the ad will redirect user to that page",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *   @OA\Parameter(
     *         name="headlines",
     *         in="query",
     *         description="headlines of the Ad (should be on json)",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *   @OA\Parameter(
     *         name="descriptions",
     *         in="query",
     *         description="description of the Ad (should be on json)",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *     @OA\Parameter(
     *         name="path1",
     *         in="query",
     *         description="path1",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *     @OA\Parameter(
     *         name="path2",
     *         in="query",
     *         description="path2",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Operator be ADD or SET",
     *         required=false,
     *         @OA\Schema(type="string", default="ADD", enum={"SET", "ADD"})
     *     ),
     *     @OA\Parameter(
     *         name="adId",
     *         in="query",
     *         description="Ad id use for update specified ad",
     *         required=false,
     *         @OA\Schema(type="string",default="")
     *     ),
     *
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is: adId",
     *     ),
     * )
     *
     * Update or add new ad group ad.
     *
     * @param SetAdGroupAdRequest $request
     *
     * @return mixed
     */
    public function setAd(SetAdGroupAdRequest $request);

    /**
     * @OA\Post(
     *     path="/add/addAdgroup",
     *     operationId="/addAdgroup",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="1703779813")
     *     ),
     *    @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name set by the user for the ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="بزغاله")
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the ad group . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(type="string",enum={"ENABLED", "REMOVED", "PAUSED"}, default="ENABLED")
     *     ),
     *     @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="money price that the user is going to set for the adgroup",
     *         required=true,
     *         @OA\Schema(type="string", default="10000")
     *     ),
     *    @OA\Parameter(
     *         name="criterionType",
     *         in="query",
     *         description="criterionType that the user is going to set for the adgroup . KEYWORD,USER_INTEREST_AND_LIST,VERTICAL,GENDER,AGE_RANGE,PLACEMENT,PARENT,INCOME_RANGE,NONE,UNKNOWN",
     *         required=true,
     *         @OA\Schema(type="string", default="PLACEMENT", enum={"KEYWORD", "USER_INTEREST_AND_LIST", "VERTICAL", "GENDER", "AGE_RANGE", "PLACEMENT", "PARENT", "INCOME_RANGE", "NONE", "UNKNOWN"})
     *     ),
     *    @OA\Parameter(
     *         name="adRotationMode",
     *         in="query",
     *         description="adRotationMode that the user is going to set for the adgroup . UNKNOWN,OPTIMIZE,ROTATE_FOREVER",
     *         required=true,
     *         @OA\Schema(type="string", enum={"UNKNOWN","OPTIMIZE","ROTATE_FOREVER"}, default="OPTIMIZE")
     *     ),
     *     @OA\Parameter(
     *         name="trackingUrlTemplate",
     *         in="query",
     *         description="trackingUrlTemplate that the user is going to set for the ad group.",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="urlCustomParameters",
     *         in="query",
     *         description="urlCustomParameters that the user is going to set for the ad group. [{key: site, vakue: addup, isRemoved: false}]",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="doReplace",
     *         in="query",
     *         description="doReplace that the user is going to set for the ad group.",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="operator that the user is going to set for the ad group",
     *         required=true,
     *         @OA\Schema(type="string", enum={"SET","ADD"}, default="ADD")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="Send id for update specified ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : name,adgroupId,campaignName",
     *     ),
     * )
     *
     * Update or add new ad group.
     *
     * @param SetAdGroupRequest $request
     *
     * @return mixed
     */
    public function setAdGroup(SetAdGroupRequest $request);
}
