<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetCampaignNetworkSetting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has('partnerSearchNetwork')) {
            $this->merge(['partnerSearchNetwork' => $this->input('partnerSearchNetwork') === "true"]);
        }

        if ($this->has('googleSearch')) {
            $this->merge(['googleSearch' => $this->input('googleSearch') === "true"]);
        }

        if ($this->has('searchNetwork')) {
            $this->merge(['searchNetwork' => $this->input('searchNetwork') === "true"]);
        }

        if ($this->has('contentNetwork')) {
            $this->merge(['contentNetwork' => $this->input('contentNetwork') === "true"]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'campaignId' => 'required|string',
            'googleSearch' => 'nullable|boolean',
            'searchNetwork' => 'nullable|boolean',
            'contentNetwork' => 'nullable|boolean',
            'partnerSearchNetwork' => 'nullable|boolean',
        ];
    }
}
