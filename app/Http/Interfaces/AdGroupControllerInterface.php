<?php

namespace App\Http\Interfaces;

use App\Http\Requests\addRequests\AddAdGroupAppExtensionRequest;
use App\Http\Requests\addRequests\AddAdGroupCalloutExtensionRequest;
use App\Http\Requests\addRequests\AddAdGroupSiteLinkExtensionRequest;
use App\Http\Requests\addRequests\SetAdGroupExtensionsRequest;
use App\Http\requests\setRequests\SetAdGroupTrackingUrlTemplateRequest;
use App\Http\requests\setRequests\SetAdGroupUrlCustomParametersRequest;

interface AdGroupControllerInterface
{
    /**
     * @OA\Post(
     *     path="/set/setAdGroupTrackingUrlTemplate",
     *     operationId="/setAdGroupTrackingUrlTemplate",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="The id of ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="71237816126")
     *     ),
     *     @OA\Parameter(
     *         name="trackingUrlTemplate",
     *         in="query",
     *         description="The tracking url template",
     *         required=true,
     *         @OA\Schema(type="string", default="https://your-site.com/tracking-url-template{lpurl}?utm_campaign={campaignid}&adgroupid={adgroupid}&utm_content={creative}&utm_term={keyword}")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding ad group",
     *     ),
     * )
     *
     * Set tracking url template.
     *
     * @param SetAdGroupTrackingUrlTemplateRequest $request
     *
     * @return mixed
     */
    public function setTrackingUrlTemplate(SetAdGroupTrackingUrlTemplateRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setAdGroupUrlCustomParameters",
     *     operationId="/setAdGroupUrlCustomParameters",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="The id of ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="71237816126")
     *     ),
     *     @OA\Parameter(
     *         name="urlCustomParameters",
     *         in="query",
     *         description="The url custom parameters of array of objects. Keys: key, value, isRemoved.",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding ad group",
     *     ),
     * )
     *
     * Set url custom parameters.
     *
     * @param SetAdGroupUrlCustomParametersRequest $request
     *
     * @return mixed
     */
    public function setUrlCustomParameters(SetAdGroupUrlCustomParametersRequest $request);

    /**
     * @OA\Post(
     *     path="/add/addAdGroupCalloutExtension",
     *     operationId="/addAdGroupCalloutExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="groupId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="64912739685")
     *     ),
     *     @OA\Parameter(
     *         name="callouts",
     *         in="query",
     *         description="The array of callout titles. For example: [text 1, text 2, ...]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the groupId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupCalloutExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addCalloutExtension(AddAdGroupCalloutExtensionRequest $request);

    /**
     * @OA\Post(
     *     path="/add/addAdGroupSiteLinkExtension",
     *     operationId="/addAdGroupSiteLinkExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="id of the corresponding adGroup",
     *         required=true,
     *         @OA\Schema(type="string", default="64912739685")
     *     ),
     *     @OA\Parameter(
     *         name="links",
     *         in="query",
     *         description="The site link extension. Example: [{text: text 1, url: https://google.com/test-link-extension}]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the groupId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupSiteLinkExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addSiteLinkExtension(AddAdGroupSiteLinkExtensionRequest $request);

    /**
     * @OA\Post(
     *     path="/add/addAdGroupAppExtension",
     *     operationId="/addAdGroupAppExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="id of the corresponding adGroup",
     *         required=true,
     *         @OA\Schema(type="string", default="64912739685")
     *     ),
     *     @OA\Parameter(
     *         name="apps",
     *         in="query",
     *         description="The array of apps. store must be 1 for google play and 2 for apple, For example: [appId: com.example.myapp, store: 1, text: Clone me!, link: https://play.google.com/store/apps/details?id=com.example.myapp, ... ]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the adGroupId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupAppExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addAppExtension(AddAdGroupAppExtensionRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setAdGroupExtensions",
     *     operationId="/setAdGroupExtensions",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="id of the corresponding ad group.",
     *         required=true,
     *       @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="extensions",
     *         in="query",
     *         description="A list of ad group extensions, Like sitelink, app and callout extensions.",
     *         required=true,
     *      @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and type of corresponding campaign",
     *     ),
     * )
     *
     * Set ad group extensions.
     *
     * @param SetAdGroupExtensionsRequest $request
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setExtensions(SetAdGroupExtensionsRequest $request);
}
