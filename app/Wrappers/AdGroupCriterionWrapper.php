<?php

namespace App\Wrappers;

class AdGroupCriterionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $labels = null;
        $tmpLabels = $data->getLabels();

        if (is_array($tmpLabels)) {

            $labels = [];

            foreach ($tmpLabels as $label) {

                $labels[] = WrapperBridge::toObject($label);
            }
        }

        $criterion = $data->getCriterion();

        return (object)[
            'getAdGroupId' => $data->getAdGroupId(),
            'getCriterionUse' => $data->getCriterionUse(),
            'getBaseCampaignId' => $data->getBaseCampaignId(),
            'getBaseAdGroupId' => $data->getBaseAdGroupId(),
            'getAdGroupCriterionType' => $data->getAdGroupCriterionType(),

            'labels' => $labels,
            'criterion' => WrapperBridge::toObject($criterion),
        ];
    }
}
