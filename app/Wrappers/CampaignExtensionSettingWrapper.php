<?php

namespace App\Wrappers;

class CampaignExtensionSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $extensionSetting = $data->getExtensionSetting();

        return (object)[
            'campaignId' => $data->getCampaignId(),
            'extensionType' => $data->getExtensionType(),
            'extensionSetting' => WrapperBridge::toObject($extensionSetting),
        ];
    }
}
