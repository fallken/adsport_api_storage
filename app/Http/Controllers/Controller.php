<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="google Ads API new",
     *   version="1.0",
     *   @OA\Contact(
     *     email="arsalani@outlook.com",
     *     name="alpha team"
     *   )
     * )
     */
}
