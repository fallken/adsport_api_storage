<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetKeywordsSettingRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'adGroupId' => 'required|string',
            'keywordId' => 'required|string',
            'matchType' => 'nullable|string',
            'status' => 'nullable|string',
            'finalUrl' => 'nullable|string',
            'bidType' => 'nullable|string',
            'microAmount' => 'required|string',
            'operator' => 'nullable|string',
        ];
    }
}
