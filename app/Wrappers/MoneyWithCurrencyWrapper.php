<?php

namespace App\Wrappers;

class MoneyWithCurrencyWrapper implements AdWordsWrapperInterface
{
    use ComparableValueTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $money = $data->getMoney();

        return (object)array_merge(static::append($data),
            [
                'currencyCode' => $data->getMoney(),

                'money' => WrapperBridge::toObject($money),
                'finalMobileUrls' => WrapperBridge::toObject($finalMobileUrls),
            ]
        );
    }
}
