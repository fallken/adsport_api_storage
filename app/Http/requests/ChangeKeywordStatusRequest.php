<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class ChangeKeywordStatusRequest extends FormRequest
{

    //some comment
    protected function rules()
    {
        return [
            'adgroupId'=>'required',
            'customerId'=>'required|string',
            'keywordId'=>'required|string',
            'status'=>'required|in:ENABLED,REMOVED,PAUSED'
        ];
    }

    protected function messages () {
        return [];
    }
}
