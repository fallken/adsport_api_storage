<?php

namespace App\Wrappers;

class FeedItemCampaignTargetingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'targetingCampaignId' => $data->getTargetingCampaignId(),
        ];
    }
}
