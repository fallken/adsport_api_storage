<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetConversionTrackerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name' => 'required|string',
            'category' => 'nullable|string',
            'currencyCode' => 'nullable|string',
            'operator' => 'required|string',
            'trackerType' => 'required|string',
            'status' => 'required|string',

            'customerClientId' => 'required|string',
        ];
    }
}
