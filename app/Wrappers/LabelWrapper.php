<?php

namespace App\Wrappers;

class LabelWrapper implements AdWordsWrapperInterface
{
    use AdGroupCriterionTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'labelAttributeType' => $data->getLabelAttributeType(),
        ];
    }
}
