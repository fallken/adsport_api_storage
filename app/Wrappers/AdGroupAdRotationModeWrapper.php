<?php

namespace App\Wrappers;

class AdGroupAdRotationModeWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'adRotationMode' => $data->getAdRotationMode(),
        ];
    }
}
