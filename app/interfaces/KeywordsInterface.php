<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 11:16 AM
 */

namespace App\interfaces;

use Illuminate\Http\Request;

interface KeywordsInterface extends ResponseType
{
    /**
     * @param $data
     * @return mixed
     */
    public function addKeyword($data);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeKeywordStatus(Request $request);

    /**
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $sortOrder
     * @param string $responseType
     *
     * @return mixed
     */
    public function getKeywords(string $customerClientId, string $adGroupId, string $sortOrder = '', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeKeywordName(Request $request);

    /**
     * @param $data
     * @return mixed
     * desc:returns back the recommendations to the user .
     */
    public function getKeywordRecommendations($data);

    /**
     * Set keyword settings.
     *
     * @param string $customerId
     * @param string $adGroupId
     * @param string $keywordId
     * @param string $operator
     * @param string $matchType
     * @param string $status
     * @param string $finalUrl
     * @param string $bidType
     * @param string $microAmount
     *
     * @return array
     */
    public function setKeywordsSetting(string $customerId, string $adGroupId, string $keywordId, string $operator, string $matchType, string $status , string $finalUrl, string $bidType, string $microAmount);
}
