<?php

namespace App\Wrappers;

class MoneyWrapper implements AdWordsWrapperInterface
{
    use ComparableValueTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)array_merge(static::append($data),
            [
                'microAmount' => $data->getMicroAmount(),
            ]
        );
    }
}
