<?php

namespace App\Wrappers;

trait ComparableValueTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     */
    public static function append(&$data)
    {
        return [
            'comparableValueType' => $data->getComparableValueType(),
        ];
    }
}