<?php

namespace App\Wrappers;

class TargetingSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $details = [];

        foreach ($data->getDetails() as $detail) {

            $details[] = WrapperBridge::toObject($detail);
        }

        return (object)[
            'details' => $details,
            'settingType' => $data->getSettingType(),
        ];
    }
}
