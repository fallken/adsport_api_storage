<?php

namespace App\Copier\Abstracts;

use App\Interfaces\ResponseType;
use App\repos\AdGroupRepository;
use App\repos\AdGroupAdRepository;
use App\repos\CampaignsRepository;
use App\Copier\Interfaces\AccountCopierInterface;

use App\repos\ConversionTrackerRepository;
use LaravelGoogleAds\Services\AdWordsService;

abstract class AccountCopierAbstract implements AccountCopierInterface
{
    /**
     * @var string
     */
    protected $sourceCustomerId = '';

    /**
     * @var string
     */
    protected $destinationCustomerId = '';

    /**
     * Campaigns
     */
    protected $campaigns;

    /**
     * Set source customer.
     *
     * @param string $sourceCustomerId
     *
     * @return void
     */
    public function setSourceCustomer(string $sourceCustomerId): void
    {
        $this->sourceCustomerId = $sourceCustomerId;
    }

    /**
     * Set destination customer.
     *
     * @param string $destinationCustomerId
     *
     * @return void
     */
    public function setDestinationCustomer(string $destinationCustomerId): void
    {
        $this->destinationCustomerId = $destinationCustomerId;
    }

    /**
     * Get campaigns.
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function getCampaigns()
    {
        $campaignRepository = new CampaignsRepository();

        $predicates = [
//            ['field' => 'Id', 'operator' => 'EQUALS', 'values' => [1722928695]] // Just for test.
        ];

        return $campaignRepository->getCampaigns($this->sourceCustomerId, $predicates, ResponseType::RESPONSE_TYPE_PURE);
    }

    /**
     * Get ad groups for specified campaign.
     *
     * @param string $campaignId
     *
     * @return mixed|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function getAdGroups(string $campaignId)
    {
        $adGroupRepository = new AdGroupRepository(resolve(AdWordsService::class));

        $predicates = [
//            ['field' => 'Id', 'operator' => 'EQUALS', 'values' => [2041540934]]
        ];

        return $adGroupRepository->getAdGroups($this->sourceCustomerId, $campaignId, $predicates, ResponseType::RESPONSE_TYPE_PURE);
    }

    /**
     * Get ads for specified ad group.
     *
     * @param string $adGroupId
     * @return mixed|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getAds(string $adGroupId)
    {
        $adRepository = new AdGroupAdRepository();

        $predicates = [
//            ['field' => 'Id', 'operator' => 'EQUALS', 'values' => [2041540934]]
        ];

        return $adRepository->getAds($this->sourceCustomerId, $adGroupId, $predicates, ResponseType::RESPONSE_TYPE_PURE);
    }

    /**
     * Get conversion trackers.
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getConversionTrackers()
    {
        $conversionTrackerRepository = new ConversionTrackerRepository();

        $predicates = [
//            ['field' => 'Id', 'operator' => 'EQUALS', 'values' => [2041540934]]
        ];

        return $conversionTrackerRepository->getConversionTrackers($this->sourceCustomerId, $predicates, ResponseType::RESPONSE_TYPE_PURE);
    }
}