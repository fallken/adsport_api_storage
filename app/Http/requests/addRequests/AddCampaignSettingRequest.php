<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests\addRequests;

use stdClass;
use App\Exceptions\EndPointRequestException;

use Anik\Form\FormRequest;

class AddCampaignSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        $settingsHolder = @json_decode($this->input('settings'));

        if (!$settingsHolder) {
            $settingsHolder = $this->input('settings');
        }

        if (!is_array($settingsHolder)) {
            $settingsHolder = [];
        }

        $settings = [];

        foreach ($settingsHolder as $setting) {

            if (!($setting instanceof stdClass)) {
                if (is_array($setting)) {
                    $setting = (object)$setting;
                }
                else {
                    $setting = json_decode($setting);
                }
            }

            if (!isset($setting->id) || !isset($setting->type) || !isset($setting->isNegative)) {
                throw new EndPointRequestException("The setting id, type or isNegative is invalid. Payload is: " . json_encode($setting), 422);
            }

            $settings[] = $setting;
        }

        $this->merge(['settings' => $settings]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'campaignId' => 'required|string',
            'customerId' => 'required|string',
            'language' => 'integer',
            'ageRange' => 'integer',
            'device' => 'integer',
            'settings' => 'required|array'
        ];
    }
}
