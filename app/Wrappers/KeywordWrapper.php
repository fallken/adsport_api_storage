<?php

namespace App\Wrappers;

class KeywordWrapper implements AdWordsWrapperInterface
{
    use CriterionTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)array_merge(static::append($data),
            [
                'text' => $data->getText(),
                'matchType' => $data->getMatchType(),
            ]
        );
    }
}
