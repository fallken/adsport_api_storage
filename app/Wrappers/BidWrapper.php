<?php

namespace App\Wrappers;

class BidWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $amount = $data->getAmount();

        return (object)[
            'amount' => WrapperBridge::toObject($amount),
        ];
    }
}
