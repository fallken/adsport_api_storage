<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Connection Name
    |--------------------------------------------------------------------------
    |
    | Lumen's queue API supports an assortment of back-ends via a single
    | API, giving you convenient access to each back-end using the same
    | syntax for every one. Here you may define a default connection.
    |
    */

    'token' => env('COPIER_TOKEN', 'put-your-copier-token-here'),

    'callback_fail_delay' => env('COPIER_CALLBACK_FAILED_DELAY', 60),
];
