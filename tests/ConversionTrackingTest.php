<?php

use App\Interfaces\ResponseType;
use App\repos\ConversionTrackerRepository;

class ConversionTrackingTest extends TestCase
{
    /**
     * @var ConversionTrackerRepository
     */
    private $conversionTracker;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->conversionTracker = new ConversionTrackerRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function testSetConversionTracking()
    {
        $category = '';
        $operator = 'ADD';
        $currencyCode = '';
        $status = 'ENABLED';
        $trackerType = 'website';
        $customerId = '541-403-9496';
        $name = 'New conversion tracker 1';

        $result = $this->conversionTracker->setConversionTracker(
            $customerId,
            $trackerType,
            $operator,
            $name,
            $category,
            $currencyCode,
            $status
        );

        print_r($result);

        $this->assertIsArray($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function testGetConversionTracking()
    {
        $result = $this->conversionTracker->getConversionTrackers('541-403-9496', [], ResponseType::RESPONSE_TYPE_PURE);

        print_r($result);

        $this->assertIsArray($result);
    }
}
