<?php

use App\repos\CampaignsRepository;

class AddLocationBidModifierTest extends TestCase
{
    /**
     * @var \App\repos\CampaignsRepository;
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = new CampaignsRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddCampaignLocationBidModifier()
    {
        $result = $this->campaignRepository->addGeoBidModifier(
            '541-403-9496',
            '2045454855',
            '1000138',
            4.7
        );

        $this->assertIsArray($result);
    }
}
