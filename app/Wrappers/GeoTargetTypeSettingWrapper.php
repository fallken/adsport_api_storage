<?php

namespace App\Wrappers;

class GeoTargetTypeSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object) [
            'settingType' => $data->getSettingType(),
            'positiveGeoTargetType' => $data->getPositiveGeoTargetType(),
            'negativeGeoTargetType' => $data->getNegativeGeoTargetType(),
        ];
    }
}
