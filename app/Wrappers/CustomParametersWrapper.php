<?php

namespace App\Wrappers;

class CustomParametersWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $params = null;

        $parameters = $data->getParameters();

        if (is_array($parameters)) {

            $params = [];

            foreach ($parameters as $parameter) {

                $params[] = WrapperBridge::toObject($parameter);
            }
        }

        return (object)[
            'parameters' => $params,

            'doReplace' => $data->getDoReplace(),
        ];
    }
}
