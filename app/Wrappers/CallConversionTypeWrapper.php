<?php

namespace App\Wrappers;

class CallConversionTypeWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'conversionTypeId' => $data->getConversionTypeId(),
        ];
    }
}
