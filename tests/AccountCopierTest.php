<?php

use App\Copier\Repositories\AccountCopierRepository;

class AccountCopierTest extends TestCase
{
    /**
     * @var AccountCopierRepository
     */
    private $transformer;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->transformer = new AccountCopierRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function testAccountCopier()
    {
        $sourceCustomer = '261-911-0558';
        $destinationCustomer = '304-362-6043';

        $this->transformer->setSourceCustomer($sourceCustomer);

        $this->transformer->setDestinationCustomer($destinationCustomer);

        $this->assertTrue($this->transformer->handle());
    }

    /**
     * Test copier api.
     */
    public function testCopierApi()
    {
        $data = [
            'sourceId' => '261-911-0558', 'destinationId' => '64005138661', 'callbackUrl' => 'https://local.api/call-me'
        ];

        $response = $this->post('/copiers', $data, ['copierToken' => 'put-your-copier-token-here']);

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }
}
