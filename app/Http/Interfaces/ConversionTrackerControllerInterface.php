<?php

namespace App\Http\Interfaces;

use App\Http\requests\setRequests\SetConversionTrackerRequest;

interface ConversionTrackerControllerInterface
{
    /**
     * @OA\Post(
     *     path="/set/setConversionTracking",
     *     operationId="/setConversionTracking",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name of conversion",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="Status of conversion",
     *         required=true,
     *         @OA\Schema(type="string", default="ENABLED", enum={"ENABLED","DISABLED","HIDDEN"})
     *     ),@OA\Parameter(
     *         name="category",
     *         in="query",
     *         description="Category of conversion",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),@OA\Parameter(
     *         name="currencyCode",
     *         in="query",
     *         description="Currency code of conversion",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),@OA\Parameter(
     *         name="trackerType",
     *         in="query",
     *         description="Tracker type of conversion",
     *         required=true,
     *         @OA\Schema(type="string", default="website", enum={"website", "phoneCall"})
     *     ),@OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Action operator",
     *         required=true,
     *         @OA\Schema(type="string", default="ADD", enum={"ADD", "SET"})
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the 200 and some data regarding the operation.",
     *     ),
     * )
     *
     * @param SetConversionTrackerRequest $request
     *
     * @return mixed
     */
    public function setConversionTracking(SetConversionTrackerRequest $request);
}
