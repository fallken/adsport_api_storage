<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetDynamicSearchAdsSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if ($this->has('suppliedUrlsOnly')) {
            $this->merge(['suppliedUrlsOnly' => $this->input('suppliedUrlsOnly') === "true"]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'campaignId' => 'required|string',
            'domainName' => 'required|string',
            'languageCode' => 'required|string',
            'pageFeed' => 'nullable|numeric',
            'suppliedUrlsOnly' => 'nullable|string',
        ];
    }
}
