<?php

use Illuminate\Support\Str;
use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class AdsTest extends TestCase
{
    protected $keywordRepo,$campaignRepo,$accountRepo;
    public function __construct(?string $name = null, array $data = [], string $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->keywordRepo = new \App\repos\KeywordsRepository();
        $this->campaignRepo = new \App\repos\CampaignsRepository();
        $this->accountRepo =  new \App\repos\CustomerAccountRepository();

    }
    /**
     * A basic test example.
     *
     * @return void
     */

    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testkeywordRename() {
        $result = $this->keywordRepo->changeKeywordName('261-911-0558'
                ,'64912739685'
                ,'354452660044'
                ,'نام جدید دام مرده'
        );
        print_r($result);
    }
    public function testAddKeyword() {
        $data=[
                'customerId'=>'541-403-9496','adgroupId'=>'71237816126'
                ,'name'=>'hopesdfPoasdfsdasdasafwer','status'=>'ENABLED'
            ,'finalUrl'=>null,'amount'=>null
            ];
        $result = $this->keywordRepo->addKeyword($data);
        print_r($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddCampaign()
    {
        $data = [
            'customerId' => "261-911-0558",
            'name' => "کمپین تست" . '-' . Str::random(),
            'microAmount' => 10000,
            'deliveryMethod' => "STANDARD",
            'adsChannelType' => "SEARCH",
            'status' => "ENABLED",
            'biddingStrategy' => "MANUAL_CPC"
        ];

        $result = $this->campaignRepo->addCampaign($data);

        $this->assertIsArray($result);

        print_r($result);
    }

    public function testAddCampaignSetting() {
        $data=[
                'customerId' => "261-911-0558",
                'campaignId'=>"1703779813",
                'positive'=>[
                    'ageRange'=>['503001'],
                    'language'=>['1064']
                ],
                'negative'=>[
                        'ageRange'=>['503005'],
                        'language'=>['1013']
                ]
        ];
        $test=json_encode($data['positive']);
        $result = $this->campaignRepo->addCampaignSetting($data);
        print_r($result);
    }

    public function testGetKeywordRecommendation() {
        $data=[
                'customerId'=>'261-911-0558',
                'adGroupId'=>'',
                'pageNumber'=>'',
                'keywords'=>['gosfand','ferrari']
        ];

        $result= $this->keywordRepo->getKeywordRecommendations($data);

        print_r($result);
    }

    public function testSendInvitation(){
        $data=[
                'customerId'=>'143-777-2692'
        ];

        $result = $this->accountRepo->sendInvitationLinkToCustomer($data);

        print_r($result);

        $this->assertJson($result);
    }

    public function testAddScheduel() {
        $schedules = [
                ['day'=>'MONDAY','start_hour'=>'9','end_hour'=>'17'],
                ['day'=>'SATURDAY','start_hour'=>'10','end_hour'=>'18'],
                ['day'=>'SUNDAY','start_hour'=>'11','end_hour'=>'20'],
        ];
        $data=[
                'customerId'=>'261-911-0558',
                'campaignId'=>'1749682734',
                'schedules'=>$schedules
        ];

        $result = $this->campaignRepo->addCampaignSchedule($data);

        print_r($result);

        $this->assertIsArray($result);
    }
}
