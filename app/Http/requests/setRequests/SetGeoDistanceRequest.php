<?php

namespace App\Http\requests;

use Anik\Form\FormRequest;

class SetGeoDistanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $data = @json_decode($this->input('data'));

        if (!$data) {
            $data = $this->input('data');
        }

        $this->merge(['data' => $data]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'data' => 'required|array',
        ];
    }
}
