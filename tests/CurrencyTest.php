<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CurrencyTest extends TestCase
{
    protected $currencyRepo;
    public function __construct(?string $name = null, array $data = [], string $dataName = '') {
        parent::__construct($name, $data, $dataName);
        $this->currencyRepo = new \App\repos\CurrencyRepository();
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetCurrencies() {
        $result = $this->currencyRepo->getCurrencies();
        print_r(json_decode($result,true));
    }
}
