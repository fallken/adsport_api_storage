<?php

namespace App\Copier\Abstracts;

use App\Helpers\RetryCallTrait;
use App\Interfaces\ResponseType;
use App\repos\AdWordsSettingsRepository;
use App\Copier\Interfaces\CopierBridgeInterface;

use Illuminate\Support\Str;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\Budget;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\AdGroup;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAd;
use Google\AdsApi\AdWords\v201809\cm\BudgetOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\ResponsiveSearchAd;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupCriterionOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionOperation;
use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSettingOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSettingOperation;

abstract class CopierBridgeAbstract implements CopierBridgeInterface
{
	use RetryCallTrait;

	/**
	 * @var string
	 */
	protected $sourceCustomerId;

	/**
	 * @var string
	 */
	private $sourceObjectId;

	/**
	 * @var string
	 */
	private $destinationObjectId;

	/**
	 * @var string
	 */
	private $objectId;

	/**
	 * @var string
	 */
	private $serviceType;

	/**
	 * @var string
	 */
	private $settingType;

	/**
	 * @var AdWordsSettingsRepository
	 */
	private $accountSettings;

	/**
	 * @var array
	 */
	private $results;

	/**
	 * CampaignTransferAbstract constructor.
	 *
	 * @param AdWordsSettingsRepository $accountSettings
	 */
	public function __construct(AdWordsSettingsRepository $accountSettings)
	{
		$this->accountSettings = $accountSettings;
	}

	/**
	 * Save old ids and new ids.
	 *
	 * @param string $type
	 * @param string $operation
	 * @param string $oldId
	 * @param string $newId
	 * @param string $status
	 * @param string $reason
	 */
	protected function setResults(string $type, string $operation, string $oldId, string $newId = '0', string $status = 'success', string $reason = ''): void
	{
		if (!isset($this->results[$type]))
		{
			$this->results[$type] = [];
		}

		$this->results[$type][] = [
			'oldId' => $oldId,
			'newId' => $newId,
			'status' => $status,
			'operation' => $operation,
			'reason' => $reason ? $reason : null,
		];
	}

	/**
	 * Set campaign and ad group criterion results.
	 *
	 * @param string $type
	 * @param string $rowType
	 * @param string $oldId
	 * @param string $newId
	 * @param string $status
	 * @param string $reason
	 */
	protected function setCriterionChildResults(string $type, string $rowType, string $oldId, string $newId, string $status = 'success', string $reason = ''): void
	{
		if (!isset($this->results[$type][count($this->results[$type]) - 1]['criterion']))
		{
			$this->results[$type][count($this->results[$type]) - 1]['criterion'] = [];
		}

		$this->results[$type][count($this->results[$type]) - 1]['criterion'][] = [
			'oldId' => $oldId,
			'newId' => $newId,
			'type' => $rowType,
			'status' => $status,
			'reason' => $reason ? $reason : null,
		];
	}

	/**
	 * Set campaign and ad group criterion results.
	 *
	 * @param string $type
	 * @param string $rowType
	 * @param string $oldFeedId
	 * @param string $newFeedId
	 * @param string $oldFeedItemId
	 * @param string $newFeedItemId
	 * @param string $status
	 * @param string $reason
	 */
	protected function setExtensionsChildResults(string $type, string $rowType, string $oldFeedId, string $newFeedId, string $oldFeedItemId, string $newFeedItemId, string $status = 'success', string $reason = ''): void
	{
		if (!isset($this->results[$type][count($this->results[$type]) - 1]['extensions']))
		{
			$this->results[$type][count($this->results[$type]) - 1]['extensions'] = [];
		}

		$this->results[$type][count($this->results[$type]) - 1]['extensions'][] = [
			'oldFeedId' => $oldFeedId,
			'newFeedId' => $newFeedId,
			'oldFeedItemId' => $oldFeedItemId,
			'newFeedItemId' => $newFeedItemId,
			'type' => $rowType,
			'status' => $status,
			'reason' => $reason ? $reason : null,
		];
	}

	/**
	 * Get response data of ids.
	 *
	 * @return array
	 */
	public function getResults(): array
	{
		return $this->results ?? [];
	}

	/**
	 * @param string $serviceType
	 * @param string $settingType
	 *
	 * @return CopierBridgeAbstract
	 */
	protected function setActionType(string $serviceType, string $settingType)
	{
		$this->serviceType = $serviceType;
		$this->settingType = $settingType;

		return $this;
	}

	/**
	 * Mutate budget on new account.
	 *
	 * @param $campaign
	 * @param $budgetService
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	private function mutateBudget($campaign, $budgetService)
	{
		$oldBudget = $campaign->getBudget();

		$money = new Money();
		$money->setMicroAmount($oldBudget->getAmount()->getMicroAmount());

		$budget = new Budget();

		$budget->setAmount($money);
		$budget->setStatus($oldBudget->getStatus());
		$budget->setName($oldBudget->getName() . Str::random(10));
		$budget->setDeliveryMethod($oldBudget->getDeliveryMethod());
		$budget->setIsExplicitlyShared($oldBudget->getIsExplicitlyShared());

		$operations = [];

		$operation = new BudgetOperation();
		$operation->setOperand($budget);
		$operation->setOperator(Operator::ADD);
		$operations[] = $operation;

		$successCallback = function () use ($budgetService, $operations, $oldBudget) {

			$result = $budgetService->mutate($operations)->getValue()[0];

			if ($result === false)
			{
				return false;
			}

			$this->setResults('budgets', 'set', $oldBudget->getBudgetId(), $result->getBudgetId());

			return $result;
		};

		$failCallback = function ($reason) use ($oldBudget) {

			$this->setResults('budgets', 'set', $oldBudget->getBudgetId(), 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Mutate campaign.
	 *
	 * @param Campaign $campaign
	 * @param object $campaignService
	 * @param $budgetService
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateCampaign($campaign, $campaignService, $budgetService)
	{
		if ($campaign->getEndDate() < date('Ymd', time()))
		{
			$this->setResults('campaigns', 'set', $campaign->getId(), 0, 'expired', 'Campaign is expired.');

			return false;
		}

		if ($campaign->getBiddingStrategyConfiguration()->getBiddingStrategyType() == "UNKNOWN")
		{
			$this->setResults('campaigns', 'set', $campaign->getId(), 0, 'failed', 'Campaign strategy is unknown.');

			return false;
		}

		$budget = $this->mutateBudget($campaign, $budgetService);

		if ($budget === false)
		{
			return false;
		}

		$campaign->setBudget($budget);
		$campaign->setStartDate(date('Ymd'));
		$campaign->setAdServingOptimizationStatus(null);
		$campaign->setName($campaign->getName() . date('Ymd-Hms'));

		$operation = new CampaignOperation();
		$operation->setOperand($campaign);
		$operation->setOperator(Operator::ADD);

		$operations = [$operation];

		$successCallback = function () use ($campaign, $campaignService, $operations) {

			$result = $campaignService->mutate($operations)->getValue()[0];

			if ($result === false)
			{
				return false;
			}

			$this->setResults('campaigns', 'set', $campaign->getId(), $this->destinationObjectId = $result->getId());

			return $result;
		};

		$failCallback = function ($reason) use ($campaign) {

			$this->setResults('campaigns', 'set', $campaign->getId(), 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Mutate ad group
	 *
	 * @param AdGroup $adGroup
	 * @param $adGroupService
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateAdGroup($adGroup, $adGroupService)
	{
		$adGroup->setCampaignId($this->objectId);

		$adGroup->setBiddingStrategyConfiguration($adGroup->getBiddingStrategyConfiguration()->setBiddingStrategyType(null));

		$operation = new AdGroupOperation();
		$operation->setOperand($adGroup);
		$operation->setOperator(Operator::ADD);

		$operations = [$operation];

		$successCallback = function () use ($adGroup, $adGroupService, $operations) {

			$result = $adGroupService->mutate($operations)->getValue()[0];

			if ($result === false)
			{
				return false;
			}

			$this->setResults('adGroups', 'set', $adGroup->getId(), $this->destinationObjectId = $result->getId());

			return $result;
		};

		$failCallback = function ($reason) use ($adGroup) {

			$this->setResults('adGroups', 'set', $adGroup->getId(), 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Mutate ads
	 *
	 * @param AdGroupAd $adGroupAd
	 * @param $newAdGroup
	 * @param $adService
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateAds($adGroupAd, $newAdGroup, $adService)
	{
		$adGroupAd->setAdGroupId($newAdGroup->getId());

		$ad = $adGroupAd->getAd();

		if ($ad instanceof ResponsiveSearchAd)
		{
			$headlines = [];

			foreach ($ad->getHeadlines() ?? [] as $headline)
			{
				$asset = $headline->getAsset();

				$asset->setAssetId(null);
				$headline->setAsset($asset);

				$headlines[] = $headline;
			}

			$ad->setHeadlines($headlines);

			$descriptions = [];

			foreach ($ad->getDescriptions() ?? [] as $description)
			{
				$asset = $description->getAsset();

				$asset->setAssetId(null);

				$descriptions[] = $description->setAsset($asset);
			}

			$ad->setDescriptions($descriptions);

			$adGroupAd->setAd($ad);
		}

		$operation = new AdGroupAdOperation();
		$operation->setOperand($adGroupAd);
		$operation->setOperator(Operator::ADD);

		$successCallback = function () use ($adGroupAd, $newAdGroup, $adService, $operation) {

			$result = $adService->mutate([$operation])->getValue()[0];

			if ($result === false)
			{
				return false;
			}

			$this->setResults('ads', 'set', $adGroupAd->getAd()->getId(), $result->getAd()->getId());

			return $result;
		};

		$failCallback = function ($reason) use ($adGroupAd) {

			$this->setResults('adGroups', 'set', $adGroupAd->getAd()->getId(), 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Mutate conversion tracking.
	 *
	 * @param $conversionTracker
	 * @param $conversionTrackerService
	 * \
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateConversionTracker($conversionTracker, $conversionTrackerService)
	{
		$conversionTracker->setName($conversionTracker->getName() . Str::random());

		$operation = new ConversionTrackerOperation();
		$operation->setOperand($conversionTracker);
		$operation->setOperator(Operator::ADD);

		$successCallback = function () use ($conversionTracker, $conversionTrackerService, $operation) {

			$result = $conversionTrackerService->mutate([$operation])->getValue()[0];

			if ($result === false)
			{
				return false;
			}

			$this->setResults('conversionTrackers', 'set', $conversionTracker->getId(), $result->getId());

			return $result;
		};

		$failCallback = function ($reason) use ($conversionTracker) {

			$this->setResults('conversionTrackers', 'set', $conversionTracker->getId(), 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Get campaign criterion.
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	private function getCriterion()
	{
		$predicates = [
			[
				'values' => [$this->sourceObjectId],
				'operator' => PredicateOperator::EQUALS,
				'field' => ucfirst($this->serviceType) . 'Id',
			]
		];

		$this->accountSettings->initialize($this->sourceCustomerId, $this->serviceType, $this->settingType);

		$successCallback = function () use ($predicates) {

			$result = $this->accountSettings->handle($predicates, ResponseType::RESPONSE_TYPE_PURE);

			if ($result === false)
			{
				return [];
			}

			return $result;
		};

		$failCallback = function ($reason) use ($predicates) {

			$this->setResults('Criterion', 'get', $this->sourceObjectId, 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Get campaign extensions.
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	private function getExtensions()
	{
		$predicates = [
			[
				'values' => [$this->sourceObjectId],
				'operator' => PredicateOperator::EQUALS,
				'field' => ucfirst($this->serviceType) . 'Id',
			]
		];

		$this->accountSettings->initialize($this->sourceCustomerId, $this->serviceType, $this->settingType);

		$successCallback = function () use ($predicates) {

			$result = $this->accountSettings->handle($predicates, ResponseType::RESPONSE_TYPE_PURE);

			if ($result === false)
			{
				return [];
			}

			return $result;
		};

		$failCallback = function ($reason) use ($predicates) {

			$this->setResults('extensions', 'get', $this->sourceObjectId, 0, 'failed', $reason);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Get campaign criterion.
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function getCampaignClonedCriterion(): array
	{
		$old = [];
		$operations = [];

		foreach ($this->getCriterion() as $setting)
		{
			//            /**
			//             * Ignore os version criterion, Because this type criterion is create automaticlly.
			//             */
			//            if ($setting->getCriterion()->getCriterionType() == 'OperatingSystemVersion') {
			//                continue;
			//            }

			$criterion = $setting->getCriterion();

			$old[] = (object)['id' => $criterion->getId(), 'type' => $setting->getCriterion()->getType()];

			$setting->setCampaignId($this->destinationObjectId);

			$operation = new CampaignCriterionOperation();

			$operation->setOperator(Operator::ADD);

			if ($setting->getCriterion()->getCriterionType() == 'Platform')
			{
				$operation->setOperator(Operator::SET);
			}

			$operation->setOperand($setting);

			$operations[] = $operation;
		}

		return ['operations' => $operations, 'old' => $old];
	}

	/**
	 * Mutate campaign and ad group extensions.
	 *
	 * @param $service
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateServiceExtensions($service): bool
	{
		foreach ($this->getExtensions() as $extension)
		{
			$extensions = $extension->getExtensionSetting()->getExtensions() ?? null;

			if ($extensions === null)
			{
				continue;
			}

			foreach ($extensions as $item)
			{
				$old = (object)['feedId' => $item->getFeedId(), 'feedItemId' => $item->getFeedItemId(), 'type' => $item->getFeedType()];

				$item->setFeedId(null);
				$item->setFeedItemId(null);

				if ($this->serviceType == static::SERVICE_CAMPAIGN)
				{
					$operation = new CampaignExtensionSettingOperation();
					$extension->setCampaignId($this->destinationObjectId);
				}
				else
				{
					$operation = new AdGroupExtensionSettingOperation();
					$extension->setAdGroupId($this->destinationObjectId);
				}

				$extension->getExtensionSetting()->setExtensions([$item]);

				$operation->setOperator(Operator::ADD);
				$operation->setOperand($extension);

				$successCallback = function () use ($service, $operation, $old) {

					$result = $service->mutate([$operation])->getValue()[0];

					if ($result === false)
					{
						return false;
					}

					$extension = $result->getExtensionSetting()->getExtensions()[0];

					$this->setExtensionsChildResults(
						$this->serviceType == 'campaign' ? 'campaigns' : 'adGroups',
						$old->type,
						$old->feedId,
						$old->feedItemId,
						$extension->getFeedId(),
						$extension->getFeedItemId()
					);

					return true;
				};

				$failCallback = function ($reason) use ($old) {

					$this->setExtensionsChildResults(
						$this->serviceType == 'campaign' ? 'campaigns' : 'adGroups',
						$old->type,
						$old->feedId,
						$old->feedItemId,
						'0',
						'0',
						'failed',
						$reason
					);

					return false;
				};

				$this->call($successCallback, 1, $failCallback);
			}
		}

		return true;
	}

	/**
	 * Get AdGroup criterion.
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function getAdGroupClonedCriterion(): array
	{
		$old = [];
		$operations = [];

		foreach ($this->getCriterion() as $setting)
		{
			$criterion = $setting->getCriterion();

			$old[] = (object)['id' => $criterion->getId(), 'type' => $setting->getCriterion()->getType()];

			$setting->setAdGroupId($this->destinationObjectId);

			$operation = new AdGroupCriterionOperation();

			$operation->setOperator(Operator::ADD);

			if ($setting->getCriterion()->getCriterionType() == 'Platform')
			{

				$operation->setOperator(Operator::SET);
			}

			$operation->setOperand($setting);

			$operations[] = $operation;
		}

		return ['operations' => $operations, 'old' => $old];
	}

	/**
	 * Mutate campaign criterion.
	 *
	 * @param $service
	 *
	 * @return mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	protected function mutateCriterion($service)
	{
		if ($this->serviceType == static::SERVICE_CAMPAIGN)
		{
			$criterion = $this->getCampaignClonedCriterion();
		}
		else
		{
			$criterion = $this->getAdGroupClonedCriterion();
		}

		if (count($criterion['operations']) == 0)
		{
			return null;
		}

		$successCallback = function () use ($service, $criterion) {

			$results = $service->mutate($criterion['operations'])->getValue();

			if ($results === false)
			{
				return false;
			}

			foreach ($results as $result)
			{
				$newCriterion = $result->getCriterion();

				foreach ($criterion['old'] as $item)
				{
					if ($item->id == $newCriterion->getId() && $item->type = $newCriterion->getType())
					{
						$this->setCriterionChildResults(
							$this->serviceType == 'campaign' ? 'campaigns' : 'adGroups',
							$item->type,
							$item->id,
							$newCriterion->getId()
						);
					}
				}
			}

			return $results;
		};

		$failCallback = function ($reason) {

			$this->setCriterionChildResults(
				$this->serviceType == 'campaign' ? 'campaigns' : 'adGroups',
				'unknown',
				'0',
				'0',
				'failed',
				$reason
			);

			return false;
		};

		return $this->call($successCallback, 1, $failCallback);
	}

	/**
	 * Set source customer id.
	 *
	 * @param string $customerId
	 */
	public function setSourceCustomerId(string $customerId)
	{
		$this->sourceCustomerId = $customerId;
	}

	/**
	 * Set source object id.
	 *
	 * @param string $objectId
	 */
	public function setSourceObjectId(string $objectId)
	{
		$this->sourceObjectId = $objectId;
	}

	/**
	 * Set source campaign id.
	 *
	 * @param string $objectId
	 */
	public function setObjectId(string $objectId)
	{
		$this->objectId = $objectId;
	}
}