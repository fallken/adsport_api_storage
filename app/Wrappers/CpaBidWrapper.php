<?php

namespace App\Wrappers;

class CpaBidWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $bid = $data->getBid();

        return (object)[
            'bidSource' => $data->getBidSource(),
            'bidsType' => $data->getBidsType(),

            'bid' => WrapperBridge::toObject($bid),
        ];
    }
}
