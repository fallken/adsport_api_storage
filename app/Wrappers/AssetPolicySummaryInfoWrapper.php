<?php

namespace App\Wrappers;

class AssetPolicySummaryInfoWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'assetPolicySummaryInfo' => PolicySummaryInfoWrapper::toObject($data),
        ];
    }
}
