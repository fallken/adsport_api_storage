<?php

namespace App\Http\Controllers;

use App\repos\AdGroupRepository;
use App\repos\CampaignsRepository;
use App\Exceptions\EndPointRequestException;
use App\Http\Interfaces\BidModifierControllerInterface;
use App\Http\Requests\addRequests\AddAdGroupPlatformBidModifierRequest;
use App\Http\Requests\addRequests\AddCampaignPlatformBidModifierRequest;
use App\Http\Requests\addRequests\AddCampaignLocationBidModifierRequest;

class BidModifierController extends Controller implements BidModifierControllerInterface
{
    /**
     * Add ad group platform bid modifier.
     *
     * @param AddAdGroupPlatformBidModifierRequest $request
     * @param AdGroupRepository $adGroupRepository
     * @return mixed
     * @throws EndPointRequestException
     */
    public function addAdGroupPlatformBidModifier(AddAdGroupPlatformBidModifierRequest $request, AdGroupRepository $adGroupRepository)
    {
        return $adGroupRepository->addPlatformBidModifier(
            $request->input('customerClientId'),
            $request->input('campaignId') ?? '',
            $request->input('adGroupId') ?? '',
            $request->input('operator'),
            $request->input('platformId'),
            $request->input('bidModifier')
        );
    }

    /**
     * Add campaign platform bid modifier.
     *
     * @param AddCampaignPlatformBidModifierRequest $request
     * @param CampaignsRepository $campaignsRepository
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function addCampaignPlatformBidModifier(AddCampaignPlatformBidModifierRequest $request, CampaignsRepository $campaignsRepository)
    {
        return $campaignsRepository->addPlatformBidModifier(
            $request->input('customerClientId'),
            $request->input('campaignId') ?? '',
            $request->input('platformId'),
            $request->input('bidModifier')
        );
    }

    /**
     * Add campaign location bid modifier.
     *
     * @param AddCampaignLocationBidModifierRequest $request
     * @param CampaignsRepository $campaignsRepository
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function addCampaignLocationBidModifier(AddCampaignLocationBidModifierRequest $request, CampaignsRepository $campaignsRepository)
    {
        return $campaignsRepository->addGeoBidModifier(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('locationId'),
            $request->input('bidModifier')
        );
    }
}
