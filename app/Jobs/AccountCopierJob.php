<?php

namespace App\Jobs;

use App\Models\Copier;
use App\Models\CopierLogs;
use App\Copier\Repositories\AccountCopierRepository;

use Exception;

class AccountCopierJob extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * @var Copier
     */
    private $copier;

    /**
     * Create a new job instance.
     *
     * @param int $copierId
     */
    public function __construct(int $copierId)
    {
        $this->copier = Copier::find($copierId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->copier) {
            return;
        }

        $accountCopier = new AccountCopierRepository();

        $accountCopier->setDebugMode(true);

        $accountCopier->setSourceCustomer($this->copier->source_id);
        $accountCopier->setDestinationCustomer($this->copier->destination_id);

        $accountCopier->initialize();

        $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_SUCCESS, 'message' => 'Started.']));

		if ($this->copier->parent_id)
		{
			$accountCopier->setResults($this->copier->results);
		}

		try
		{
            $this->copier->setAttribute('status', Copier::STATUS_RUNNING);
            $this->copier->save();

            $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_SUCCESS, 'message' => 'Handling...']));

            $accountCopier->handle();

            $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_SUCCESS, 'message' => 'Handled.']));

            $this->copier->setAttribute('status', Copier::STATUS_SUCCESS);

            $this->copier->save();
        }
        catch (Exception $exception) {

            $lastException = ['code' => $exception->getCode(), 'message' => $exception->getMessage(), 'file' => $exception->getFile(), 'line' => $exception->getLine()];

            $this->copier->setAttribute('last_exception', $lastException);
            $this->copier->setAttribute('status', Copier::STATUS_FAILED);

            $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_FAILED, 'exception' => $lastException]));
        }

        $results = $accountCopier->getResults();

        $this->copier->setAttribute('results', json_encode($results));

        $this->copier->save();

        dispatch(new CopierCallbackJob($this->copier->getAttribute('id')));
    }
}
