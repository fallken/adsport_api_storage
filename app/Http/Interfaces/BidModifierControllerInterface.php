<?php

namespace App\Http\Interfaces;

use App\repos\AdGroupRepository;
use App\repos\CampaignsRepository;
use App\Exceptions\EndPointRequestException;
use App\Http\Requests\addRequests\AddCampaignLocationBidModifierRequest;
use App\Http\Requests\addRequests\AddCampaignPlatformBidModifierRequest;
use App\Http\Requests\addRequests\AddAdGroupPlatformBidModifierRequest;

interface BidModifierControllerInterface
{
    /**
     * @OA\Post(
     *     path="/add/addAdGroupPlatformBidModifier",
     *     operationId="/addAdGroupPlatformBidModifier",
     *     tags={"Modifier operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=false,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="id of the corresponding adGroup",
     *         required=true,
     *         @OA\Schema(type="string", default="71237816126")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Operation operator",
     *         required=true,
     *         @OA\Schema(type="string", enum={"ADD", "SET"})
     *     )
     *     ,@OA\Parameter(
     *         name="platformId",
     *         in="query",
     *         description="The platform id",
     *         required=true,
     *         @OA\Schema(type="string", default="30002")
     *     )
     *     ,@OA\Parameter(
     *         name="bidModifier",
     *         in="query",
     *         description="The bid modifier value. Valid range is 0.1 to 10.0",
     *         required=true,
     *         @OA\Schema(type="string", default="7.2")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="return ids of the corresponding campaign and ad group.",
     *     ),
     * )
     * @param AddAdGroupPlatformBidModifierRequest $request
     * @param AdGroupRepository $adGroupRepository
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addAdGroupPlatformBidModifier(AddAdGroupPlatformBidModifierRequest $request, AdGroupRepository $adGroupRepository);

    /**
     * @OA\Post(
     *     path="/add/addCampaignPlatformBidModifier",
     *     operationId="/addCampaignPlatformBidModifier",
     *     tags={"Modifier operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=false,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="platformId",
     *         in="query",
     *         description="The platform id",
     *         required=true,
     *         @OA\Schema(type="string", default="30002")
     *     )
     *     ,@OA\Parameter(
     *         name="bidModifier",
     *         in="query",
     *         description="The bid modifier value. Valid range is 0.1 to 10.0",
     *         required=true,
     *         @OA\Schema(type="string", default="7.2")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="return id of the corresponding campaign.",
     *     ),
     * )
     *
     * @param AddCampaignPlatformBidModifierRequest $request
     * @param CampaignsRepository $campaignsRepository
     *
     * @return mixed
     *
     */
    public function addCampaignPlatformBidModifier(AddCampaignPlatformBidModifierRequest $request, CampaignsRepository $campaignsRepository);


    /**
     * @OA\Post(
     *     path="/add/addCampaignLocationBidModifier",
     *     operationId="/addCampaignLocationBidModifier",
     *     tags={"Modifier operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=false,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="locationId",
     *         in="query",
     *         description="The location id",
     *         required=true,
     *         @OA\Schema(type="string", default="1000133")
     *     )
     *     ,@OA\Parameter(
     *         name="bidModifier",
     *         in="query",
     *         description="The bid modifier value. Valid range is 0.1 to 10.0",
     *         required=true,
     *         @OA\Schema(type="string", default="1.2")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="return id of the corresponding campaign.",
     *     ),
     * )
     *
     * Add campaign location bid modifier.
     *
     * @param AddCampaignLocationBidModifierRequest $request
     * @param CampaignsRepository $campaignsRepository
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function addCampaignLocationBidModifier(AddCampaignLocationBidModifierRequest $request, CampaignsRepository $campaignsRepository);
}
