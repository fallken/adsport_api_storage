<?php

namespace App\Http\requests\Copier;

use Anik\Form\FormRequest;

class ShowCopierQueueRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'requestId' => 'required|numeric',
        ];
    }
}
