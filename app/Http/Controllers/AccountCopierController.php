<?php

namespace App\Http\Controllers;

use App\Jobs\CopierCallbackJob;
use App\Models\Copier;
use App\Jobs\AccountCopierJob;
use App\Exceptions\EndPointRequestException;
use App\Http\requests\Copier\HardCopierRequest;
use App\Http\requests\Copier\ShowCopierQueueRequest;
use App\Copier\Repositories\AccountCopierRepository;
use App\Http\requests\Copier\StoreCopierQueueRequest;
use App\Http\Interfaces\AccountCopierControllerInterface;

use Exception;
use Illuminate\Http\Request;

class AccountCopierController extends Controller implements AccountCopierControllerInterface
{
    protected $accountCopier;

    /**
     * AdGroupController constructor.
     */
    public function __construct()
    {
        $this->accountCopier = new AccountCopierRepository();
    }

    /**
     * Hot copy of copier.
     *
     * @param HardCopierRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function hardStore(HardCopierRequest $request)
    {
        $this->accountCopier->setDebugMode(true);

        $this->accountCopier->setSourceCustomer($request->input('sourceCustomerId'));
        $this->accountCopier->setDestinationCustomer($request->input('destinationCustomerId'));

        $this->accountCopier->initialize();

        try
        {
            $this->accountCopier->handle();
        }
        catch (Exception $exception)
        {
            throw new EndPointRequestException($exception->getMessage(), 400);
        }

        return $this->accountCopier->getResults();
    }

    /**
     * Store copier requests.
     *
     * @param StoreCopierQueueRequest $request
     *
     * @return array
     */
    public function store(StoreCopierQueueRequest $request)
    {
        if ($request->input('onlyResult') == 'yes')
        {
            dispatch(new CopierCallbackJob((int)$request->input('requestId')));
            return ['status' => 'dispatched'];
        }

        $copier = new Copier();

        $copier->setAttribute('source_id', $request->input('sourceId'));
        $copier->setAttribute('callback_url', $request->input('callbackUrl'));
        $copier->setAttribute('destination_id', $request->input('destinationId'));
        $copier->setAttribute('status', Copier::STATUS_PENDING);
        $copier->setAttribute('node_ip', $request->getClientIp());

        $id = $request->input('requestId');

        if ($id)
        {
            $parent = Copier::find($id);
            if ($parent)
            {
                $copier->setAttribute('parent_id', $parent->id);
            }
        }

        try
        {
            $copier->save();
        }
        catch (Exception $exception)
        {
            return response([
                'status' => 'failed',
                'message' => $exception->getMessage(),
            ], 500);
        }

        dispatch(new AccountCopierJob($copier->getAttribute('id')));

        return [
            'status' => 'success',
            'requestId' => $copier->getAttribute('id'),
        ];
    }

    /**
     * Show specified copier queue.
     *
     * @param ShowCopierQueueRequest $request
     *
     * @return object
     */
    public function show(ShowCopierQueueRequest $request)
    {
        $copier = Copier::find($request->input('requestId'));
        if ($copier)
        {
            return $copier;
        }

        return response([
            'status' => 'success',
            'message' => 'The request id not exists in our database.',
        ], 404);
    }

    /**
     * List of copier queues.
     *
     * @param Request $request
     *
     * @return array
     */
    public function index(Request $request)
    {
        return Copier::latest()->limit((int)$request->input('limit') ?? 25)->get();
    }
}
