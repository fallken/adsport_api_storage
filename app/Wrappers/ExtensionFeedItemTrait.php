<?php

namespace App\Wrappers;

trait ExtensionFeedItemTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function append(&$data)
    {
        $scheduling = $data->getScheduling();
        $devicePreference = $data->getDevicePreference();
        $campaignTargeting = $data->getCampaignTargeting();
        $adGroupTargeting = $data->getAdGroupTargeting();
        $keywordTargeting = $data->getKeywordTargeting();
        $getGeoTargeting = $data->getGeoTargeting();
        $geoTargetingRestriction = $data->getGeoTargetingRestriction();

        $summaries = null;
        $policySummaries = $data->getPolicySummaries();

        if (is_array($policySummaries)) {

            foreach ($policySummaries as $summary) {

                $summaries[] = WrapperBridge::toObject($summary);
            }
        }

        return [
            'policySummaries' => $summaries,

            'feedId' => $data->getFeedId(),
            'status' => $data->getStatus(),
            'endTime' => $data->getEndTime(),
            'feedType' => $data->getFeedType(),
            'startTime' => $data->getStartTime(),
            'feedItemId' => $data->getFeedItemId(),
            'extensionFeedItemType' => $data->getExtensionFeedItemType(),


            'scheduling' => WrapperBridge::toObject($scheduling),
            'geoTargeting' => WrapperBridge::toObject($getGeoTargeting),
            'adGroupTargeting' => WrapperBridge::toObject($adGroupTargeting),
            'devicePreference' => WrapperBridge::toObject($devicePreference),
            'keywordTargeting' => WrapperBridge::toObject($keywordTargeting),
            'campaignTargeting' => WrapperBridge::toObject($campaignTargeting),
            'geoTargetingRestriction' => WrapperBridge::toObject($geoTargetingRestriction),
        ];
    }
}