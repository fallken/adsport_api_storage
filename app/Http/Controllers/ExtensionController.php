<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 4/30/2019
 * Time: 5:14 PM
 */

namespace App\Http\Controllers;


use App\Http\Requests\addRequests\AddAdGroupCallExtensionRequest;
use App\interfaces\AdGroupInterface;
use App\interfaces\CampaignsInterface;
use Illuminate\Http\Request;

class ExtensionController extends Controller
{
    private $campaignRepo, $adGroupRepo;

    public function __construct(CampaignsInterface $campaignInterface, AdGroupInterface $adGroup)
    {
        $this->campaignRepo = $campaignInterface;
        $this->adGroupRepo = $adGroup;
    }


    /**
     * @OA\Post(
     *     path="/add/addAdgroupCallExtension",
     *     operationId="/addAdgroupCallExtension",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="261-911-0558"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="adgroup id",
     *         required=true,
     *         @OA\Schema(type="string",default="64912739685")
     *     ),
     *   @OA\Parameter(
     *         name="phoneNumber",
     *         in="query",
     *         description="accepts string",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *    @OA\Parameter(
     *         name="countryCode",
     *         in="query",
     *         description="accepts string",
     *         required=false,
     *         @OA\Schema(type="string",default="IRI")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="not tested yet",
     *     ),
     * )
     *
     * Add ad group call extension.
     *
     * @param AddAdGroupCallExtensionRequest $request
     *
     * @return mixed
     */
    public function addAdgroupCallExtension(AddAdGroupCallExtensionRequest $request)
    {
        return $this->adGroupRepo->addAdgroupCallExtension([
            'adGroupId' => $request->input('adGroupId'),
            'phoneNumber' => $request->input('phoneNumber'),
            'countryCode' => $request->input('countryCode'),
            'customerClientId' => $request->input('customerClientId'),
        ]);
    }
}