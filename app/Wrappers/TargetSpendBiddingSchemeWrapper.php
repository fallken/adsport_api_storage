<?php

namespace App\Wrappers;

class TargetSpendBiddingSchemeWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $bidCeiling = $data->getBidCeiling();
        $spendTarget = $data->getSpendTarget();

        return (object)[
            'bidCeiling' => WrapperBridge::toObject($bidCeiling),
            'spendTarget' => WrapperBridge::toObject($spendTarget),
        ];
    }
}
