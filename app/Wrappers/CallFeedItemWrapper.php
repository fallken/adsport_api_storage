<?php

namespace App\Wrappers;

class CallFeedItemWrapper implements AdWordsWrapperInterface
{
    use ExtensionFeedItemTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        // TODO: getCallConversionType = $data->getCallConversionType() // CallConversionType

        return (object)array_merge(static::append($data),
            [
                'callTracking' => $data->getCallTracking(),
                'callPhoneNumber' => $data->getCallPhoneNumber(),
                'callCountryCode' => $data->getCallCountryCode(),
                'extensionFeedItemType' => $data->getExtensionFeedItemType(),
                'disableCallConversionTracking' => $data->getDisableCallConversionTracking(),
            ]
        );
    }
}
