<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 4/6/2019
 * Time: 1:19 PM
 */

namespace App\interfaces;


interface CustomerAccountInterface
{
    /**
     * Set bidding strategy for portfolio.
     *
     * @param string $customerClientId
     * @param string $operator
     * @param string $name
     * @param string $status
     * @param string $strategyType
     * @param string $microAmount
     * @param string $bidCeilingAmount
     * @param string $bidFloorAmount
     * @param string $biddingStrategyId
     *
     * @return mixed
     */
    public function SetBiddingStrategy(string $customerClientId, string $operator, string $name, string $status, string $strategyType, string $microAmount, string $bidCeilingAmount = '', string $bidFloorAmount = '', string $biddingStrategyId = '');

    /**
     * @param $params
     * @return mixed
     * desc: create customer account using the provided information
     */
    public function createAccount($params);

    /**
     * @param $params
     * @return mixed
     * desc: this function will a budget order for the customer account with start , end and budget value for it .
     */
    public function setBudgetOrder($params);

    /**
     * @param $params
     * @return mixed
     * desc: returns the main budget and remaining customer budget by the customer id .
     */
    public function getCustomerBudget($params);

    /**
     * @param $params
     * @return mixed
     * desc:accepts a pedning invitation to link adwords account to a google manager account .
     */
    public function acceptAccountInvitationLink($params);

    /**
     * @param $params
     * @return mixed
     * desc:this function will send an invitation to the user to join to the manager account
     */
    public function sendInvitationLinkToCustomer($params);
}
