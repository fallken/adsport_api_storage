<?php

namespace App\Http\Requests\getRequests;

use Anik\Form\FormRequest;
use App\Exceptions\EndPointRequestException;

class GetAdWordsSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        $newPredicates = [];

        $predicates = @json_decode($this->input('predicates')) ?? [];

        foreach ($predicates as $predicate) {

            if (!is_array($predicate)) {
                $predicate = (array)$predicate;
            }

            foreach (['field', 'operator', 'values'] as $key) {

                if (!isset($predicate[$key]) || !$predicate[$key]) {
                    throw new EndPointRequestException("Bad predicate exception.", 422);
                }
            }

            if (!is_array($predicate['values'])) {
                throw new EndPointRequestException("Bad predicate exception.", 422);
            }

            $newPredicates[] = $predicate;
        }

        $this->merge(['predicates' => $newPredicates]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'objectId' => ['required', 'string'],
            'serviceType' => ['required', 'string'],
            'settingType' => ['required', 'string'],
            'predicates' => ['nullable', 'array', 'max:5'],
        ];
    }
}
