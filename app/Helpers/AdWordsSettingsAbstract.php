<?php

namespace App\Helpers;

use App\Exceptions\EndPointRequestException;
use App\interfaces\AdWordsSettingsInterface;

use Exception;
use LaravelGoogleAds\Services\AdWordsService;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\AdGroupCriterionService;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSettingService;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSettingService;

abstract class AdWordsSettingsAbstract implements AdWordsSettingsInterface
{
	use RetryCallTrait;

	protected const PAGE_LIMIT = 500;

	/**
	 * Base namespace of google ad words apis.
	 */
	private const SERVICE_CONFIGS = [
		'campaign' => [
			'extensions' => [
				'fields' => [
					'FeedId',
					'CampaignId',
					'Extensions',
					'ExtensionType',
					'PlatformRestrictions',
				],
				'class' => CampaignExtensionSettingService::class
			],
			'criterion' => [
				'fields' => [
					'Id',
					'CampaignId',
					'BidModifier',
					'PlatformName',
					'IsNegative',
					'BaseCampaignId',
					'CriteriaType',
					'CampaignCriterionStatus',
					'ChannelId',
					'ChannelName',
					'Parameter',
					'VerticalId',
					'VerticalParentId',
					'Path',
					'UserListEligibleForDisplay',
					'UserListEligibleForSearch',
					'UserListMembershipStatus',
					'UserListName',
					'UserListId',
					'UserInterestId',
					'UserInterestParentId',
					'UserInterestName',
					'FeedId',
					'MatchingFunction',
					'GeoPoint',
					'RadiusDistanceUnits',
					'RadiusInUnits',
					'Address',
					'Dimensions',
					'PlacementUrl',
					'ParentType',
					'OperatingSystemName',
					'OsMajorVersion',
					'OsMinorVersion',
					'OperatorType',
					'DeviceName',
					'ManufacturerName',
					'DeviceType',
					'AppId',
					'DisplayName',
					'MobileAppCategoryId',
					'LanguageCode',
					'LanguageName',
					'KeywordText',
					'KeywordMatchType',
					'IpAddress',
					'IncomeRangeType',
					'GenderType',
					'ContentLabelType',
					'CarrierName',
					'CarrierCountryCode',
					'AgeRangeType',
					'DayOfWeek',
					'StartHour',
					'StartMinute',
					'EndHour',
					'EndMinute',
				],
				'class' => CampaignCriterionService::class
			],

		],
		'adGroup' => [
			'extensions' => [
				'fields' => [
					'AdGroupId',
					'CampaignId',
					'Extensions',
					'ExtensionType',
					'PlatformRestrictions',
				],
				'class' => AdGroupExtensionSettingService::class,
			],
			'criterion' => [
				'fields' => [
					'AdGroupId',
					'CriterionUse',
					'Labels',
					'BaseCampaignId',
					'BaseAdGroupId',
					'Id',
					'CriteriaType',
					'AgeRangeType',
					'AppPaymentModelType',
					'CustomAffinityId',
					'CustomIntentId',
					'GenderType',
					'IncomeRangeType',
					'KeywordText',
					'KeywordMatchType',
					'MobileAppCategoryId',
					'AppId',
					'DisplayName',
					'ParentType',
					'PlacementUrl',
					'PartitionType',
					'CaseValue',
					'UserInterestId',
					'UserInterestName',
					'UserListId',
					'UserListName',
					'UserListMembershipStatus',
					'UserListEligibleForSearch',
					'UserListEligibleForDisplay',
					'VerticalId',
					'VerticalParentId',
					'Path',
					'Parameter',
					'CriteriaCoverage',
					'CriteriaSamples',
					'ChannelId',
					'ChannelName',
					'VideoId',
					'VideoName',
					'Status',
					'SystemServingStatus',
					'ApprovalStatus',
					'DisapprovalReasons',
					'FirstPageCpc',
					'TopOfPageCpc',
					'FirstPositionCpc',
					'BidModifier',
					'FinalUrls',
					'QualityScore',
					'FinalAppUrls',
					'FinalUrlSuffix',
					'FinalMobileUrls',
					'BiddingStrategy',
					'TrackingUrlTemplate',
					'UrlCustomParameters',
				],
				'class' => AdGroupCriterionService::class
			],
		]
	];

	/**
	 * The ad words service.
	 *
	 * @var object
	 */
	protected $service;

	/**
	 * The type of ad words service.
	 *
	 * @var string
	 */
	private $serviceType;

	/**
	 * The type of service setting.
	 *
	 * @var string
	 */
	private $settingType;

	/**
	 * Current object id.
	 *
	 * @var string
	 */
	private $currentObjectId;

	/**
	 * Make ad words service.
	 *
	 * @param string $customerClientId
	 * @param string $serviceType
	 * @param string $settingType
	 *
	 * @throws EndPointRequestException
	 */
	public function initialize(string $customerClientId, string $serviceType, string $settingType): void
	{
		$this->serviceType = $serviceType;
		$this->settingType = $settingType;

		try
		{
			$this->service = (new AdWordsService())->getService(
				(self::SERVICE_CONFIGS)[$serviceType][$settingType]['class'],
				$customerClientId
			);
		}
		catch (Exception $exception)
		{
			throw new EndPointRequestException($exception->getMessage(), 400, $exception);
		}
	}

	/**
	 * Get selector fields for specified object.
	 *
	 * @return array
	 */
	public function getDefaultFields(): array
	{
		return (self::SERVICE_CONFIGS)[$this->serviceType][$this->settingType]['fields'];
	}

	/**
	 * Generate default predicates.
	 *
	 * @return array
	 */
	public function generateDefaultPredicates(): array
	{
		if (!$this->currentObjectId)
		{
			return [];
		}

		return [
			[
				'field' => ucfirst($this->serviceType) . 'Id',
				'operator' => PredicateOperator::EQUALS,
				'values' => [$this->currentObjectId]
			]
		];
	}

	/**
	 * Set current object id.
	 *
	 * @param string $objectId
	 */
	public function setCurrentObjectId(string $objectId)
	{
		$this->currentObjectId = $objectId;
	}
}