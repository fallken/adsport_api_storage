<?php

namespace App\Http\Controllers;

use App\interfaces\CurrencyServiceInterface;

class CurrencyController extends Controller
{
    protected $currency;

    /**
     * CurrencyController constructor.
     * @param CurrencyServiceInterface $currency
     */
    public function __construct(CurrencyServiceInterface $currency)
    {
        $this->currency = $currency;
    }

    /**
     * @OA\Get(
     *     path="/get/getCurrencyPairs",
     *     operationId="/getCurrencyPairs",
     *     tags={"get operation"},
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),response="200",
     *      description="and structure of the response is json",
     *     ),
     * )
     * @return mixed
     */
    public function getCurrencyPairs() {
        $result=$this->currency->getCurrencies();
        return $result;
    }

    public function testify() {
        return 'its working like charm';
    }

}
