<?php

namespace App\Http\Middleware;

use Closure;
//use Illuminate\Support\Facades\Log;

class RequestLoggerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $data = [
//            'route' => $request->getPathInfo(),
//            'data' => $request->all(),
//        ];
//
//        Log::info("Request payload: " . json_encode($data));

        return $next($request);
    }
}
