<?php

namespace App\Wrappers;

class CampaignWrapper implements AdWordsWrapperInterface
{
    /**
     * @param $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $budget = $data->getBudget();
        $settings = $data->getSettings();
        $networkSetting = $data->getNetworkSetting();
        $urlCustomParameters = $data->getUrlCustomParameters();
        $selectiveOptimization = $data->getSelectiveOptimization();
        $biddingStrategyConfiguration = $data->getBiddingStrategyConfiguration();

        $campaign = (object)[
            'id' => $data->getId(),
            'name' => $data->getName(),
            'status' => $data->getStatus(),
            'labels' => $data->getLabels(),
            'endDate' => $data->getEndDate(),
            'startDate' => $data->getStartDate(),
            'servingStatus' => $data->getServingStatus(),
            'finalUrlSuffix' => $data->getFinalUrlSuffix(),
            'baseCampaignId' => $data->getBaseCampaignId(),
            'campaignGroupId' => $data->getCampaignGroupId(),
            'campaignTrialType' => $data->getCampaignTrialType(),
            'trackingUrlTemplate' => $data->getTrackingUrlTemplate(),
            'advertisingChannelType' => $data->getAdvertisingChannelType(),
            'advertisingChannelSubType' => $data->getAdvertisingChannelSubType(),
            'adServingOptimizationStatus' => $data->getAdServingOptimizationStatus(),

            'budget' => WrapperBridge::toObject($budget),
            'networkSetting' => WrapperBridge::toObject($networkSetting),
            'urlCustomParameters' => WrapperBridge::toObject($urlCustomParameters),
            'selectiveOptimization' => WrapperBridge::toObject($selectiveOptimization),
            'biddingStrategyConfiguration' => WrapperBridge::toObject($biddingStrategyConfiguration),
        ];

        $campaign->settings = [];

        foreach ($settings as $setting) {
            $campaign->settings[] = WrapperBridge::toObject($setting);
        }

        return $campaign;
    }
}
