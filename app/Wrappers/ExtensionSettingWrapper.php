<?php

namespace App\Wrappers;

class ExtensionSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $extensions = [];

        if (!is_array($data->getExtensions())) {
            return null;
        }

        foreach ($data->getExtensions() as $extension) {

            $extensions[] = WrapperBridge::toObject($extension);
        }

        return (object)[
            'extensions' => $extensions,
            'platformRestrictions' => $data->getPlatformRestrictions(),
        ];
    }
}
