<?php

namespace App\Wrappers;

class AdGroupWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $settings = null;
        $settingsOriginal = $data->getSettings();

        if (is_array($settingsOriginal)) {

            $settings = [];

            foreach ($settingsOriginal as $setting) {

                $settings[] = WrapperBridge::toObject($setting);
            }
        }

        $labels = null;
        $labelsOriginal = $data->getLabels();

        if (is_array($labelsOriginal)) {

            $labels = [];

            foreach ($labelsOriginal as $label) {

                $labels[] = WrapperBridge::toObject($label);
            }
        }

        $urlCustomParameters = $data->getUrlCustomParameters();
        $adGroupAdRotationMode = $data->getAdGroupAdRotationMode();
        $biddingStrategyConfiguration = $data->getBiddingStrategyConfiguration();

        return (object)[
            'id' => $data->getId(),
            'name' => $data->getName(),
            'status' => $data->getStatus(),
            'campaignId' => $data->getCampaignId(),
            'adGroupType' => $data->getAdGroupType(),
            'campaignName' => $data->getCampaignName(),
            'baseAdGroupId' => $data->getBaseAdGroupId(),
            'baseCampaignId' => $data->getBaseCampaignId(),
            'finalUrlSuffix' => $data->getFinalUrlSuffix(),
            'trackingUrlTemplate' => $data->getTrackingUrlTemplate(),
            'contentBidCriterionTypeGroup' => $data->getContentBidCriterionTypeGroup(),

            'labels' => $labels,
            'settings' => $settings,
            'urlCustomParameters' => WrapperBridge::toObject($urlCustomParameters),
            'adGroupAdRotationMode' => WrapperBridge::toObject($adGroupAdRotationMode),
            'biddingStrategyConfiguration' => WrapperBridge::toObject($biddingStrategyConfiguration),
        ];
    }
}
