<?php

namespace App\Wrappers;

class NegativeAdGroupCriterionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        return (object)[
            'negativeAdGroupCriterion' => AdGroupCriterionWrapper::toObject($data),
        ];
    }
}
