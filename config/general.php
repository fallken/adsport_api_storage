<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 5/26/2019
 * Time: 10:23 AM
 */

return [
    'campaignSettingOperators' => [
        'ageRange' => 'ADD',
        'language' => 'ADD',
        'platform' => 'SET',
        'gender' => 'ADD'
    ],
    'proxy_host' => env('SOAP_PROXY_HOST', 'localhost'), // The host without protocol( http, https and ...), For example: my-proxy.local
    'proxy_port' => env('SOAP_PROXY_PORT', '1080') // Only set port number.
];