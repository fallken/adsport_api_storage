<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class SetCampaignExtensionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $extensions = @json_decode($this->input('extensions'));

        if (!$extensions) {
            $extensions = $this->input('extensions');
        }

        $this->merge(['extensions' => $extensions]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'campaignId' => ['required', 'string'],
            'extensions' => ['required', 'array', 'max:50', 'min:1']
        ];
    }
}
