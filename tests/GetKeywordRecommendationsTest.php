<?php

use App\repos\KeywordsRepository;

class GetKeywordRecommendationsTest extends TestCase
{
    /**
     * @var KeywordsRepository
     */
    private $keywordRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->keywordRepository = new KeywordsRepository();
    }

    /**
     */
    public function testGetKeywordRecommendations()
    {
        $data = [
            'type' => 'STATS',
            'pageNumber' => 0,
            'keywords' => ['Gulf'],
            'customerId' => '541-403-9496',
        ];

        $result = $this->keywordRepository->getKeywordRecommendations($data);

        $this->assertNotNull($result);

        $this->assertStringContainsString('keyword', $result);
    }
}
