<?php

namespace App\Wrappers;

class NegativeCampaignCriterionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        return CampaignCriterionWrapper::toObject($data);
    }
}
