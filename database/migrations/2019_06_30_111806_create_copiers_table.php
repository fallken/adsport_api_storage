<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copiers', function (Blueprint $table) {

            $table->engine = 'innodb';

            $table->bigIncrements('id');

            $table->string('parent_id')->nullable();

            $table->string('source_id')->index();
            $table->string('destination_id')->index();

            $table->ipAddress('node_ip');

            $table->enum('status', ['pending', 'running', 'success', 'canceled', 'failed', 'done']);

            $table->string('callback_url', 500)->nullable();

            $table->longText('results')->nullable();

            $table->longText('last_exception')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copiers');
    }
}
