<?php

namespace App\Copier\Repositories;

use App\repos\AdWordsSettingsRepository;
use App\Copier\Interfaces\CopierBridgeTypes;
use App\Copier\Abstracts\CopierBridgeAbstract;

use LaravelGoogleAds\Services\AdWordsService;
use Google\AdsApi\AdWords\v201809\cm\BudgetService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupService;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupCriterionService;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;
use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSettingService;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSettingService;

class CopierBridgeRepository extends CopierBridgeAbstract
{
    /**
     * Budget service.
     */
    private $budgetService;

    /**
     * Campaign services.
     */
    private $campaignService;
    private $campaignCriterionService;
    private $campaignExtensionService;

    /**
     * Ad group services.
     */
    private $adGroupService;
    private $adGroupCriterionService;
    private $adGroupExtensionService;

    /**
     * Ad group ad service.
     */
    private $adService;

    /**
     * Conversion tracker service.
     */
    private $conversionTrackerService;

    /**
     * CopierBridgeRepository constructor.
     */
    public function __construct()
    {
        parent::__construct(new AdWordsSettingsRepository());
    }

    public function initialize(string $destinationCustomerId): void
    {
        /**
         * Create ad words service.
         */
        $adWordsService = new AdWordsService();

        /**
         * Create budget service.
         */
        $this->budgetService = $adWordsService->getService(BudgetService::class, $destinationCustomerId);

        /**
         * Create campaign services.
         */
        $this->campaignService = $adWordsService->getService(CampaignService::class, $destinationCustomerId);
        $this->campaignCriterionService = $adWordsService->getService(CampaignCriterionService::class, $destinationCustomerId);
        $this->campaignExtensionService = $adWordsService->getService(CampaignExtensionSettingService::class, $destinationCustomerId);

        /**
         * Create ad group services.
         */
        $this->adGroupService = $adWordsService->getService(AdGroupService::class, $destinationCustomerId);
        $this->adGroupCriterionService = $adWordsService->getService(AdGroupCriterionService::class, $destinationCustomerId);
        $this->adGroupExtensionService = $adWordsService->getService(AdGroupExtensionSettingService::class, $destinationCustomerId);

        /**
         * Create ad group ad service.
         */
        $this->adService = $adWordsService->getService(AdGroupAdService::class, $destinationCustomerId);

        /**
         * Create conversion tracking service.
         */
        $this->conversionTrackerService = $adWordsService->getService(ConversionTrackerService::class, $destinationCustomerId);
    }

    /**
     * Handle campaign settings transfer process.
     *
     * @param $campaign
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function handleCampaigns($campaign)
    {
        $newCampaign = $this->mutateCampaign($campaign, $this->campaignService, $this->budgetService);

        if ($newCampaign === false) {
            return false;
        }

        $this->setActionType(CopierBridgeTypes::SERVICE_CAMPAIGN, CopierBridgeTypes::SETTING_CRITERION)
            ->mutateCriterion($this->campaignCriterionService);

        $this->setActionType(CopierBridgeTypes::SERVICE_CAMPAIGN, CopierBridgeTypes::SETTING_EXTENSIONS)
            ->mutateServiceExtensions($this->campaignExtensionService);

        return $newCampaign;
    }

    /**
     * Handle campaign settings transfer process.
     *
     * @param $adGroup
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function handleAdGroups($adGroup)
    {
        $newAdGroup = $this->mutateAdGroup($adGroup, $this->adGroupService);

        if ($newAdGroup === false) {
            return false;
        }

        $this->setActionType(CopierBridgeTypes::SERVICE_AD_GROUP, CopierBridgeTypes::SETTING_CRITERION)
            ->mutateCriterion($this->adGroupCriterionService);

        $this->setActionType(CopierBridgeTypes::SERVICE_AD_GROUP, CopierBridgeTypes::SETTING_EXTENSIONS)
            ->mutateServiceExtensions($this->adGroupExtensionService);

        return $newAdGroup;
    }

    /**
     * Handle ad group ads transfer process.
     *
     * @param $adGroupAd
     * @param $newAdGroup
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function handleAds($adGroupAd, $newAdGroup)
    {
        $adGroupAd = $this->mutateAds($adGroupAd, $newAdGroup, $this->adService);

        if ($adGroupAd === false) {
            return false;
        }

        return $adGroupAd;
    }

    /**
     * Handle conversion tracker.
     *
     * @param $conversionTracker
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function handleConversionTracker($conversionTracker)
    {
        $newConversionTracker = $this->mutateConversionTracker($conversionTracker, $this->conversionTrackerService);

        if ($newConversionTracker === false) {
            return false;
        }

        return $newConversionTracker;
    }
}
