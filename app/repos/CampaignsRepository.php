<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 9:17 AM
 */

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\Wrappers\WrapperBridge;
use App\interfaces\CampaignsInterface;
use App\Exceptions\EndPointRequestException;

use Exception;
use Google\AdsApi\AdWords\v201809\cm\Address;
use Google\AdsApi\AdWords\v201809\cm\AdSchedule;
use Google\AdsApi\AdWords\v201809\cm\AdvertisingChannelSubType;
use Google\AdsApi\AdWords\v201809\cm\AdvertisingChannelType;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItem;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItemAppStore;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\Budget;
use Google\AdsApi\AdWords\v201809\cm\BudgetOperation;
use Google\AdsApi\AdWords\v201809\cm\BudgetService;
use Google\AdsApi\AdWords\v201809\cm\CalloutFeedItem;
use Google\AdsApi\AdWords\v201809\cm\Campaign;
use Google\AdsApi\AdWords\v201809\cm\CampaignBidModifier;
use Google\AdsApi\AdWords\v201809\cm\CampaignBidModifierOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterion;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignCriterionService;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSetting;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSettingOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignExtensionSettingService;
use Google\AdsApi\AdWords\v201809\cm\CampaignOperation;
use Google\AdsApi\AdWords\v201809\cm\CampaignService;
use Google\AdsApi\AdWords\v201809\cm\CustomParameter;
use Google\AdsApi\AdWords\v201809\cm\CustomParameters;
use Google\AdsApi\AdWords\v201809\cm\DynamicSearchAdsSetting;
use Google\AdsApi\AdWords\v201809\cm\ExtensionSetting;
use Google\AdsApi\AdWords\v201809\cm\FeedType;
use Google\AdsApi\AdWords\v201809\cm\GeoPoint;
use Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSetting;
use Google\AdsApi\AdWords\v201809\cm\GeoTargetTypeSettingNegativeGeoTargetType;
use Google\AdsApi\AdWords\v201809\cm\Location;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\NetworkSetting;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\PageFeed;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Platform;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\Proximity;
use Google\AdsApi\AdWords\v201809\cm\ProximityDistanceUnits;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SitelinkFeedItem;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\cm\TargetCpaBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\UniversalAppBiddingStrategyGoalType;
use Google\AdsApi\AdWords\v201809\cm\UniversalAppCampaignSetting;
use Google\AdsApi\AdWords\v201809\cm\UrlList;
use Illuminate\Http\Request;
use LaravelGoogleAds\Services\AdWordsService;

class CampaignsRepository implements CampaignsInterface
{
    use RetryCallTrait;
    /**
     * @var
     */
    protected $adWordsService, $oAuth2Credential;

    /**
     * @var int
     */
    const PAGE_LIMIT = 500;

    /**
     * CampaignsRepository constructor.
     */
    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * Get campaigns of specified customer.
     *
     * @param string $customerClientId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function getCampaigns(string $customerClientId, array $predicates = [], string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaignFields = [
            'Id',
            'CampaignGroupId',
            'Name',
            'Status',
            'ServingStatus',
            'StartDate',
            'EndDate',
            'BudgetId',
            'BudgetName',
            'Amount',
            'DeliveryMethod',
            'BudgetReferenceCount',
            'IsBudgetExplicitlyShared',
            'BudgetStatus',

            'AdServingOptimizationStatus',
            'Settings',

            'BiddingStrategy',
            'TargetRoas',
            'TargetRoasBidCeiling',
            'TargetRoasBidFloor',

            'BiddingStrategyId',
            'BiddingStrategyName',
            'BiddingStrategyType',

            'TargetSpendBidCeiling',
            'TargetSpendSpendTarget',

            'ShoppingSetting',
            'GeoTargetTypeSetting',
            'RealTimeBiddingSetting',

            'EnhancedCpcEnabled',
            'ViewableCpmEnabled',
            'BidCeiling',
            'TargetCpa',
            'TargetCpaMaxCpcBidCeiling',
            'TargetCpaMaxCpcBidFloor',

            'AdvertisingChannelType',
            'AdvertisingChannelSubType',
            'Labels',
            'CampaignTrialType',
            'BaseCampaignId',
            'FinalUrlSuffix',
            'TrackingUrlTemplate',
            'UrlCustomParameters',
            'SelectiveOptimization',

            'TargetGoogleSearch',
            'TargetSearchNetwork',
            'TargetContentNetwork',
            'TargetPartnerSearchNetwork',

            'Level',
            'TimeUnit',
            'FrequencyCapMaxImpressions',

            'MaximizeConversionValueTargetRoas',
            'BiddingStrategyGoalType',
            'AppId',
            'AppVendor',
            'VanityPharmaDisplayUrlMode',
            'VanityPharmaText',

            'Eligible',
            'RejectionReasons',
        ];

        $selector = new Selector();

        $selector->setFields($campaignFields);

        $selectorPredicates = [
//            (object)['field' => 'Id', 'operator' => 'EQUALS', 'values' => [2045454855]]
        ];

        foreach ($predicates as $predicate) {

            $predicate = (object)$predicate;

            $selectorPredicates[] = new Predicate($predicate->field, $predicate->operator, $predicate->values);
        }

        $selector->setPredicates($selectorPredicates);

        $selector->setOrdering([
            new OrderBy('Name', SortOrder::ASCENDING)
        ]);

        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        $page = $campaignService->get($selector);

        $entries = $page->getEntries() ?? [];

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        $results = [];

        foreach ($entries as $entry) {

            $results[] = WrapperBridge::toObject($entry);
        }

        return response()->json($results);
    }

    /**
     * Add site link extension to specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $links
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addSiteLinkExtension(string $customerClientId, string $campaignId, array $links, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $siteLinks = [];

        foreach ($links as $link) {

            try {
                $link = (object)$link;

                if (!$link->text || !$link->url) {

                    throw new EndPointRequestException('Bad site link exception.', 400);
                }
            }
            catch (Exception $exception) {

                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $siteLink = new SitelinkFeedItem();
            $siteLink->setSitelinkText($link->text);

            try {

                $siteLink->setSitelinkFinalUrls(
                    new UrlList([$link->url])
                );
            }
            catch (Exception $exception) {

                throw new EndPointRequestException("Bad link submitted.", 400);
            }

            if ($link->feedId) {
                $siteLink->setFeedId($link->feedId);
            }

            $siteLinks[] = $siteLink;
        }

        $campaignExtensionSetting = new CampaignExtensionSetting();
        $campaignExtensionSetting->setCampaignId($campaignId);
        $campaignExtensionSetting->setExtensionType(FeedType::SITELINK);
        $campaignExtensionSetting->setExtensionSetting(new ExtensionSetting());
        $campaignExtensionSetting->getExtensionSetting()->setExtensions($siteLinks);

        $operation = new CampaignExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($campaignExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $operations = [$operation];

        $extensionService = $this->adWordsService->getService(CampaignExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operations) {
            return $extensionService->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'campaignId' => $result->getCampaignId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * * Set campaign extensions.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $extensions
     * @param string $responseType
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setExtensions(string $customerClientId, string $campaignId, array $extensions, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $operations = [];

        foreach ($extensions as $extension) {

            try {
                $extension = (object)$extension;

                if (!$extension->type) {
                    throw new EndPointRequestException('Bad extension type exception.', 400);
                }

                if (!$extension->operator) {
                    $extension->operator = Operator::ADD;
                }
            }
            catch (Exception $exception) {
                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            switch ($extension->type) {
                case 'app':
                    $operations[] = $this->addAppExtension('', $campaignId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
                case 'callout':
                    $operations[] = $this->addCalloutExtension('', $campaignId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
                case 'sitelink':
                    $operations[] = $this->addSiteLinkExtension('', $campaignId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
            }
        }

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operations;
        }

        $extensionService = $this->adWordsService->getService(CampaignExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operations) {
            return $extensionService->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'campaignId' => $result->getCampaignId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * Add callout extension to specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $callouts
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCalloutExtension(string $customerClientId, string $campaignId, array $callouts, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $feedCallouts = [];

        foreach ($callouts as $callout) {

            try {
                $callout = (object)$callout;

                if (!$callout->value) {

                    throw new EndPointRequestException('Bad callout value exception.', 400);
                }
            }
            catch (Exception $exception) {

                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $feed = new CalloutFeedItem();
            $feed->setCalloutText($callout->value);

            if ($callout->feedId) {
                $feed->setFeedId($callout->feedId);
            }

            $feedCallouts[] = $feed;
        }

        $campaignExtensionSetting = new CampaignExtensionSetting();
        $campaignExtensionSetting->setCampaignId($campaignId);
        $campaignExtensionSetting->setExtensionType(FeedType::CALLOUT);

        $extensionSetting = new ExtensionSetting();
        $extensionSetting->setExtensions($feedCallouts);
        $campaignExtensionSetting->setExtensionSetting($extensionSetting);

        $operation = new CampaignExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($campaignExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $operations = [$operation];

        $results = [];

        $extensionService = $this->adWordsService->getService(CampaignExtensionSettingService::class, $customerClientId);

        $values = $this->call(function () use ($extensionService, $operations) {
            return $extensionService->mutate($operations)->getValue();
        }, 1);

        foreach ($values as $index => $extensionSetting) {

            $results[$index]['campaignId'] = $extensionSetting->getCampaignId();
            $results[$index]['extensionType'] = $extensionSetting->getExtensionType();
        }

        return response()->json($results);
    }

    /**
     * Add callout extension to specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $apps
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addAppExtension(string $customerClientId, string $campaignId, array $apps, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $appsItems = [];

        foreach ($apps as $app) {

            try {
                $app = (object)$app;

                if (!$app->appId || !$app->store || !$app->text || !$app->link) {

                    throw new EndPointRequestException('Bad app extension exception.', 400);
                }
            }
            catch (Exception $exception) {

                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $app->store = (int)$app->store;

            if ($app->store === 1) {

                $app->store = AppFeedItemAppStore::GOOGLE_PLAY;
            }
            else if ($app->store === 2) {

                $app->store = AppFeedItemAppStore::APPLE_ITUNES;
            }
            else {
                throw new EndPointRequestException('Invalid app store exception.', 400);
            }

            $appFeedItem = new AppFeedItem();
            $appFeedItem->setAppId($app->appId);
            $appFeedItem->setAppStore($app->store);
            $appFeedItem->setAppLinkText($app->text);

            try {
                $appFeedItem->setAppFinalUrls(new UrlList([$app->link]));
            }
            catch (Exception $exception) {
                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            if ($app->feedId) {
                $appFeedItem->setFeedId($app->feedId);
            }

            $appsItems[] = $appFeedItem;
        }

        $campaignExtensionSetting = new CampaignExtensionSetting();
        $campaignExtensionSetting->setCampaignId($campaignId);
        $campaignExtensionSetting->setExtensionType(FeedType::APP);

        $extensionSetting = new ExtensionSetting();
        $extensionSetting->setExtensions($appsItems);
        $campaignExtensionSetting->setExtensionSetting($extensionSetting);

        $operation = new CampaignExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($campaignExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $operations = [$operation];

        $results = [];

        $extensionService = $this->adWordsService->getService(CampaignExtensionSettingService::class, $customerClientId);

        $values = $this->call(function () use ($extensionService, $operations) {
            return $extensionService->mutate($operations)->getValue();
        }, 1);

        foreach ($values as $index => $extensionSetting) {

            $results[$index]['campaignId'] = $extensionSetting->getCampaignId();
            $results[$index]['extensionType'] = $extensionSetting->getExtensionType();
        }

        return response()->json($results);
    }

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $platformId
     * @param float $bidModifier
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addPlatformBidModifier(string $customerClientId, string $campaignId, string $platformId, float $bidModifier)
    {
        $service = $this->adWordsService->getService(CampaignCriterionService::class, $customerClientId);

        $campaignBidModifier = new CampaignBidModifier();
        $campaignBidModifier->setCampaignId($campaignId);
        $campaignBidModifier->setCriterion(new Platform($platformId));
        $campaignBidModifier->setBidModifier($bidModifier);

        $operations = [];

        $operation = new CampaignBidModifierOperation();
        $operation->setOperand($campaignBidModifier);
        $operation->setOperator(Operator::SET);

        $operations[] = $operation;

        $result = $this->call(function () use ($service, $operations) {
            return $service->mutate($operations);
        }, 1)->getValue()[0];

        return [
            'id' => $result->getCriterion()->getId(),
            'campaignId' => $result->getCampaignId(),
            'platformName' => $result->getCriterion()->getPlatformName(),
        ];
    }

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $locationId
     * @param float $bidModifier
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addGeoBidModifier(string $customerClientId, string $campaignId, string $locationId, float $bidModifier)
    {
        $service = $this->adWordsService->getService(CampaignCriterionService::class, $customerClientId);

        $location = new Location($locationId);

        $campaignBidModifier = new CampaignBidModifier();
        $campaignBidModifier->setCampaignId($campaignId);
        $campaignBidModifier->setCriterion($location);
        $campaignBidModifier->setBidModifier($bidModifier);

        $operations = [];

        $operation = new CampaignBidModifierOperation();
        $operation->setOperand($campaignBidModifier);
        $operation->setOperator(Operator::ADD);

        $operations[] = $operation;

        $result = $this->call(function () use ($service, $operations) {
            return $service->mutate($operations);
        }, 1)->getValue()[0];

        return [
            'id' => $result->getCriterion()->getId(),
            'campaignId' => $result->getCampaignId(),
            'locationName' => $result->getCriterion()->getLocationName(),
        ];
    }

    /**
     * Set network setting for specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param bool|null $contentNetwork
     * @param bool|null $partnerSearchNetwork
     * @param bool|null $searchNetwork
     * @param bool|null $googleSearch
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setNetworkSetting(string $customerClientId, string $campaignId, $contentNetwork, $searchNetwork, $googleSearch, $partnerSearchNetwork)
    {
        $service = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $target = new NetworkSetting();

        if ($contentNetwork !== null) {
            $target->setTargetContentNetwork($contentNetwork);
        }

        if ($contentNetwork !== null) {

            $target->setTargetGoogleSearch($googleSearch);
        }

        if ($contentNetwork !== null) {
            $target->setTargetPartnerSearchNetwork($partnerSearchNetwork);
        }

        if ($contentNetwork !== null) {
            $target->setTargetSearchNetwork($searchNetwork);
        }

        $campaign = new Campaign($campaignId);

        $campaign->setNetworkSetting($target);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::SET);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation]);
        }, 1)->getValue()[0];

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * Set tracking url template.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $trackingUrlTemplate
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setTrackingUrlTemplate(string $customerClientId, string $campaignId, string $trackingUrlTemplate)
    {
        $service = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaign = new Campaign();
        $campaign->setId($campaignId);
        $campaign->setTrackingUrlTemplate($trackingUrlTemplate);

        $operation = new CampaignOperation();
        $operation->setOperator(Operator::SET);
        $operation->setOperand($campaign);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * Set url custom parameters.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $urlCustomParameters
     * @param bool $doReplace
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setUrlCustomerParameters(string $customerClientId, string $campaignId, array $urlCustomParameters, $doReplace = null, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $parameters = [];

        foreach ($urlCustomParameters as $item) {

            $parameter = new CustomParameter();

            $parameter->setKey($item->key);
            $parameter->setValue($item->value);
            $parameter->setIsRemove($item->isRemoved ?? false);

            $parameters[] = $parameter;
        }

        $customParameters = new CustomParameters($parameters);
        $customParameters->setDoReplace($doReplace);

        if ($responseType == static::RESPONSE_TYPE_INSTANCE) {
            return $customParameters;
        }

        $campaign = new Campaign();
        $campaign->setId($campaignId);
        $campaign->setUrlCustomParameters($customParameters);

        $operation = new CampaignOperation();
        $operation->setOperator(Operator::SET);
        $operation->setOperand($campaign);

        $service = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * Set bidding strategy for portfolio.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $finalUrlSuffix
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setFinalUrlSuffix(string $customerClientId, string $campaignId, string $finalUrlSuffix)
    {
        $service = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaign = new Campaign();

        $campaign->setId($campaignId);

        $campaign->setFinalUrlSuffix($finalUrlSuffix);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::SET);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation]);
        }, 1)->getValue()[0];

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
            'status' => $result->getStatus(),
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function changeCampaignStatus(Request $request)
    {
        $customerClientId = $request->customerId;
        $campaignId = $request->campaignId;

        $adGroupCriterionService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $operations = [];
        // Create a campaign with PAUSED status.
        $campaign = new Campaign();
        $campaign->setId($campaignId);
        $campaign->setStatus($request->status);

//        $campaign->setStatus();

        // Create a campaign operation and add it to the list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operator = $request->status == 'REMOVED' ? 'REMOVE' : 'SET';
        $operation->setOperator($operator);
//        $operation->setOperator(Operator::SET);
        $operations[] = $operation;

        // Update the campaign on the server.

        try {
            $result = $adGroupCriterionService->mutate($operations);

            $campaign = $result->getValue()[0];
            $resultArr = [
                'status' => $campaign->getStatus(),
                'settings' => $campaign->getSettings(),
                'campaignId' => $campaign->getCampaignGroupId(),
                'campaignName' => $campaign->getName()
            ];

            return response()->json($resultArr, 200);
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeCampaignName(Request $request)
    {
        $customerClientId = $request->customerId;
        $campaignId = $request->campaignId;
        $name = $request->name;

        $adGroupCriterionService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $operations = [];
        // Create a campaign with PAUSED status.
        $campaign = new Campaign();
        $campaign->setId($campaignId);
        $campaign->setName($name);

        // Create a campaign operation and add it to the list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::SET);
        //$operation->setOperator(Operator::SET);
        $operations[] = $operation;
        // Update the campaign on the server.
        try {
            $result = $adGroupCriterionService->mutate($operations);

            $campaign = $result->getValue()[0];
            $resultArr = [
                'name' => $name,
                'campaignId' => $campaign->getCampaignGroupId(),
                'campaignName' => $campaign->getName()
            ];

            return response()->json($resultArr, 200);
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    /**
     * Set universal app campaign.
     *
     * @param string $customerClientId
     * @param string $name
     * @param string $status
     * @param string $startDay
     * @param string $endDay
     * @param string $microAmount
     * @param string $budgetName
     * @param string $deliveryMethod
     * @param bool $isExplicitlyShared
     * @param string $description1
     * @param string $description2
     * @param string $description3
     * @param string $description4
     * @param string $appId
     * @param string $appMarket
     * @param string $appBiddingStrategyGoalType
     * @param string $operator
     * @param string $campaignId
     *
     * @return array|mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setUniversalAppCampaign(string $customerClientId,
                                            string $name, string $status, string $startDay, string $endDay,
                                            string $microAmount, string $budgetName, string $deliveryMethod, bool $isExplicitlyShared,
                                            string $description1, string $description2, string $description3, string $description4,
                                            string $appId, string $appMarket, string $appBiddingStrategyGoalType, string $operator, string $campaignId = '')
    {

        $budget = (object)$this->setBudget($customerClientId, $budgetName, $microAmount, $deliveryMethod, $isExplicitlyShared);

        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaign = new Campaign();

        if ($campaignId) {
            $campaign->setId($campaignId);
        }

        $campaign->setName($name);
        $campaign->setAdvertisingChannelType(AdvertisingChannelType::MULTI_CHANNEL); //AdvertisingChannelType::DISPLAY

        $campaign->setAdvertisingChannelSubType(AdvertisingChannelSubType::UNIVERSAL_APP_CAMPAIGN);

        //adding start time for the campaign
        $campaign->setStartDate(date('Ymd', strtotime($startDay ?? '+1 day')));
        $campaign->setEndDate(date('Ymd', strtotime($endDay ?? '+2 years')));

        // Set shared budget (required).
        $campaign->setBudget(new Budget($budget->id));

        $campaign->setStatus($status); //CampaignStatus::PAUSED`

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType(BiddingStrategyType::TARGET_CPA); //BiddingStrategyType::MANUAL_CPC

        // Set the target CPA to $1 / app install.
        $biddingScheme = new TargetCpaBiddingScheme();
        $money = new Money();
        $money->setMicroAmount($microAmount);
        $biddingScheme->setTargetCpa($money);

        $biddingStrategyConfiguration->setBiddingScheme($biddingScheme);

        $campaign->setBiddingStrategyConfiguration(
            $biddingStrategyConfiguration
        );

        // Set the campaign's assets and ad text ideas. These values will be used to generate ads.
        $universalAppSetting = new UniversalAppCampaignSetting();
        $universalAppSetting->setAppId($appId); //'com.lab-data.color'
        $universalAppSetting->setAppVendor($appMarket); // MobileApplicationVendor::VENDOR_GOOGLE_MARKET

        $universalAppSetting->setDescription1($description1);
        $universalAppSetting->setDescription2($description2);
        $universalAppSetting->setDescription3($description3);
        $universalAppSetting->setDescription4($description4);

        // Optional: You can set up to 20 image assets for your campaign.
        // See UploadImage.php for an example on how to upload images.
        //
        // $universalAppSetting->imageMediaIds = [INSERT_IMAGE_MEDIA_ID_HERE];

        // Optimize this campaign for getting new users for your app.
        $universalAppSetting->setUniversalAppBiddingStrategyGoalType(
            $appBiddingStrategyGoalType ?? UniversalAppBiddingStrategyGoalType::OPTIMIZE_FOR_INSTALL_CONVERSION_VOLUME
        ); //UniversalAppBiddingStrategyGoalType::OPTIMIZE_FOR_INSTALL_CONVERSION_VOLUME

        // If you select bidding strategy goal type as
        // OPTIMIZE_FOR_IN_APP_CONVERSION_VOLUME, then you may specify a set of
        // conversion types for in-app actions to optimize the campaign towards.
        // Conversion type IDs can be retrieved using ConversionTrackerService.get.
        //
        // $campaign->selectiveOptimization = new SelectiveOptimization();
        // $campaign->selectiveOptimization->conversionTypeIds = [
        //     INSERT_CONVERSION_TYPE_ID_1_HERE,
        //     INSERT_CONVERSION_TYPE_ID_2_HERE
        // ];

        // Optional: Set the campaign settings for Advanced location options.
        $geoTargetTypeSetting = new GeoTargetTypeSetting();

        $geoTargetTypeSetting->setNegativeGeoTargetType(GeoTargetTypeSettingNegativeGeoTargetType::LOCATION_OF_PRESENCE);

        $campaign->setSettings([$universalAppSetting, $geoTargetTypeSetting]);

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($campaignService, $operation) {
            return $campaignService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'name' => $result->getName(),
            'campaignId' => $result->getId()
        ];
    }

    /**
     * Set or add new campaign.
     *
     * @param $data
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCampaign($data)
    {
        $name = $data['name'];
        $status = $data['status'];
        $endDay = $data['endDay'] ?? null;
        $startDay = $data['startDay'] ?? null;
        $customerClientId = $data['customerId'];
        $campaignId = $data['campaignId'] ?? null;
        $adsChannelType = $data['adsChannelType'];
        $biddingStrategy = $data['biddingStrategy'];
        $operator = $data['operator'] ?? Operator::ADD;
        $finalUrlSuffix = $data['finalUrlSuffix'] ?? null;
        $trackingUrlTemplate = $data['trackingUrlTemplate'] ?? null;
        $urlCustomParameters = $data['urlCustomParameters'] ?? [];
        $doReplace = $data['doReplace'] ?? null;

        $budget = (object)$this->setBudget(
            $customerClientId,
            $data['budgetName'] ?? '',
            $data['microAmount'] ?? '',
            $data['deliveryMethod'] ?? '',
            $data['isExplicitlyShared'] ?? ''
        );

        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaign = new Campaign();

        if ($campaignId) {
            $campaign->setId($campaignId);
        }

        $campaign->setFinalUrlSuffix($finalUrlSuffix);
        $campaign->setTrackingUrlTemplate($trackingUrlTemplate);

        $urlCustomParameters = $this->setUrlCustomerParameters('', '', $urlCustomParameters, $doReplace, static::RESPONSE_TYPE_INSTANCE);
        if ($urlCustomParameters) {
            $urlCustomParameters = null;
        }

        $campaign->setUrlCustomParameters($urlCustomParameters);

        $campaign->setName($name);
        $campaign->setAdvertisingChannelType($adsChannelType);//AdvertisingChannelType::DISPLAY

        //adding start time for the campaign
        $campaign->setStartDate(date('Ymd', strtotime($startDay ?? '+1 day')));
        $campaign->setEndDate(date('Ymd', strtotime($endDay ?? '+2 years')));

        // Set shared budget (required).
        $campaign->setBudget(new Budget());
        $campaign->getBudget()->setBudgetId($budget->id);

        $campaign->setStatus($status);//CampaignStatus::PAUSED

        // Set bidding strategy (required).
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBiddingStrategyType($biddingStrategy); //BiddingStrategyType::MANUAL_CPC

        $campaign->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($campaignService, $operation) {
            return $campaignService->mutate([$operation])->getValue()[0];
        });

        return [
            'name' => $result->getName(),
            'campaignId' => $result->getId()
        ];
    }

    /**
     * Set or add new campaign setting.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $settings
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCampaignSetting(string $customerClientId, string $campaignId, array $settings)
    {
        $campaignCriterionService = $this->adWordsService->getService(CampaignCriterionService::class, $customerClientId);

        $operations = [];
        foreach ($settings as $item):

            if (!$item->operator) {
                $item->operator = Operator::ADD;
            }

            // Override operator for platform.
            if ($item->operator != 'REMOVE'):
                if ($item->operator == 'ADD' && $item->type == 'platform') {
                    $item->operator = 'SET';
                }
            endif;

            $className = config('names.' . $item->type);

            // Create criterion.
            $criterion = new CampaignCriterion(
                $campaignId,
                $item->isNegative,
                new $className($item->id)
            );

            // Create operation
            $operation = new CampaignCriterionOperation();
            $operation->setOperator($item->operator);
            $operation->setOperand($criterion);

            $operations[] = $operation;
        endforeach;

        $result = $this->call(function () use ($campaignCriterionService, $operations) {
            return $campaignCriterionService->mutate($operations)->getValue();
        });

        $results = [];

        foreach ($result as $item) {
            $results[] = (object)[
                'id' => $item->getCriterion()->getId(),
                'type' => $item->getCriterion()->getType()
            ];
        }

        return $results;
    }

    /**
     * @param $data
     * @return mixed
     * desc: will add timing schedule on the campaign for the user to for example set the days for it to work and function in the project
     */
    public function addCampaignSchedule($data)
    {
        $customerClientId = $data['customerId'];
        $campaignId = $data['campaignId'];
        $operations = [];
        $campaignCriteria = [];
        $campaignService = $this->adWordsService->getService(CampaignCriterionService::class, $customerClientId);

        foreach ($data['schedules'] as $schedule) {//create one adschedule criteria for each schedule
            $adschedule = new AdSchedule();
            $adschedule->setDayOfWeek($schedule['day']);
            $adschedule->setStartHour($schedule['start_hour']);
            #$adschedule->setStartMinute(isset($schedule['start_minute'])?$schedule['start_minute']:"ZERO");
            $adschedule->setStartMinute("ZERO");
            $adschedule->setEndHour($schedule['end_hour']);
            #$adschedule->setEndMinute(isset($schedule['end_minute'])?$schedule['end_minute']:"ZERO");
            $adschedule->setEndMinute("ZERO");
            $campaignCriteria[] = new CampaignCriterion($campaignId, null, $adschedule);
        }

        foreach ($campaignCriteria as $campaignCriterion) {
            $operation = new CampaignCriterionOperation();
            $operation->setOperator(Operator::ADD);
            $operation->setOperand($campaignCriterion);
            $operations[] = $operation;
        }

        try {

            $result = $campaignService->mutate($operations);
            $resultArr = $result;

            return response()->json($resultArr, 200);
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    /**
     * Will set geo target using the location id for the user.
     *
     * @param $data
     * @param $customerId
     *
     * @return array|mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setGeoTarget($data, $customerId)
    {
        $campaignService = $this->adWordsService->getService(CampaignCriterionService::class, $customerId);
        $operations = [];
        foreach ($data as $item):
            $operator = $item->operator ?? 'ADD';
            $isNegative = $item->negative ?? false;

            $location = new Location();

            $location->setId($item->locationId);

            $operation = new CampaignCriterionOperation();

            $campaignCriterion = new CampaignCriterion();
            $campaignCriterion->setIsNegative($isNegative);
            $campaignCriterion->setCampaignId($item->campaignId);
            $campaignCriterion->setCriterion($location);

            $operation->setOperand($campaignCriterion);
            $operation->setOperator($operator);

            $operations[] = $operation;
        endforeach;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'campaignId' => $result->getCampaignId(),
        ];
    }

    /**
     * Will add geo distance targeting using both address and geo point .
     * @param $data
     * @param $customerId
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setGeoDistanceTarget($data, $customerId)
    {
        $campaignService = $this->adWordsService->getService(CampaignCriterionService::class, $customerId);
        $operations = [];
        foreach ($data as $item):
            $operator = $item->operator ?? 'ADD';
            $isNegative = $item->negative ?? false;
            $proximity = new Proximity();
            $proximity->setRadiusDistanceUnits(ProximityDistanceUnits::KILOMETERS);
            $proximity->setRadiusInUnits($item->radius ?? 10);
            if ($item->type == 'address'):
                $address = new Address();
                $address->setStreetAddress($item->streetAddress ?? '');
                $address->setCityName($item->cityName ?? '');
                $address->setPostalCode($item->postalCode ?? '');
                $address->setCountryCode($item->countryCode ?? '');

                $proximity->setAddress($address);
            elseif ($item->type == 'point'):
                $geoPoint = new GeoPoint();
                $geoPoint->setLatitudeInMicroDegrees($item->latitude);
                $geoPoint->setLongitudeInMicroDegrees($item->longtitude);

                $proximity->setGeoPoint($geoPoint);
            endif;

            $operation = new CampaignCriterionOperation();
            $campaignCriterion = new CampaignCriterion();
            $campaignCriterion->setIsNegative($isNegative);
            $campaignCriterion->setCampaignId($item->campaignId);
            $campaignCriterion->setCriterion($proximity);

            $operation->setOperand($campaignCriterion);
            $operation->setOperator($operator);

            $operations[] = $operation;
        endforeach;

        $result = $this->call(function () use ($campaignService, $operations) {
            return $campaignService->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'campaignId' => $result->getCampaignId(),
        ];
    }

    /**
     * Set ads dynamic search setting.
     *
     * @param string $customerId
     * @param string $campaignId
     * @param string $domainName
     * @param string $languageCode
     * @param string $pageFeed
     * @param string $suppliedUrlsOnly
     *
     * @return array|mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setDynamicSearchAdSetting(string $customerId, string $campaignId, string $domainName, string $languageCode = '', string $pageFeed = '', $suppliedUrlsOnly = '')
    {
        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerId);

        $dynamicSearchSetting = new DynamicSearchAdsSetting();
        $dynamicSearchSetting->setDomainName($domainName);
        $dynamicSearchSetting->setLanguageCode($languageCode);
        $dynamicSearchSetting->setUseSuppliedUrlsOnly($suppliedUrlsOnly);

        if ($pageFeed) {
            $feed = new PageFeed();
            $feed->setFeedIds([$pageFeed]);
            $dynamicSearchSetting->setPageFeed($feed);
        }

        $campaign = new Campaign();
        $campaign->setId($campaignId);
        $campaign->setSettings([$dynamicSearchSetting]);

        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::SET);

        $result = $this->call(function () use ($campaignService, $operation) {
            return $campaignService->mutate([$operation])->getValue()[0];
        });

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * Will set new|update budget for specific campaign.
     *
     * @param string $customerClientId
     * @param string $name
     * @param string $microAmount
     * @param string $deliveryMethod
     * @param bool $isExplicitlyShared
     * @param string $budgetId
     * @param string $campaignId
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setBudget(string $customerClientId, string $name, string $microAmount, string $deliveryMethod, bool $isExplicitlyShared, string $budgetId = '', string $campaignId = '')
    {
        $budgetService = $this->adWordsService->getService(BudgetService::class, $customerClientId);

        $money = new Money();
        $money->setMicroAmount($microAmount);

        // Create the shared budget (required).
        $budget = new Budget();

        if ($budgetId) {
            $budget->setBudgetId($budgetId);
        }

        $budget->setAmount($money);
        $budget->setName(($name ?? 'budget_') . uniqid());
        $budget->setDeliveryMethod($deliveryMethod ?? 'STANDARD'); //BudgetBudgetDeliveryMethod::STANDARD
        $budget->setIsExplicitlyShared($isExplicitlyShared ?? true);

        // Create a budget operation.
        $operation = new BudgetOperation();
        $operation->setOperand($budget);
        $operation->setOperator(Operator::ADD);

        // Create the budget on the server.
        $budget = $this->call(function () use ($budgetService, $operation) {
            return $budgetService->mutate([$operation])->getValue()[0];
        });

        if ($campaignId) {
            return $this->setCampaignBudget($customerClientId, $campaignId, $budget->getBudgetId());
        }

        return [
            'id' => $budget->getBudgetId(),
        ];
    }

    /**
     * Set campaign budget.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $budgetId
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setCampaignBudget(string $customerClientId, string $campaignId, string $budgetId)
    {
        $campaign = new Campaign();

        $campaignService = $this->adWordsService->getService(CampaignService::class, $customerClientId);

        $campaign->setId($campaignId);

        // Set shared budget (required).
        $campaign->setBudget(new Budget($budgetId));

        // Create a campaign operation and add it to the operations list.
        $operation = new CampaignOperation();
        $operation->setOperand($campaign);
        $operation->setOperator(Operator::SET);

        $this->call(function () use ($campaignService, $operation) {
            return $campaignService->mutate([$operation]);
        });

        return [
            'bidgetId' => $budgetId,
            'campaignId' => $campaignId,
        ];
    }

    /**
     * Will set the delivery method for the budget . types  : STANDARD , ACCELERATED , UNKNOWN
     *
     * @param string $customerId
     * @param $data
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setBudgetDeliveryMethod(string $customerId, $data)
    {
        $operations = [];
        $budget_id = $data->budgetId;

        $budgetService = $this->adWordsService->getService(BudgetService::class, $customerId);

        $budget = new Budget();
        $budget->setBudgetId($budget_id);

        $budget->setDeliveryMethod($data->deliveryMethod ?? 'STANDARD');//BudgetBudgetDeliveryMethod::STANDARD

        $operation = new BudgetOperation();
        $operation->setOperand($budget);
        $operation->setOperator('SET');
        $operations[] = $operation;

        $result = $this->call(function () use ($budgetService, $operations) {
            return $budgetService->mutate($operations)->getValue()[0];
        }, 1);
        return $result;
    }
}
