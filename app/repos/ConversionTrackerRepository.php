<?php

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\Wrappers\WrapperBridge;
use App\Exceptions\EndPointRequestException;
use App\interfaces\ConversionTrackerInterface;

use Google\AdsApi\AdWords\v201809\cm\Paging;
use LaravelGoogleAds\Services\AdWordsService;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerService;
use Google\AdsApi\AdWords\v201809\cm\AdWordsConversionTracker;
use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerOperation;
use Google\AdsApi\AdWords\v201809\cm\WebsiteCallMetricsConversion;

class ConversionTrackerRepository implements ConversionTrackerInterface
{
    use RetryCallTrait;

    /**
     * @var AdWordsService
     */
    protected $adWordsService;

    /**
     * @var int
     */
    const PAGE_LIMIT = 500;

    /**
     * ConversionTrackingRepository constructor.
     */
    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * Add or update specified conversion tracker.
     *
     * @param string $customerClientId
     * @param string $trackerType
     * @param string $operator
     * @param string $name
     * @param string $category
     * @param string $currencyCode
     * @param string $status
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setConversionTracker(string $customerClientId, string $trackerType, string $operator, string $name, string $category, string $currencyCode, string $status)
    {
        $service = $this->adWordsService->getService(ConversionTrackerService::class, $customerClientId);

        $conversionTracker = $trackerType == 'website' ? new AdWordsConversionTracker() : new WebsiteCallMetricsConversion();
        $conversionTracker->setName($name);
        $conversionTracker->setStatus($status);
        $conversionTracker->setCategory($category ?? null);
        $conversionTracker->setDefaultRevenueCurrencyCode($currencyCode ?? null);

        $operation = new ConversionTrackerOperation();
        $operation->setOperand($conversionTracker);
        $operation->setOperator($operator);

        $values = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        }, 1);

        $results = [
            'id' => $values->getId(),
            'googleGlobalSiteTag' => $values->getGoogleGlobalSiteTag(),
            'googleEventSnippet' => $values->getGoogleEventSnippet(),
        ];

        return $results;
    }

    /**
     * Get conversion trackers.
     *
     * @param string $customerClientId
     * @param array $predicates
     * @param string $responseType
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getConversionTrackers(string $customerClientId, array $predicates, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $fields = [
            'Id',
            'OriginalConversionTypeId',
            'Name',
            'Status',
            'Category',
            'GoogleEventSnippet',
            'GoogleGlobalSiteTag',
            'DataDrivenModelStatus',
            'ConversionTypeOwnerCustomerId',
            'ViewthroughLookbackWindow',
            'CtcLookbackWindow',
            'CountingType',
            'DefaultRevenueValue',
            'DefaultRevenueCurrencyCode',
            'AlwaysUseDefaultRevenueValue',
            'ExcludeFromBidding',
            'AttributionModelType',
            'MostRecentConversionDate',
            'LastReceivedRequestTime',
            'PhoneCallDuration',
            'TrackingCodeType',
            'AppId',
            'AppPlatform',
            'AppPostbackUrl',
            'WebsitePhoneCallDuration',
        ];

        $selector = new Selector($fields);

        $selectorPredicates = [
//            (object)['field' => 'Id', 'operator' => 'EQUALS', 'values' => [2045454855]]
        ];

        foreach ($predicates as $predicate) {

            $predicate = (object)$predicate;

            $selectorPredicates[] = new Predicate($predicate->field, $predicate->operator, $predicate->values);
        }

        $selector->setPredicates($selectorPredicates);

        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        $service = $this->adWordsService->getService(ConversionTrackerService::class, $customerClientId);

        $entries = $this->call(function () use ($service, $selector) {
            return $service->get($selector)->getEntries() ?? [];
        }, 1);

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        $results = [];

        foreach ($entries as $entry) {

            $results[] = WrapperBridge::toObject($entry);
        }

        return $results;
    }
}
