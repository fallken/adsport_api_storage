<?php

/**
 * Add this two lines to AdsSopClient.php before call parent constructor in constructor method.
 */
$options['proxy_host'] = config('general.proxy_host');
$options['proxy_port'] = config('general.proxy_port');
