<?php

namespace App\Helpers;

use App\Exceptions\ThreadShutdownException;
use App\Exceptions\EndPointRequestException;

use Exception;
use Google\AdsApi\AdWords\v201809\cm\ApiException;
use Google\AdsApi\AdWords\v201809\cm\DatabaseError;
use Google\AdsApi\AdWords\v201809\cm\InternalApiError;
use Google\AdsApi\AdWords\v201809\cm\RateExceededError;
use Google\AdsApi\AdWords\v201809\cm\AuthorizationError;
use Google\AdsApi\AdWords\v201809\cm\AuthenticationError;

trait RetryCallTrait
{
    /**
     * Get max number call attempts.
     *
     * @return int
     */
    private function getMaxAttempts(): int
    {
        $attempts = (int)request()->header('attempts');

        $attempts = $attempts < 1 || $attempts > 5 ? 3 : $attempts;

        return $attempts;
    }

    /**
     * Call the callback function for specified attempts.
     *
     * @param $callback
     * @param int $attempts
     * @param $failCallback
     * @param int $secondsSleep
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     *
     * @throws ThreadShutdownException
     */
    public function call($callback, int $attempts = 0, $failCallback = null, int $secondsSleep = 3)
    {
        if (!is_callable($callback)) {

            throw new EndPointRequestException('Bad method call exception.', 400);
        }

        $errorMessage = 'Service unavailable.';

        $response = null;
        $attempts = $attempts ?? $this->getMaxAttempts();

        for ($attempt = -1; $attempt < $attempts; $attempt++) {

            try {
                $response = $callback() ?? [];
            }
            catch (Exception $exception) {

                if ($this->shouldRetry($exception, $failCallback)) {
                    sleep($secondsSleep);
                    continue;
                }

                $errorMessage = $exception->getMessage();
            }

            break;
        }

        if ($response === null) {

            if (is_callable($failCallback)) {
                return $failCallback($errorMessage);
            }

            throw new EndPointRequestException($errorMessage, 400);
        }

        return $response;
    }

    /**
     * Determine for retry call api.
     *
     * @param $exception
     * @param $callback
     *
     * @return bool
     *
     * @throws ThreadShutdownException
     */
    private function shouldRetry(&$exception, &$callback)
    {
        if ($exception instanceof ApiException) {

            $error = $exception->getErrors()[0];

            if ($error instanceof AuthenticationError || $error instanceof AuthorizationError) {

                $message = $error->getErrorString() . ": " . $error->getTrigger();

                if (is_callable($callback)) {
                    $callback($message);
                }

                throw new ThreadShutdownException($message, 403);
            }
            else if ($error instanceof DatabaseError || $error instanceof RateExceededError || $error instanceof InternalApiError) {
                return true;
            }
        }

        return false;
    }
}