<?php

namespace App\Copier\Interfaces;

interface AccountCopierInterface
{
    /**
     * Set source customer.
     *
     * @param string $sourceCustomerId
     *
     * @return void
     */
    public function setSourceCustomer(string $sourceCustomerId): void;

    /**
     * Set destination customer.
     *
     * @param string $destinationCustomerId
     *
     * @return void
     */
    public function setDestinationCustomer(string $destinationCustomerId): void;

    /**
     * Get source customer campaigns.
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function getCampaigns();

    /**
     * Get ad groups for specified campaign.
     *
     * @param string $campaignId
     *
     * @return mixed
     */
    public function getAdGroups(string $campaignId);

    /**
     * Get ads for specified ad group.
     *
     * @param string $adGroupId
     * @return mixed|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getAds(string $adGroupId);

    /**
     * Get conversion trackers.
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getConversionTrackers();

    /**
     * Initialize account transfer requirements.
     *
     * @return bool
     */
    public function initialize();

    /**
     * Handle account transfer process.
     *
     * @return bool
     */
    public function handle(): bool;

    /**
     * Set last results.
     *
     * @param mixed $results
     */
    public function setResults($results): void;

    /**
     * Get account copier results.
     *
     * @return array
     */
    public function getResults(): array;

    /**
     * Set debug mode.
     *
     * @param bool $status
     *
     * @return bool
     */
    public function setDebugMode(bool $status): bool;

    /**
     * Get debug mode.
     *
     * @return bool
     */
    public function getDebugMode(): bool;
}