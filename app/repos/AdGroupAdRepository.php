<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 4/18/2019
 * Time: 10:09 AM
 */

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\Wrappers\WrapperBridge;
use App\interfaces\AdgroupAdInterface;
use App\Exceptions\EndPointRequestException;

use Google\AdsApi\AdWords\v201809\cm\Ad;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAd;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdStatus;
use Google\AdsApi\AdWords\v201809\cm\AdType;
use Google\AdsApi\AdWords\v201809\cm\AssetLink;
use Google\AdsApi\AdWords\v201809\cm\CallOnlyAd;
use Google\AdsApi\AdWords\v201809\cm\ExpandedDynamicSearchAd;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\ResponsiveSearchAd;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\ServedAssetFieldType;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\cm\TextAsset;
use LaravelGoogleAds\Services\AdWordsService;

class AdGroupAdRepository implements AdgroupAdInterface
{
    use RetryCallTrait;

    /**
     * @var AdWordsService
     */
    protected $adWordsService;

    /**
     * @var int
     */
    private const PAGE_LIMIT = 500;

    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * Set ads dynamic search setting.
     *
     * @param string $customerId
     * @param string $adGroupId
     * @param string $adId
     * @param string $description
     * @param string $description2
     *
     * @return array|mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setExpandedDynamicSearchAd(string $customerId, string $adGroupId, string $adId, string $description, string $description2)
    {
        $service = $this->adWordsService->getService(AdGroupAdService::class, $customerId);

        $expandedDynamicSearchAd = new ExpandedDynamicSearchAd();
        $expandedDynamicSearchAd->setDescription($description);
        $expandedDynamicSearchAd->setDescription2($description2);

        $ad = new Ad();
        $ad->setId($adId);

        $adGroupAd = new AdGroupAd();
        $adGroupAd->setAdGroupId($adGroupId);
        $adGroupAd->setStatus(AdGroupAdStatus::PAUSED);

        $adGroupAd->setAd($ad);

        $operation = new AdGroupAdOperation();
        $operation->setOperand($adGroupAd);
        $operation->setOperator(Operator::SET);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        });

        return [
            'adId' => $result->getAd()->getId(),
            'adGroupId' => $result->getAdGroupId(),
            'status' => $result->getStatus(),
        ];
    }

    /**
     * Add call only to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $countryCode
     * @param string $phone
     * @param string $businessName
     * @param string $desc1
     * @param string $desc2
     * @param mixed $callTracked
     * @param mixed $disableCallConversion
     * @param string $conversionTypeId
     * @param string $phoneNumberVerificationUrl
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCallOnlyAd(string $customerClientId, string $adGroupId, string $countryCode, string $phone, string $businessName, string $desc1, string $desc2, $callTracked, $disableCallConversion, string $conversionTypeId, string $phoneNumberVerificationUrl)
    {
        $service = $this->adWordsService->getService(AdGroupAdService::class, $customerClientId);

        $operations = [];

        // Create call only feed.
        $callOnly = new CallOnlyAd();
        $callOnly->setPhoneNumber($phone);
        $callOnly->setDescription1($desc1);
        $callOnly->setDescription2($desc2);
        $callOnly->setCountryCode($countryCode);
        $callOnly->setCallTracked($callTracked);
        $callOnly->setBusinessName($businessName);
        $callOnly->setBusinessName($businessName);
        $callOnly->setConversionTypeId($conversionTypeId);
        $callOnly->setDisableCallConversion($disableCallConversion);
        $callOnly->setPhoneNumberVerificationUrl($phoneNumberVerificationUrl);

        // Create ad group ad.
        $adGroupAd = new AdGroupAd();
        $adGroupAd->setAdGroupId($adGroupId);
        $adGroupAd->setAd($callOnly);

        // Optional: Set additional settings.
        $adGroupAd->setStatus(AdGroupAdStatus::PAUSED);

        // Create ad group ad operation and add it to the list.
        $operation = new AdGroupAdOperation();
        $operation->setOperand($adGroupAd);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        // Add call only ads on the server.
        $result = $this->call(function () use ($service, $operations) {
            return $service->mutate($operations);
        });

        return response()->json([
            'id' => $result->getValue()[0]->getAd()->getId(),
        ]);
    }

    /**
     * Update or add new Ad for the specific ad group.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $adType
     * @param string $finalUrl
     * @param string $path1
     * @param string $path2
     * @param object $headlines
     * @param object $descriptions
     * @param string $operator
     * @param string $status
     * @param string $adId
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     *
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setAd(string $customerClientId, string $adGroupId, string $adType, string $finalUrl, string $path1, string $path2, $headlines, $descriptions, string $operator, string $status, string $adId = '')
    {
        $adService = $this->adWordsService->getService(AdGroupAdService::class, $customerClientId);

        switch ($adType) {
            case AdType::RESPONSIVE_SEARCH_AD:

                $ad = new ResponsiveSearchAd();
                $ad->setFinalUrls([$finalUrl]);

                //Set paths
                if ($path1) {
                    $ad->setPath1($path1);
                }

                if ($path2) {
                    $ad->setPath2($path2);
                }

                // Create text assets for headlines.
                $headline1 = new TextAsset();
                $headline1->setAssetText($headlines->headline1);

                $headline2 = new TextAsset();
                $headline2->setAssetText($headlines->headline2);

                $headline3 = new TextAsset();
                $headline3->setAssetText($headlines->headline3);

                $ad->setHeadlines([
                    // Set a pinning to always choose this asset for HEADLINE_1.
                    // Pinning is optional; if no pinning is set, then headlines
                    // and descriptions will be rotated and the ones that perform
                    // best will be used more often.
                    new AssetLink($headline1, ServedAssetFieldType::HEADLINE_1),
                    new AssetLink($headline2),
                    new AssetLink($headline3)
                ]);

                $assets = [];

                $asset1 = new TextAsset();
                $asset1->setAssetText($descriptions->description1);

                $asset2 = new TextAsset();
                $asset2->setAssetText($descriptions->description2);

                $assets[] = new AssetLink ($asset1);
                $assets[] = new AssetLink($asset2);

                $ad->setDescriptions($assets);

                break;
            default:
                throw  new EndPointRequestException("Invalid ad type exception.", 400);
        }

        if ($adId) {
            $ad->setId($adId);
        }

        // Create ad group ad.
        $adGroupAd = new AdGroupAd();
        $adGroupAd->setAd($ad);
        $adGroupAd->setStatus($status);
        $adGroupAd->setAdGroupId($adGroupId);

        // Create ad group ad operation and add it to the list.
        $operation = new AdGroupAdOperation();
        $operation->setOperand($adGroupAd);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($adService, $operation) {
            return $adService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'adId' => $result->getAd()->getId(),
        ];
    }

    /**
     * Will return ad group ads for the for each specific ad group.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $predicates
     * @param string $responseType
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getAds(string $customerClientId, string $adGroupId, array $predicates, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $adService = $this->adWordsService->getService(AdGroupAdService::class, $customerClientId);

        $fields = [
            'Id',
            'AdGroupId',
            'Status',
            'PolicySummary',
            'Labels',
            'BaseCampaignId',
            'BaseAdGroupId',
            'AdStrengthInfo',
            'Url',
            'DisplayUrl',
            'CreativeFinalUrls',
            'CreativeFinalMobileUrls',
            'CreativeFinalAppUrls',
            'CreativeTrackingUrlTemplate',
            'CreativeFinalUrlSuffix',
            'CreativeUrlCustomParameters',
            'UrlData',
            'Automated',
            'AdType',
            'DevicePreference',
            'SystemManagedEntitySource',

            'HeadlinePart1',
            'HeadlinePart2',
            'ExpandedTextAdDescription2',
            'ExpandedTextAdHeadlinePart3',
            'Description',
            'Path1',
            'Path2',

            'CallOnlyAdCountryCode',
            'GmailHeaderImage',
            'GmailMarketingImage',
            'MarketingImageHeadline',
            'MarketingImageDescription',
            'ProductImages',
            'ProductVideoList',
            'ImageCreativeName',
            'MultiAssetResponsiveDisplayAdMarketingImages',
            'MultiAssetResponsiveDisplayAdSquareMarketingImages',
            'MultiAssetResponsiveDisplayAdLogoImages',
            'MultiAssetResponsiveDisplayAdLandscapeLogoImages',
            'MultiAssetResponsiveDisplayAdHeadlines',
            'MultiAssetResponsiveDisplayAdLongHeadline',
            'MultiAssetResponsiveDisplayAdDescriptions',
            'MultiAssetResponsiveDisplayAdYouTubeVideos',
            'MultiAssetResponsiveDisplayAdBusinessName',
            'MultiAssetResponsiveDisplayAdMainColor',
            'MultiAssetResponsiveDisplayAdAccentColor',
            'MultiAssetResponsiveDisplayAdAllowFlexibleColor',
            'MultiAssetResponsiveDisplayAdCallToActionText',
            'MultiAssetResponsiveDisplayAdDynamicSettingsPricePrefix',
            'MultiAssetResponsiveDisplayAdDynamicSettingsPromoText',
            'MultiAssetResponsiveDisplayAdFormatSetting',
            'LogoImage',
            'CallOnlyAdPhoneNumber',
            'CallOnlyAdBusinessName',
            'CallOnlyAdDescription1',
            'CallOnlyAdDescription2',
            'CallOnlyAdCallTracked',
            'CallOnlyAdDisableCallConversion',
            'CallOnlyAdConversionTypeId',
            'CallOnlyAdPhoneNumberVerificationUrl',
            'ExpandedDynamicSearchCreativeDescription2',
            'SquareMarketingImage',
            'ShortHeadline',
            'LongHeadline',
            'BusinessName',
            'MainColor',
            'AccentColor',
            'AllowFlexibleColor',
            'CallToActionText',
            'FormatSetting',
            'ResponsiveSearchAdHeadlines',
            'ResponsiveSearchAdDescriptions',
            'ResponsiveSearchAdPath1',
            'ResponsiveSearchAdPath2',
            'TemplateId',
            'TemplateAdUnionId',
            'TemplateAdName',
            'TemplateAdDuration',
            'TemplateOriginAdId',
            'Headline',
            'Description1',
            'Description2',
            'UniversalAppAdHeadlines',
            'UniversalAppAdDescriptions',
            'UniversalAppAdMandatoryAdText',
            'UniversalAppAdImages',
            'UniversalAppAdYouTubeVideos',
            'UniversalAppAdHtml5MediaBundles',
            'RichMediaAdName',
            'RichMediaAdSnippet',
            'RichMediaAdImpressionBeaconUrl',
            'RichMediaAdDuration',
            'RichMediaAdCertifiedVendorFormatId',
            'RichMediaAdSourceUrl',
            'RichMediaAdType',
            'CombinedApprovalStatus',
        ];

        // Create a selector to select all ads for the specified ad group.
        $selector = new Selector($fields);

        $selector->setOrdering([new OrderBy('Id', SortOrder::ASCENDING)]);

        $predicatesConditions = [];

        if ($adGroupId) {
            $predicatesConditions[] = new Predicate('AdGroupId', PredicateOperator::EQUALS, [$adGroupId]);
        }
//        $predicatesConditions[] = new Predicate('AdType', PredicateOperator::IN, [AdType::EXPANDED_TEXT_AD]);

        foreach ($predicates as $predicate) {

            $selectorPredicates[] = new Predicate($predicate->field, $predicate->operator, $predicate->values);
        }

        $selector->setPredicates($predicatesConditions);

        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        $entries = $this->call(function () use ($adService, $selector) {
            return $adService->get($selector)->getEntries();
        }, 1);

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        $results = [];

        foreach ($entries as $entry) {

            $results[] = WrapperBridge::toObject($entry);
        }

        return $results;
    }

    /**
     * Get ad groups ads.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function getAdGroupAdsV1(string $customerClientId, string $adGroupId, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $campaignService = $this->adWordsService->getService(AdGroupAdService::class, $customerClientId);

        $selector = new Selector();

        $selector->setFields(['Id']);

        $selector->setPredicates([
            new Predicate('CampaignId', PredicateOperator::EQUALS, [$adGroupId])
        ]);

        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        $page = $campaignService->get($selector);

        $entries = $page->getEntries() ?? [];

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        $results = [];

        foreach ($entries as $entry) {
            $results[] = WrapperBridge::toObject($entry);
        }

        return response()->json($results);
    }
}