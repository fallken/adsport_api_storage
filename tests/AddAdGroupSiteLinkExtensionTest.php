<?php

use App\repos\AdGroupRepository;

class AddAdGroupSiteLinkExtensionTest extends TestCase
{
    /**
     * @var AdGroupRepository
     */
    private $adGroupRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adGroupRepository = resolve(AdGroupRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddSiteLinkExtension()
    {
        $customerClientId = '261-911-0558';
        $adGroupId = '64912739685';

        $links = [
            ['text' => 'Test', 'url' => 'https://google.com/sdafsdf--fg-dfg-gdg-dfg-df-g']
        ];

        $result = $this->adGroupRepository->addSiteLinkExtension($customerClientId, $adGroupId, $links);

        $this->assertIsArray($result);

        $response = $this->post('/add/addAdGroupSiteLinkExtension', [
            'customerClientId' => $customerClientId,
            'adGroupId' => $adGroupId,
            'links' => $links,
        ]);

        $response->assertResponseStatus(200);

        $links = [
            ['text' => 'Test', 'url' => 'bad link!']
        ];

        $response = $this->post('/add/addAdGroupSiteLinkExtension', [
            'customerClientId' => $customerClientId,
            'adGroupId' => $adGroupId,
            'links' => $links,
        ]);

        $response->assertResponseStatus(400);
    }
}
