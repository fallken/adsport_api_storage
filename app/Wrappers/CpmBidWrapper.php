<?php

namespace App\Wrappers;

class CpmBidWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $money = $data->getBid();

        return (object)[
            'bid' => WrapperBridge::toObject($money),
            'cpmBidSource' => $data->getCpmBidSource(),
            'bidsType' => $data->getBidsType(),
        ];
    }
}
