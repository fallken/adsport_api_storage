<?php

namespace App\Wrappers;

class AssetLinkWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $asset = $data->getAsset();
        $assetPolicySummaryInfo = $data->getAssetPolicySummaryInfo();

        return (object)[
            'pinnedField' => $data->getPinnedField(),
            'assetPerformanceLabel' => $data->getAssetPerformanceLabel(),

            'asset' => AssetWrapper::toObject($asset),
            'assetPolicySummaryInfo' => AssetPolicySummaryInfoWrapper::toObject($assetPolicySummaryInfo),
        ];
    }
}
