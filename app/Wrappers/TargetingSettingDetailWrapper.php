<?php

namespace App\Wrappers;

class TargetingSettingDetailWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'targetAll' => $data->getTargetAll(),
            'criterionTypeGroup' => $data->getCriterionTypeGroup(),
        ];
    }
}
