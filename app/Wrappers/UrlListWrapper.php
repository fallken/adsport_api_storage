<?php

namespace App\Wrappers;

class UrlListWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return $data->getUrls();
    }
}
