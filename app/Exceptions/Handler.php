<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Anik\Form\ValidationException as AnikValidationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ThreadShutdownException::class,
        ValidationException::class,
        ModelNotFoundException::class,
        AuthorizationException::class,
        AnikValidationException::class,
        EndPointRequestException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     *
     * @return void
     *
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof AnikValidationException || $exception instanceof ValidationException) {

            $data = [
                'route' => $request->getPathInfo(),
                'params' => $request->all(),
                'exception' => $exception->getResponse(),
            ];

            Log::info("Validation: " . json_encode($data));

            return response($exception->getResponse(), 422);
        }

        if ($exception instanceof EndPointRequestException) {

            $data = [
                'route' => $request->getPathInfo(),
                'params' => $request->all(),
                'exception' => $exception->getMessage(),
            ];

            Log::info("EndPoint: " . json_encode($data));

            return response(['code' => $exception->getCode(), 'message' => $exception->getMessage()], 400);
        }

        if ($exception instanceof ThreadShutdownException) {

            $data = [
                'route' => $request->getPathInfo(),
                'params' => $request->all(),
                'exception' => $exception->getMessage(),
            ];

            Log::info("ThreadShutdown: " . json_encode($data));

            return response(['code' => $exception->getCode(), 'message' => $exception->getMessage()], 400);
        }

        return parent::render($request, $exception);
    }
}
