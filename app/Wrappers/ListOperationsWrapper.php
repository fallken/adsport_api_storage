<?php

namespace App\Wrappers;

class ListOperationsWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'clear' => $data->getClear(),
            'operators' => $data->getOperators(),
        ];
    }
}
