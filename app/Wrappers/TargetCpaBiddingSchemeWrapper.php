<?php

namespace App\Wrappers;

class TargetCpaBiddingSchemeWrapper implements AdWordsWrapperInterface
{
    use BiddingSchemeTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $targetCpa = $data->getTargetCpa();
        $maxCpcBidFloor = $data->getMaxCpcBidFloor();
        $maxCpcBidCeiling = $data->getMaxCpcBidCeiling();

        return (object)array_merge(static::append($data),
            [
                'targetCpa' => WrapperBridge::toObject($targetCpa),
                'maxCpcBidFloor' => WrapperBridge::toObject($maxCpcBidFloor),
                'maxCpcBidCeiling' => WrapperBridge::toObject($maxCpcBidCeiling),
            ]
        );
    }
}
