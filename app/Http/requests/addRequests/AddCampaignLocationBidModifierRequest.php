<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddCampaignLocationBidModifierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'campaignId' => ['nullable', 'string'],
            'locationId' => ['required', 'string'],
            'bidModifier' => ['required', 'numeric'],
        ];
    }
}
