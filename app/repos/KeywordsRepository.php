<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 11:15 AM
 */

namespace App\repos;

use App\Exceptions\EndPointRequestException;
use App\Helpers\RetryCallTrait;
use App\interfaces\KeywordsInterface;
use App\Wrappers\WrapperBridge;

use Google\AdsApi\AdWords\v201809\cm\Language;
use Google\AdsApi\AdWords\v201809\cm\Location;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\AdGroupCriterionOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupCriterionService;
use Google\AdsApi\AdWords\v201809\cm\BiddableAdGroupCriterion;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201809\cm\CpcBid;
use Google\AdsApi\AdWords\v201809\cm\Criterion;
use Google\AdsApi\AdWords\v201809\cm\CriterionType;
use Google\AdsApi\AdWords\v201809\cm\Keyword;
use Google\AdsApi\AdWords\v201809\cm\KeywordMatchType;
use Google\AdsApi\AdWords\v201809\cm\NegativeAdGroupCriterion;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\cm\UrlList;
use Google\AdsApi\AdWords\v201809\o\AdGroupEstimateRequest;
use Google\AdsApi\AdWords\v201809\o\AttributeType;
use Google\AdsApi\AdWords\v201809\o\CampaignEstimateRequest;
use Google\AdsApi\AdWords\v201809\o\IdeaType;
use Google\AdsApi\AdWords\v201809\o\KeywordEstimateRequest;
use Google\AdsApi\AdWords\v201809\o\RelatedToQuerySearchParameter;
use Google\AdsApi\AdWords\v201809\o\SeedAdGroupIdSearchParameter;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaSelector;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaService;
use Google\AdsApi\AdWords\v201809\o\TrafficEstimatorSelector;
use Google\AdsApi\AdWords\v201809\o\TrafficEstimatorService;
use Google\AdsApi\Common\Util\MapEntries;
use Illuminate\Http\Request;
use LaravelGoogleAds\Services\AdWordsService;


class KeywordsRepository implements KeywordsInterface
{
    use RetryCallTrait;

    protected $adWordsService;

    const PAGE_LIMIT = 500;

    public function __construct()
    {
        $this->adWordsService = new AdWordsService();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function changeKeywordStatus(Request $request)
    {
        $customerClientId = $request->customerId;
        $adGroupId = $request->adgroupId;
        $criterionId = $request->keywordId;

        $adGroupCriterionService = $this->adWordsService->getService(AdGroupCriterionService::class, $customerClientId);
        //$adGroupCriterionService = $adWordsServices->get($session, AdGroupCriterionService::class);

        // Create ad group criterion.
        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->setAdGroupId($adGroupId);
        // Create criterion using an existing ID. Use the base class Criterion
        // instead of Keyword to avoid having to set keyword-specific fields.
        $adGroupCriterion->setCriterion(new Criterion($criterionId));

        // Update final URL.
        /*$adGroupCriterion->setFinalUrls(
            new UrlList(['http://www.example.com/new'])
        );*/
        $adGroupCriterion->setUserStatus($request->status);
        // Create ad group criterion operation and add it to the list.
        $operation = new AdGroupCriterionOperation();
        $operation->setOperand($adGroupCriterion);
        $operator = $request->status == 'REMOVED' ? 'REMOVE' : 'SET';
        $operation->setOperator($operator);

        // Update the keyword on the server.

        $result = $this->call(function () use ($adGroupCriterionService, $operation) {
            return $adGroupCriterionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'status' => $result->getUserStatus(),
            'ApprovalStatus' => $result->getApprovalStatus(),
            'keywordId' => $criterionId
        ];
    }

    /**
     *  Send get key word request to google.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $sortOrder
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getKeywords(string $customerClientId, string $adGroupId, string $sortOrder = '', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $adWordsService = $this->adWordsService->getService(AdGroupCriterionService::class, $customerClientId);

        $fields = [
            'Id',
            'CriteriaType',
            'KeywordMatchType',
            'KeywordText',
            'FirstPositionCpc',
            'BiddingStrategy',
            'Status',
            'CpcBid',
            'CpmBid',
            'BiddingStrategyName',
            'BiddingStrategyType',
            'SystemServingStatus',
            'ApprovalStatus',
            'FirstPageCpc',
            'TopOfPageCpc',
            'UrlCustomParameters',
            'FinalUrlSuffix',
            'BidModifier',
            'DisapprovalReasons',
            'FinalUrls',
            'FinalMobileUrls',
            'FinalAppUrls',
            'Labels',
            'BaseCampaignId',
            'BaseAdGroupId',
        ];

        /**
         * Create Selector instances.
         **/
        $selector = new Selector();

        $selector->setFields($fields);

        $sortOrder = $sortOrder ?? SortOrder::DESCENDING;

        $selector->setOrdering([new OrderBy('KeywordText', $sortOrder)]);

        $selector->setPredicates([
            new Predicate(
                'AdGroupId',
                PredicateOperator::IN,
                [$adGroupId]
            ),
            new Predicate(
                'CriteriaType',
                PredicateOperator::IN,
                [CriterionType::KEYWORD]
            )
        ]);

        /**
         * Create pagination.
         */
        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        /**
         * Send get request for getting key words to google ad words api.
         */
        $page = $this->call(function () use ($adWordsService, $selector) {
            return $adWordsService->get($selector);
        }, 1);

        /**
         * Store keywords to results and return them.
         */
        $entries = $page->getEntries() ?? [];

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        $results = [];

        foreach ($entries as $keyword) {

            $results[] = WrapperBridge::toObject($keyword);
        }

        return response()->json($results);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeKeywordName(Request $request)
    {
        $customerClientId = $request->customerId;
        $adGroupId = $request->adgroupId;
        $criterionId = $request->keywordId;
        $newName = $request->name;

        $adGroupCriterionService = $this->adWordsService->getService(AdGroupCriterionService::class, $customerClientId);
        $operations = [];


        // Create the first keyword criterion.
        $keyword = new Keyword();
        $keyword->setId($criterionId);
        $keyword->setText($newName);

        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->setAdGroupId($adGroupId);
        $adGroupCriterion->setCriterion($keyword);

        $operation = new AdGroupCriterionOperation();
        $operation->setOperand($adGroupCriterion);
        $operation->setOperator(Operator::SET);
        $operations[] = $operation;
        // Update the keyword on the server.

        try {
            $result = $adGroupCriterionService->mutate($operations);
            $adGroupCriterion = $result->getValue()[0];
            $resultArr = [
                'newName' => $newName
                , 'ApprovalStatus' => $adGroupCriterion->getApprovalStatus()
                , 'keywordId' => $criterionId
            ];
            return response()->json($resultArr, 200);
        }
        catch (\Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }


    /**
     * Add keyword for specified ad group.
     *
     * @param $data
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addKeyword($data)
    {
        $customerClientId = $data['customerClientId'];
        $adGroupId = $data['adGroupId'];
        $name = $data['name'];
        $status = $data['status'];
        $type = $data['type'] ?? null;
        $finalUrl = $data['finalUrl'] ?? null;
        $price = $data['amount'];
        $matchType = $data['matchType'] ?? 'EXACT';

        $criterion = new AdGroupCriterionService();

        $adGroupCriterionService = $this->adWordsService->getService($criterion, $customerClientId);
        $operations = [];

        // Create the first keyword criterion.
        $keyword = new Keyword();
        $keyword->setText($name);
        $keyword->setMatchType($matchType); // KeywordMatchType::EXACT;

        if (!is_null($type) && $type == 'negative') {
            $adGroupCriterion = new NegativeAdGroupCriterion();
        }
        else {
            $adGroupCriterion = new BiddableAdGroupCriterion();

            if ($finalUrl) {
                $adGroupCriterion->setFinalUrls(new UrlList([$finalUrl]));
            }

            $adGroupCriterion->setUserStatus($status); //UserStatus::PAUSED

            if ($price) {
                $money = new Money();
                $money->setMicroAmount($price);

                $bid = new CpcBid();
                $bid->setBid($money);

                $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
                $biddingStrategyConfiguration->setBids([$bid]);
                $adGroupCriterion->setBiddingStrategyConfiguration($biddingStrategyConfiguration);
            }
        }

        $adGroupCriterion->setAdGroupId($adGroupId);
        $adGroupCriterion->setCriterion($keyword);

        // Create an ad group criterion operation and add it to the list.
        $operation = new AdGroupCriterionOperation();
        $operation->setOperand($adGroupCriterion);
        $operation->setOperator(Operator::ADD);
        $operations[] = $operation;

        $result = $this->call(function () use ($adGroupCriterionService, $operations) {
            return $adGroupCriterionService->mutate($operations)->getValue()[0];
        });

        if (is_null($type)):
            return [
                $result->getCriterion()->getId()
            ];
        else:
            return $result;
        endif;
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse|mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getKeywordRecommendations($data)
    {
        $customerClientId = $data['customerId'];
        $pageNumber = $data['pageNumber'] ?? 0;
        $adGroupId = $data['adGroupId'] ?? '';
        $keywords = $data['keywords'];
        $reqType = $data['type'] ? $data['type'] : 'IDEAS';#IDEAS #STATS

        $targetingIdeaService = $this->adWordsService->getService(TargetingIdeaService::class, $customerClientId);

        $selector = new TargetingIdeaSelector();
        $selector->setRequestType($reqType);
        $selector->setIdeaType(IdeaType::KEYWORD);
        $selector->setRequestedAttributeTypes(
            [
                AttributeType::KEYWORD_TEXT,
                AttributeType::SEARCH_VOLUME,
                AttributeType::AVERAGE_CPC,
                AttributeType::IDEA_TYPE,
                AttributeType::EXTRACTED_FROM_WEBPAGE,
                AttributeType::COMPETITION,
                AttributeType::TARGETED_MONTHLY_SEARCHES,
                AttributeType::CATEGORY_PRODUCTS_AND_SERVICES
            ]
        );
        $paging = new Paging();
        $paging->setStartIndex($pageNumber);
        $paging->setNumberResults(30);
        $selector->setPaging($paging);

        $searchParameters = [];

        $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
        $relatedToQuerySearchParameter->setQueries($keywords);
        $searchParameters[] = $relatedToQuerySearchParameter;

        //
        // Optional: Use an existing ad group to generate ideas.
        if (!empty($adGroupId)) {
            $seedAdGroupIdSearchParameter = new SeedAdGroupIdSearchParameter();
            $seedAdGroupIdSearchParameter->setAdGroupId($adGroupId);
            $searchParameters[] = $seedAdGroupIdSearchParameter;
        }
        $selector->setSearchParameters($searchParameters);

        $entries = $this->call(function () use ($targetingIdeaService, $selector) {
            return $targetingIdeaService->get($selector)->getEntries();
        }, 3, null, 2);


        if ($entries === null) {
            return [];
        }

        $results = [];

        foreach ($entries as $targetingIdea) {
            $data = MapEntries::toAssociativeArray($targetingIdea->getData());
            $keyword = $data[AttributeType::KEYWORD_TEXT]->getValue();

            $searchVolume = ($data[AttributeType::SEARCH_VOLUME]->getValue() !== null) ? $data[AttributeType::SEARCH_VOLUME]->getValue() : 0;

            $averageCpc = $data[AttributeType::AVERAGE_CPC]->getValue();
            $competition = $data[AttributeType::COMPETITION]->getValue();

            if ($data[AttributeType::CATEGORY_PRODUCTS_AND_SERVICES]->getValue() === null) {
                $categoryIds = '';
            }
            else {
                $categoryIds = implode(', ', $data[AttributeType::CATEGORY_PRODUCTS_AND_SERVICES]->getValue());
            }

            //add the each result to results
            $results[] = [
                'keyword' => $keyword,
                'searchVolume' => $searchVolume,
                'averageCpc' => ($averageCpc === null) ? 0 : $averageCpc->getMicroAmount(),
                'competition' => $competition,
                'categoryIds' => $categoryIds
            ];
        }

        return $results;
    }

    public function getEstimatedTraffic($data)
    {
        /*AdWordsServices $adWordsServices,
        AdWordsSession $session*/

        $trafficEstimatorService = $this->adWordsService->get(TrafficEstimatorService::class, TrafficEstimatorService::class);

        // Create keywords. Up to 2000 keywords can be passed in a single request.
        $keywords = [];

        $keyword = new Keyword();
        $keyword->setText('mars cruise');
        $keyword->setMatchType(KeywordMatchType::BROAD);
        $keywords[] = $keyword;

        $keyword = new Keyword();
        $keyword->setText('cheap cruise');
        $keyword->setMatchType(KeywordMatchType::PHRASE);
        $keywords[] = $keyword;

        $keyword = new Keyword();
        $keyword->setText('cruise');
        $keyword->setMatchType(KeywordMatchType::EXACT);
        $keywords[] = $keyword;

        // Create a keyword estimate request for each keyword.
        $keywordEstimateRequests = [];
        foreach ($keywords as $keyword) {
            $keywordEstimateRequest = new KeywordEstimateRequest();
            $keywordEstimateRequest->setKeyword($keyword);
            $keywordEstimateRequests[] = $keywordEstimateRequest;
        }

        // Negative keywords don't return estimates, but adjust the estimates of the
        // other keywords in the hypothetical ad group.
        $negativeKeywords = [];

        $keyword = new Keyword();
        $keyword->setText('moon walk');
        $keyword->setMatchType(KeywordMatchType::BROAD);
        $negativeKeywords[] = $keyword;
        // Create ad group estimate requests.
        $adGroupEstimateRequest = new AdGroupEstimateRequest();
        $adGroupEstimateRequest->setKeywordEstimateRequests(
            $keywordEstimateRequests
        );
        $money = new Money();
        $money->setMicroAmount(1000000);
        $adGroupEstimateRequest->setMaxCpc($money);
        // Create campaign estimate requests.
        $campaignEstimateRequest = new CampaignEstimateRequest();
        $campaignEstimateRequest->setAdGroupEstimateRequests(
            [$adGroupEstimateRequest]
        );
        // Optional: Set additional criteria for filtering estimates.
        // See http://code.google.com/apis/adwords/docs/appendix/countrycodes.html
        // for a detailed list of country codes.
        // Set targeting criteria. Only locations and languages are supported.
        $unitedStates = new Location();
        $unitedStates->setId(2840);

        // See http://code.google.com/apis/adwords/docs/appendix/languagecodes.html
        // for a detailed list of language codes.
        $english = new Language();
        $english->setId(1000);

        $campaignEstimateRequest->setCriteria([$unitedStates, $english]);

        // Create selector.
        $selector = new TrafficEstimatorSelector();
        $selector->setCampaignEstimateRequests([$campaignEstimateRequest]);

        // Optional: Request a list of campaign level estimates segmented by
        // platform.
        $selector->setPlatformEstimateRequested(true);

        $result = $trafficEstimatorService->get($selector);

        $platformEstimates = $result->getCampaignEstimates()[0]->getPlatformEstimates();
        if ($platformEstimates !== null) {
            foreach ($platformEstimates as $platformEstimate) {
                if ($platformEstimate->getMinEstimate() !== null
                    && $platformEstimate->getMaxEstimate() !== null) {
                    printf(
                        "Results for the platform with ID %d and name '%s':\n",
                        $platformEstimate->getPlatform()->getId(),
                        $platformEstimate->getPlatform()->getPlatformName()
                    );
                    self::printMeanEstimate(
                        $platformEstimate->getMinEstimate(),
                        $platformEstimate->getMaxEstimate()
                    );
                }
            }
        }

        $keywordEstimates = $result->getCampaignEstimates()[0]->getAdGroupEstimates()[0]->getKeywordEstimates();
        $estimatesCount = count($keywordEstimates);
        for ($i = 0; $i < $estimatesCount; $i++) {
            $keywordEstimateRequest = $keywordEstimateRequests[$i];
            // Skip negative keywords, since they don't return estimates.
            if ($keywordEstimateRequest->getIsNegative() !== true) {
                $keyword = $keywordEstimateRequest->getKeyword();
                $keywordEstimate = $keywordEstimates[$i];

                if ($keywordEstimate->getMin() !== null
                    && $keywordEstimate->getMax() !== null) {
                    // Print the mean of the min and max values.
                    printf(
                        "Results for the keyword with text '%s' and match type '%s':\n",
                        $keyword->getText(),
                        $keyword->getMatchType()
                    );
                    self::printMeanEstimate(
                        $keywordEstimate->getMin(),
                        $keywordEstimate->getMax()
                    );
                }
            }
        }
    }


    /**
     * Set keyword settings.
     *
     * @param string $customerId
     * @param string $adGroupId
     * @param string $keywordId
     * @param string $operator
     * @param string $matchType
     * @param string $status
     * @param string $finalUrl
     * @param string $bidType
     * @param string $microAmount
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setKeywordsSetting(string $customerId, string $adGroupId, string $keywordId, string $operator, string $matchType, string $status, string $finalUrl, string $bidType, string $microAmount)
    {
        $adGroupCriterionService = $this->adWordsService->getService(AdGroupCriterionService::class, $customerId);

        // Create the first keyword criterion.
        $keyword = new Keyword();
        $keyword->setId($keywordId);

        if (isset($data->matchType)):
            $keyword->setMatchType($matchType); // KeywordMatchType::EXACT;
        endif;

        // Create biddable ad group criterion.
        $adGroupCriterion = new BiddableAdGroupCriterion();
        $adGroupCriterion->setAdGroupId($adGroupId);
        $adGroupCriterion->setCriterion($keyword);

        // Set additional settings (optional).
        if (isset($data->status)):
            $adGroupCriterion->setUserStatus($status);//UserStatus::PAUSED
        endif;

        if (isset($data->finalUrl)):
            $adGroupCriterion->setFinalUrls(
                new UrlList([$finalUrl])
            );
        endif;

        $bid = null;

        // Set bids (optional).
        if (isset($bidType)):
            switch ($bidType):
                case 'cpc':
                    $bid = new CpcBid();
                    $money = new Money();
                    $money->setMicroAmount($microAmount);
                    $bid->setBid($money);
                    break;
                default:
                    if (!$bid) {
                        throw  new EndPointRequestException("The bid type is not supported.", 400);
                    }
                    break;
            endswitch;

            $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
            $biddingStrategyConfiguration->setBids([$bid]);
            $adGroupCriterion->setBiddingStrategyConfiguration(
                $biddingStrategyConfiguration
            );

        endif;

        // Create an ad group criterion operation and add it to the list.
        $operation = new AdGroupCriterionOperation();
        $operation->setOperand($adGroupCriterion);
        $operation->setOperator($operator ?? Operator::SET);

        $result = $this->call(function () use ($adGroupCriterionService, $operation) {
            return $adGroupCriterionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'userStatus' => $result->getUserStatus(),
            'approvalStatus' => $result->getApprovalStatus(),
            'systemServingStatus' => $result->getSystemServingStatus(),
        ];
    }
}
