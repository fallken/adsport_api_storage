<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;
use App\Exceptions\EndPointRequestException;
use stdClass;

class AddCalloutExtensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        $calloutsOriginal = @json_decode($this->input('callouts'));

        if (!$calloutsOriginal) {
            $calloutsOriginal = $this->input('urlCustomParameters');
        }

        if (!is_array($calloutsOriginal)) {
            $calloutsOriginal = [];
        }

        $callouts = [];

        foreach ($calloutsOriginal as $callout) {

            if (!($callout instanceof stdClass)) {
                if (is_array($callout)) {
                    $callout = (object)$callout;
                }
                else {
                    $callout = json_decode($callout);
                }
            }

            if (!isset($callout->value)) {
                throw new EndPointRequestException("Bad callout exception. Payload is: " . json_encode($callout),422);
            }

            if (!isset($callout->feedId)) {
                $callout->feedId = null;
            }

            $callouts[] = $callout;
        }

        $this->merge(['callouts' => $callouts]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'campaignId' => ['required', 'string'],
            'callouts' => ['required', 'array', 'max:10', 'min:1']
        ];
    }
}
