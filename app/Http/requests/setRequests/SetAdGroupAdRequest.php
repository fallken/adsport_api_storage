<?php

namespace App\Http\requests\setRequests;

use App\Exceptions\EndPointRequestException;

use Anik\Form\FormRequest;

class SetAdGroupAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        // Ad headlines validation.
        $headlines = @json_decode($this->input('headlines'));

        if (!$headlines) {
            $headlines = $this->input('headlines');
        }

        if (!isset($headlines->headline_1) || !isset($headlines->headline_2) || !isset($headlines->headline_3)) {
            throw new EndPointRequestException("Bad ad headlines exception. Payload data is: " . json_encode($headlines), 422);
        }

        $this->merge(['headlines' => (object)[
            'headline1' => $headlines->headline_1,
            'headline2' => $headlines->headline_2,
            'headline3' => $headlines->headline_3,
        ]]);

        // Ad description validation.
        $descriptions = @json_decode($this->input('descriptions'));

        if (!$descriptions) {
            $descriptions = $this->input('descriptions');
        }

        if (!isset($descriptions->description_1) || !isset($descriptions->description_2)) {
            throw new EndPointRequestException("Bad ad descriptions exception. Payload data is: " . json_encode($descriptions), 422);
        }

        $this->merge(['descriptions' => (object)[
            'description1' => $descriptions->description_1,
            'description2' => $descriptions->description_2,
        ]]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'adGroupId' => 'required|string',
            'customerClientId' => 'required|string',
            'status' => 'nullable|in:ENABLED,REMOVED,PAUSED',
            'adType' => 'nullable|string|in:RESPONSIVE_SEARCH_AD',
            'path1' => 'nullable|string',
            'path2' => 'nullable|string',
            'operator' => 'nullable|string',
            'adId' => 'nullable|string',
            'finalUrl' => 'required|string',
            'headlines' => 'required',
            'descriptions' => 'required',
        ];
    }
}
