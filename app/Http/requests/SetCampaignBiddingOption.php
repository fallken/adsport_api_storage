<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class SetCampaignBiddingOption extends FormRequest
{
    //todo: campaign bidding option
    protected function rules()
    {
        return [
            'customerClientId'=>'required|string',
            'type'=>'required|in:MANUAL_CPC,ENHANCED_MANUAL_CPC,MANUAL_CPM,PAGE_ONE_PROMOTED,TARGET_SPEND,TARGET_CPA,
            TARGET_ROAS,MAXIMIZE_CONVERSIONS,MAXIMIZE_CONVERSION_VALUE,TARGET_OUTRANK_SHARE','NONE','UNKNOWN',
            'data'=>'required|string'
        ];
    }

    protected function messages () {
        return [];
    }
}
