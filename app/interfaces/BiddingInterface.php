<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 7/2/2019
 * Time: 12:15 PM
 */

namespace App\interfaces;


interface BiddingInterface {
    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * includes both set manual and set enhanced cpc
     * desc: will set manual cpc scheme for a campaign for specific user . 
     */
    public function setManualCPC($customerId,$data);



    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setMaximizeConversions($customerId,$data);




    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * desc: will set set Maximize Clicks scheme for a campaign for specific user .
     */
    public function setMaximizeClicks($customerId,$data);

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * lvl:campaign and portfolio
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetImpressionShare($customerId,$data);

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * lvl:campaign and portfolio
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetROAS(string $customerId,$data);


    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * lvl:campaign and portfolio
     * desc: will set set Target CPA scheme for a campaign for specific user .
     */
    public function setTargetCPA($customerId,$data);
}