<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetUniversalCampaignAppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     */
    public function authorize()
    {
        if ($this->has('isExplicitlyShared')) {
            $this->merge(['isExplicitlyShared' => $this->input('isExplicitlyShared') === "true"]);
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'name' => 'required|string',
            'campaignId' => 'nullable|string',
            'microAmount' => 'nullable|string',

            'status' => 'required|in:ENABLED,REMOVED,PAUSED',

            'deliveryMethod' => 'required|string',
            'startDay' => 'string',
            'endDay' => 'string',

            'budgetName' => 'required|string',
            'isExplicitlyShared' => 'required|bool',
            'description1' => 'required|string|min:5|max:15',
            'description2' => 'required|string|min:5|max:15',
            'description3' => 'required|string|min:5|max:15',
            'description4' => 'required|string|min:5|max:15',
            'appId' => 'required|string',
            'appMarket' => 'required|string',
            'appBiddingStrategyGoalType' => 'required|string',
            'operator' => 'required|string'
        ];
    }
}
