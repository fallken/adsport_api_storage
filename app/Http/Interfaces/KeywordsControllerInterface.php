<?php

namespace App\Http\Interfaces;

use App\Http\requests\setRequests\SetKeywordsSettingRequest;

interface KeywordsControllerInterface
{
    /**
     * @OA\Post(
     *     path="/set/setKeywordsSetting",
     *     operationId="/setKeywordsSetting",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="The id of ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="71237816126")
     *     ),
     *     @OA\Parameter(
     *         name="keywordId",
     *         in="query",
     *         description="The id of keyword",
     *         required=true,
     *         @OA\Schema(type="string", default="296619402213")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="operator",
     *         required=true,
     *         @OA\Schema(type="string", default="SET")
     *     ),
     *     @OA\Parameter(
     *         name="matchType",
     *         in="query",
     *         description="matchType",
     *         required=true,
     *         @OA\Schema(type="string", default="PHRASE")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status",
     *         required=true,
     *         @OA\Schema(type="string", default="ENABLE")
     *     ),
     *     @OA\Parameter(
     *         name="finalUrl",
     *         in="query",
     *         description="finalUrl",
     *         required=true,
     *         @OA\Schema(type="string", default="https://www.myhome.com")
     *     ),
     *     @OA\Parameter(
     *         name="bidType",
     *         in="query",
     *         description="bidType",
     *         required=true,
     *         @OA\Schema(type="string", default="cpc")
     *     ),
     *     @OA\Parameter(
     *         name="microAmount",
     *         in="query",
     *         description="microAmount",
     *         required=true,
     *         @OA\Schema(type="string", default="5670000")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="Response: adId, adGroupId, status",
     *     ),
     * )
     *
     * Set keyword setting.
     *
     * @param SetKeywordsSettingRequest $request
     *
     * @return mixed
     */
    public function setKeywordsSetting(SetKeywordsSettingRequest $request);
}
