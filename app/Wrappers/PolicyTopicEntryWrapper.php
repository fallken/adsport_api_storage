<?php

namespace App\Wrappers;

class PolicyTopicEntryWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
//        setPolicyTopicConstraints = $data->setPolicyTopicConstraints()[]; TODO: Implement PolicyTopicConstraint wrapper
//        getPolicyTopicEvidences = $data->getPolicyTopicEvidences()[]; TODO: Implement PolicyTopicEvidence wrapper

        return (object)[
            'policyTopicEntryType' => $data->getPolicyTopicEntryType(),
            'policyTopicId' => $data->getPolicyTopicId(),
            'policyTopicName' => $data->getPolicyTopicName(),
            'policyTopicHelpCenterUrl' => $data->getPolicyTopicHelpCenterUrl(),

//            'GeoTargetingRestriction' => AdWordsWrapper::toObject($geoTargetingRestriction)

//            'PolicySummaries' => $policySummaries,
        ];
    }
}
