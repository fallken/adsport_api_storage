<?php

namespace App\Wrappers;

class LocationWrapper implements AdWordsWrapperInterface
{
    use CriterionTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $locations = $data->getParentLocations();

        $parentLocations = null;

        if (is_array($locations)) {

            $parentLocations = [];

            foreach ($locations as $locaion) {

                $parentLocations [] = WrapperBridge::toObject($locaion);
            }
        }

        return (object)array_merge(static::append($data),
            [
                'locationName' => $data->getLocationName(),
                'displayType' => $data->getDisplayType(),
                'targetingStatus' => $data->getTargetingStatus(),

                'parentLocations' => $parentLocations,
            ]
        );
    }
}
