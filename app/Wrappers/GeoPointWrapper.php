<?php

namespace App\Wrappers;

class GeoPointWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data)
    {
        return (object)[
            'latitudeInMicroDegrees' => $data->getLatitudeInMicroDegrees(),
            'longitudeInMicroDegrees' => $data->getLongitudeInMicroDegrees(),
        ];
    }
}
