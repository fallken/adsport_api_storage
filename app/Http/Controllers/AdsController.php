<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 9:07 AM
 */

namespace App\Http\Controllers;

use App\Http\Interfaces\AdsControllerInterface;
use App\Http\requests\setRequests\SetAdGroupRequest;
use App\Http\requests\addRequests\AddCampaignSettingRequest;
use App\Http\requests\addRequests\AddKeywordRequest;
use App\Http\requests\ChangeAdgroupNameRequest;
use App\Http\requests\ChangeAdgroupStatusRequest;
use App\Http\requests\ChangeCampaignNameRequest;
use App\Http\requests\ChangeKeywordNameRequest;
use App\Http\requests\ChangeKeywordStatusRequest;
use App\Http\requests\getRequests\GetKeywordRecommendationRequest;
use App\Http\Requests\getRequests\GetKeyWordsRequest;
use App\Http\requests\setRequests\SetAdGroupAdRequest;
use App\interfaces\AdgroupAdInterface;
use App\interfaces\AdGroupInterface;
use App\interfaces\CampaignsInterface;
use App\interfaces\KeywordsInterface;
use App\Http\requests\ChangeCampaignStatusRequest;
use App\repos\CampaignsRepository;
use Illuminate\Http\Request;

class AdsController extends Controller implements AdsControllerInterface
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepo;

    private $keywordsRepo, $adGroupRepo, $adgroupAdRepo;

    public function __construct(CampaignsInterface $campaignInterface,
                                KeywordsInterface $keywords, AdGroupInterface $adGroup,
                                AdgroupAdInterface $adgroupAd)
    {
        $this->campaignRepo = $campaignInterface;
        $this->keywordsRepo = $keywords;
        $this->adGroupRepo = $adGroup;
        $this->adgroupAdRepo = $adgroupAd;
    }

    /**
     * @OA\Post(
     *     path="/get/getKeywords",
     *     operationId="/getKeywords",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="Ad group id",
     *         required=true,
     *         @OA\Schema(type="string", default="71171191084")
     *     ),
     *     @OA\Parameter(
     *         name="sortOrder",
     *         in="query",
     *         description="Sort order",
     *         required=false,
     *         @OA\Schema(type="string", enum={"ASCENDING", "DESCENDING"}, default="ASCENDING")
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is : id, type, text, matchType",
     *     ),
     * )
     * @param GetKeyWordsRequest $request
     *
     * @return mixed
     */
    public function getKeywords(GetKeyWordsRequest $request)
    {
        return $this->keywordsRepo->getKeywords(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('sortOrder') ?? ''
        );
    }


    /**
     * @OA\Post(
     *     path="/status/changeKeywordStatus",
     *     operationId="/changeKeywordStatus",
     *     tags={"change status"},
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="adgroupId",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="keywordId",
     *         in="query",
     *         description="id of the corresponding keyword",
     *         required=true,
     *         @OA\Schema(type="string"),
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the keyword . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ENABLED", "REMOVED","PAUSED"})
     *     ),
     *     @OA\Response(
     *      @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is : status,ApprovalStatus,keywordId",
     *     ),
     * )
     */
    public function changeKeywordStatus(ChangeKeywordStatusRequest $request)
    {
        $result = $this->keywordsRepo->changeKeywordStatus($request);
        return $result;
    }


    /**
     * @OA\Post(
     *     path="/status/changeAdgroupStatus",
     *     operationId="/changeAdgroupStatus",
     *     tags={"change status"},
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="adgroupId",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the adgroup . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ENABLED", "REMOVED","PAUSED"})
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : status,campaignId,adgroupId,campaignName",
     *     ),
     * )
     */
    public function changeAdgroupStatus(ChangeAdgroupStatusRequest $request)
    {
        $result = $this->adGroupRepo->changeAdgroupStatus($request);
        return $result;
    }

    /**
     * @OA\Post(
     *     path="/status/changeCampaignStatus",
     *     operationId="/changeCampaignStatus",
     *     tags={"change status"},
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="campaignId",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the campaign . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ENABLED", "REMOVED","PAUSED"})
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : status,settings,campaignId,campaignName",
     *     ),
     * )
     */
    public function changeCampaignStatus(ChangeCampaignStatusRequest $request)
    {
        $result = $this->campaignRepo->changeCampaignStatus($request);
        return $result;
    }


    /**
     *     path="/rename/changeKeywordName",
     *     operationId="/changeKeywordName",
     *     tags={"change name"},
     * @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="adgroupId",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * @OA\Parameter(
     *         name="keywordId",
     *         in="query",
     *         description="id of the corresponding keyword",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     * @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="new name set by the user for the keyword",
     *         required=true,
     *         @OA\Schema(
     *          type="string")
     *     ),
     * @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : newName,ApprovalStatus,keywordId",
     *     ),
     * )
     *
     * @param ChangeKeywordNameRequest $request
     * @return mixed
     */
    public function changeKeywordName(ChangeKeywordNameRequest $request)
    {
//        return $request->all();
        $result = $this->keywordsRepo->changeKeywordName($request);
        return $result;
    }

    /**
     * @OA\Post(
     *     path="/rename/changeCampaignName",
     *     operationId="/changeCampaignName",
     *     tags={"change name"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="new name set by the user for the campaign",
     *         required=true,
     *         @OA\Schema(
     *          type="string")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : name,campaignId,campaignName",
     *     ),
     * )
     */
    /**
     * @param ChangeCampaignNameRequest $request
     */
    public function changeCampaignName(ChangeCampaignNameRequest $request)
    {
        $result = $this->campaignRepo->changeCampaignName($request);
        return $result;
    }

    /**
     * @OA\Post(
     *     path="/rename/changeAdgroupName",
     *     operationId="/changeAdgroupName",
     *     tags={"change name"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string",default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="id of the corresponding adgroup",
     *         required=true,
     *         @OA\Schema(type="string",default="66844116696")
     *     ),
     *    @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="new name set by the user for the adgroup",
     *         required=true,
     *         @OA\Schema(
     *          type="string",default="نام گروه جدید")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : name,adgroupId,campaignName",
     *     ),
     * )
     *
     * @param ChangeAdgroupNameRequest $request
     * @return mixed
     */
    public function changeAdgroupName(ChangeAdgroupNameRequest $request)
    {
        $result = $this->adGroupRepo->changeAdgroupName($request);
        return $result;
    }

    /**
     * @OA\Post(
     *     path="/add/addKeyword",
     *     operationId="/addKeyword",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string",default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="id of the corresponding adgroup",
     *         required=true,
     *         @OA\Schema(type="string",default="66844116696")
     *     ),
     *    @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name set by the user for the keyword",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,default="کلمه کلیدی گوسفندی"
     *           )
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the keyword . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ENABLED", "REMOVED","PAUSED"},default="ENABLED")
     *     ),
     *    @OA\Parameter(
     *         name="finalUrl",
     *         in="query",
     *         description="finalUrl that the user is going to set for the keyword",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         name="amount",
     *         in="query",
     *         description="money price that the user is going to set for the keyword",
     *         required=false,
     *         @OA\Schema(type="string",default="100")
     *     ),
     *     @OA\Parameter(
     *         name="matchType",
     *         in="query",
     *         description="match type for the keyword should be : EXACT , PHRASE , BROAD",
     *         required=false,
     *         @OA\Schema(type="string",enum={"EXACT", "PHRASE","BROAD"},default="EXACT")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",
     *         description="and structure of the response is : name,matchType,ApprovalStatus,keywordId",
     *     ),
     * )
     * @param AddKeywordRequest $request
     * @return mixed
     */
    public function addKeyword(AddKeywordRequest $request)
    {
        $data = [
            'customerClientId' => $request->input('customerClientId'),
            'adGroupId' => $request->input('adGroupId'),
            'name' => $request->input('name'),
            'status' => $request->input('status'),
            'amount' => $request->input('amount'),
            'matchType' => $request->input('matchType')
        ];

        return $this->keywordsRepo->addKeyword($data);
    }

    /**
     * Update or add new ad group.
     *
     * @param SetAdGroupRequest $request
     *
     * @return mixed
     */
    public function setAdGroup(SetAdGroupRequest $request)
    {
        return $this->adGroupRepo->setAdGroup(
            $request->input('customerId'),
            $request->input('campaignId'),
            $request->input('name'),
            $request->input('amount'),
            $request->input('status'),
            $request->input('criterionType'),
            $request->input('adRotationMode'),
            $request->input('TrackingUrlTemplate') ?? null,
            $request->input('urlCustomParameters') ?? [],
            $request->input('doReplace') ?? false,
            $request->input('operator') ?? 'ADD',
            $request->input('adGroupId') ?? ''
        );
    }

    /**
     * @OA\Post(
     *     path="/add/addCampaignSetting",
     *     operationId="/addCampaignSetting",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *     @OA\Schema(type="string",default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="campaignId",
     *         required=true,
     *         @OA\Schema(type="string",default="1703779813")
     *     ),
     *   @OA\Parameter(
     *         name="negative",
     *         in="query",
     *         description="criterias that are going to be set as negative ones",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *   @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="operator for campaign setting operation. ADD,SET,REMOVE",
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ADD","SET","REMOVE"}
     *          ,default="ADD"
     *          )
     *     ),
     *   @OA\Parameter(
     *         name="positive",
     *         in="query",
     *         description="criterias that are going to be set as positive ones",
     *         required=true,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),response="200",description="and structure of the response is : campignId,campignType"))
     *
     * @param AddCampaignSettingRequest $request
     *
     * @return array
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addCampaignSetting(AddCampaignSettingRequest $request)
    {
        return $this->campaignRepo->addCampaignSetting(
            $request->input('customerId'),
            $request->input('campaignId'),
            $request->input('settings')
        );
    }

    /**
     * @OA\Post(
     *     path="/get/getKeywordRecommendations",
     *     operationId="/getKeywordRecommendations",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string",default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="adgroupId is not required but the result would be more precise",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *    @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="the type of the recommendation to check if it is for generatig ideas or stats , ",
     *         required=false,
     *         @OA\Schema(type="string",enum={"IDEAS","STATS"},default="IDEAS")
     *     ),
     *   @OA\Parameter(
     *         name="pageNumber",
     *         in="query",
     *         description="pagination number",
     *         required=false,
     *         @OA\Schema(type="integer"
     *          ,default="0"
     *          )
     *     ),
     *   @OA\Parameter(
     *         name="keywords",
     *         in="query",
     *         description="list of keywords for the for generating ideas based on them,accepts json",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is : keyword,searchVolume,averageCpc,competition,categoryIds",
     *     ),
     * )
     * @param GetKeywordRecommendationRequest $request
     *
     * @return mixed
     */
    public function getKeywordRecommendations(GetKeywordRecommendationRequest $request)
    {
        $data = [
            'customerId' => $request->customerId,
            'adGroupId' => $request->adGroupId,
            'pageNumber' => $request->pageNumber,
            'type' => $request->type,
            'keywords' => json_decode($request->keywords, true)
        ];

        return $this->keywordsRepo->getKeywordRecommendations($data);
    }


    /**
     * Update or add new ad group ad.
     *
     * @param SetAdGroupAdRequest $request
     *
     * @return mixed
     */
    public function setAd(SetAdGroupAdRequest $request)
    {
        return $this->adgroupAdRepo->setAd(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('adType') ?? 'RESPONSIVE_SEARCH_AD',
            $request->input('finalUrl'),
            $request->input('path1') ?? '',
            $request->input('path2') ?? '',
            $request->input('headlines'),
            $request->input('descriptions'),
            $request->input('operator') ?? 'ADD',
            $request->input('status') ?? 'ENABLED',
            $request->input('adId') ?? ''
        );
    }


    /**
     * @OA\Post(
     *     path="/add/addAdSchedule",
     *     operationId="/addAdSchedule",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="261-911-0558"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaignId",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="1703779813"
     *          )
     *     ),
     *   @OA\Parameter(
     *         name="schedules",
     *         in="query",
     *         description="(accepts json) list of schedules for adding user's specified schedules for corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string",default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is :adId,headlines,descriptions",
     *     ),
     * )
     */
    public function addAdSchedule(Request $request)
    {
        return $this->campaignRepo->addCampaignSchedule([
            'customerId' => $request->customerId,
            'campaignId' => $request->campaignId,
            'schedules' => json_decode($request->schedules, true)
        ]);
    }
}
