<?php

use App\repos\CampaignsRepository;
use App\repos\BiddingRepository;

class BiddingRepoTest extends TestCase
{
    //todo:added this bidding repo
    /**
     * @var CampaignsRepository
     */
    private $biddingRepo;
    public $customerId = '541-403-9496';

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->biddingRepo = new BiddingRepository();
    }

    public function testSetManualCPC() {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'operator'=>'SET'
        ];
        $result = $this->biddingRepo->setManualCPC($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }
    public function testSetEnhancedCPC	() {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'operator'=>'SET',
                'enableEnhancedCpc'=>true
        ];
        $result = $this->biddingRepo->setManualCPC($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

    public function testSetTargetCPA() {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'operator'=>'SET',
                'targetCpa'=>'',
                'maxCpcBidCeiling'=>'',

        ];
        $result = $this->biddingRepo->setTargetCPA($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

    public function testSetMaximizeConversions () {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'bidCeiling'=>'50000000',
                'spendTarget'=>'1000000000',
                'operator'=>'SET',
        ];
        $result = $this->biddingRepo->setMaximizeConversions($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

    public function testSetTargetImpressionShare () {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'bidCeiling'=>'50000000',
                'spendTarget'=>'1000000000',
                'operator'=>'SET',
        ];
        $result = $this->biddingRepo->setMaximizeClicks($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

    public function testSetTargetRoas () {//todo: not passed the test (run to check errors)
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'bidCeiling'=>'50000000',
                'bidFloor'=>'30000000',
                'targetRoas'=>1.4,
                'operator'=>'SET',
                'strategyId'=>'2058876868'
        ];
        $result = $this->biddingRepo->setTargetROAS($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

    public function testSetMaximizeClicks  () {
        $customerId = '541-403-9496';
        $data = (object) [
                'campaignId' => '2045454855',
                'targetCpa'=>'50000000',
                'bidCeiling'=>'3000000',
                'spendTarget'=>'3000000',
                'maxCpcBidFloor'=>'1000000',
                'operator'=>'SET',
//                'strategyId'=>'2058876868'
        ];
        $result = $this->biddingRepo->setMaximizeClicks($customerId, $data);
        print_r($result);
        $this->assertIsObject($result);
    }

}
