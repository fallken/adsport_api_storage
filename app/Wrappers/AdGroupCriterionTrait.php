<?php

namespace App\Wrappers;

trait AdGroupCriterionTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function append(&$data)
    {
        $labelArray = null;

        $labels = $data->getLabels();

        if (is_array($labels)) {

            $labelArray = [];

            foreach ($labels as $label) {

                $labelArray = WrapperBridge::toObject($label);
            }
        }

        $criterion = $data->getCriterion();

        return [
            'adGroupId' => $data->getAdGroupId(),
            'criterionUse' => $data->getCriterionUse(),
            'baseAdGroupId' => $data->getBaseAdGroupId(),
            'baseCampaignId' => $data->getBaseCampaignId(),
            'adGroupCriterionType' => $data->getAdGroupCriterionType(),

            'labels' => $labelArray,

            'criterion' => WrapperBridge::toObject($criterion),
        ];
    }
}