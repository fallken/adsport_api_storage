<?php

namespace App\Wrappers;

class FeedItemAttributeErrorWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {

        return (object)[
            'feedAttributeIds' => $data->getFeedAttributeIds(),
            'validationErrorCode' => $data->getValidationErrorCode(),
            'errorInformation' => $data->getErrorInformation(),
        ];
    }
}
