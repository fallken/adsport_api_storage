<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests\addRequests;

use Anik\Form\FormRequest;
use App\Exceptions\EndPointRequestException;
use stdClass;

class AddCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        if ($this->has('doReplace')) {
            $this->merge(['doReplace' => $this->input('doReplace') === "true"]);
        }

        $urlCustomParametersHolder = @json_decode($this->input('urlCustomParameters'));

        if (!$urlCustomParametersHolder) {
            $urlCustomParametersHolder = $this->input('urlCustomParameters');
        }

        if (!is_array($urlCustomParametersHolder)) {
            $urlCustomParametersHolder = [];
        }

        $urlCustomParameters = [];

        foreach ($urlCustomParametersHolder as $parameter) {

            if (!($parameter instanceof stdClass)) {
                if (is_array($parameter)) {
                    $parameter = (object)$parameter;
                }
                else {
                    $parameter = json_decode($parameter);
                }
            }

            if (!isset($parameter->key) || !isset($parameter->value)) {
                throw new EndPointRequestException("Bad url custom parameter exception. Payload is: " . json_encode($parameter), 422);
            }

            $urlCustomParameters[] = $parameter;
        }

        $this->merge(['urlCustomParameters' => $urlCustomParameters]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'name' => 'required|string',
            'microAmount' => 'required|integer',
            'status' => 'required|in:ENABLED,REMOVED,PAUSED',
            'deliveryMethod' => 'required|string',
            'adsChannelType' => 'string',
            'biddingStrategy' => 'string',
            'budgetName' => 'required|string',

            'finalUrlSuffix' => 'nullable|string',

            'trackingUrlTemplate' => 'nullable|string',

            'urlCustomParameters' => 'nullable|array',
            'doReplace' => 'nullable|bool',
        ];
    }
}
