<?php

namespace App\Wrappers;

/**
 * Convert response class to object for google ad words apis.
 *
 * Interface GoogleClassToObjectWrapperInterface
 *
 * @package App\Wrappers
 */
interface AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data);
}
