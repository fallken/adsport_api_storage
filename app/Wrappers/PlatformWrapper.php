<?php

namespace App\Wrappers;

class PlatformWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'id' => $data->getId(),
            'type' => $data->getType(),
            'criterionType' => $data->getCriterionType(),

            'platformName' => $data->getPlatformName(),
        ];
    }
}
