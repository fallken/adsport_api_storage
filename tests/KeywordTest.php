<?php

use App\Copier\Repositories\AccountCopierRepository;

class KeywordTest extends TestCase
{
    /**
     * @var AccountCopierRepository
     */
    private $keywordRepo;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->keywordRepo = new \App\repos\KeywordsRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetKeywordSetting()
    {
        $customerId = '541-403-9496';
        $adGroupId = '71237816126';
        $keywordId = '296619402213';
        $operator = 'SET';
        $matchType = 'PHRASE';
        $status = '';
        $finalUrl = 'https://www.myhome.com';
        $bidType = 'cpc';
        $microAmount = '7000000';

        $result = $this->keywordRepo->setKeywordsSetting(
            $customerId,
            $adGroupId,
            $keywordId,
            $operator,
            $matchType,
            $status,
            $finalUrl,
            $bidType,
            $microAmount
        );

        $this->assertIsArray($result);

        print_r($result);
    }
}
