<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class setAdGroupAdExpandedDynamicSearchAdRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'adGroupId' => 'required|string',
            'adId' => 'required|string',
            'description' => 'required|string',
            'description2' => 'required|string',
        ];
    }
}
