<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetSharedBiddingStrategyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'strategyId' => 'nullable|string',
            'operator' => 'required|string',
            'name' => 'required|string',
            'status' => 'required|string',
            'strategyType' => 'required|string',
            'microAmount' => 'required|string',
            'bidCeilingAmount' => 'nullable|string',
            'bidFloorAmount' => 'nullable|string',
        ];
    }
}
