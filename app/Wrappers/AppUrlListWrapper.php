<?php

namespace App\Wrappers;

class AppUrlListWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {

        $AppUrls = null;

        $finalAppUrls = $data->getAppUrls();

        if (is_array($finalAppUrls)) {

            $AppUrls = [];

            foreach ($finalAppUrls as $appUrl) {

                $AppUrls[] = WrapperBridge::toObject($appUrl);
            }
        }

        return (object)[
            'appUrls' => $AppUrls,
        ];
    }
}
