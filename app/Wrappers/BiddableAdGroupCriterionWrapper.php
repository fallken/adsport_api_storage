<?php

namespace App\Wrappers;

class BiddableAdGroupCriterionWrapper implements AdWordsWrapperInterface
{
    use AdGroupCriterionTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $finalUrls = $data->getFinalUrls();
        $qualityInfo = $data->getQualityInfo();
        $firstPageCpc = $data->getFirstPageCpc();
        $topOfPageCpc = $data->getTopOfPageCpc();
        $finalAppUrls = $data->getFinalAppUrls();
        $finalMobileUrls = $data->getFinalMobileUrls();
        $firstPositionCpc = $data->getFirstPositionCpc();
        $urlCustomParameters = $data->getUrlCustomParameters();
        $biddingStrategyConfiguration = $data->getBiddingStrategyConfiguration();

        return (object)array_merge(static::append($data), [
                'userStatus' => $data->getUserStatus(),
                'bidModifier' => $data->getBidModifier(),
                'approvalStatus' => $data->getApprovalStatus(),
                'baseCampaignId' => $data->getBaseCampaignId(),
                'finalUrlSuffix' => $data->getFinalUrlSuffix(),
                'disapprovalReasons' => $data->getDisapprovalReasons(),
                'trackingUrlTemplate' => $data->getTrackingUrlTemplate(),
                'systemServingStatus' => $data->getSystemServingStatus(),

                'finalUrls' => WrapperBridge::toObject($finalUrls),
                'firstPageCpc' => WrapperBridge::toObject($firstPageCpc),
                'topOfPageCpc' => WrapperBridge::toObject($topOfPageCpc),
                'qualityScore' => WrapperBridge::toObject($qualityInfo),
                'finalAppUrls' => WrapperBridge::toObject($finalAppUrls),
                'finalMobileUrls' => WrapperBridge::toObject($finalMobileUrls),
                'firstPositionCpc' => WrapperBridge::toObject($firstPositionCpc),
                'urlCustomParameters' => WrapperBridge::toObject($urlCustomParameters),
                'biddingStrategyConfiguration' => WrapperBridge::toObject($biddingStrategyConfiguration),
            ]
        );
    }
}
