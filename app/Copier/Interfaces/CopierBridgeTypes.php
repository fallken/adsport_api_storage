<?php

namespace App\Copier\Interfaces;

interface CopierBridgeTypes
{
    const SERVICE_CAMPAIGN = 'campaign';
    const SERVICE_AD_GROUP = 'adGroup';

    const SETTING_CRITERION = 'criterion';
    const SETTING_EXTENSIONS = 'extensions';
}