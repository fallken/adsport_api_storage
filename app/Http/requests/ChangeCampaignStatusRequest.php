<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:44 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class ChangeCampaignStatusRequest extends FormRequest
{


    protected function rules()
    {
        return [
            'campaignId'=>'required|string',
            'customerId'=>'required|string',
            'status'=>'required|in:ENABLED,REMOVED,PAUSED'
        ];
    }

    protected function messages () {
        return [];
    }
}
