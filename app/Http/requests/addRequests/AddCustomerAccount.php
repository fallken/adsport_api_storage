<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests\addRequests;


use Anik\Form\FormRequest;

class AddCustomerAccount extends FormRequest
{

    protected function rules()
    {
        return [
            'accountName'=>'required|string',
            'currencyCode'=>'required|string',
            'timeZone'=>'required|string',
        ];
    }

    protected function messages () {
        return [];
    }
}
