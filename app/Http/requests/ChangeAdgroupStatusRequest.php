<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class ChangeAdgroupStatusRequest extends FormRequest
{

    protected function rules()
    {
        return [
            'customerId'=>'required|string',
            'adgroupId'=>'required|string',
            'status'=>'required|in:ENABLED,REMOVED,PAUSED'
        ];
    }

    protected function messages () {
        return [];
    }
}
