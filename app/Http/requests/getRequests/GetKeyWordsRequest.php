<?php

namespace App\Http\Requests\getRequests;

use Anik\Form\FormRequest;

class GetKeyWordsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'adGroupId' => ['required', 'string'],
            'sortOrder' => ['nullable', 'string']
        ];
    }
}
