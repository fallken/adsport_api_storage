<?php

namespace App\Wrappers;

class AdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $finalAppUrls = null;
        $finalAppUrlsOriginal = $data->getFinalAppUrls();

        if (is_array($finalAppUrlsOriginal)) {

            $finalAppUrls = [];

            foreach ($finalAppUrlsOriginal as $item) {

                $finalAppUrls[] = WrapperBridge::toObject($item);
            }
        }

        $urlData = null;
        $urlDataOriginal = $data->getUrlData();

        if (is_array($urlDataOriginal)) {

            $urlData = [];

            foreach ($urlDataOriginal as $item) {

                $urlData[] = WrapperBridge::toObject($item);
            }
        }

        $urlCustomParameters = $data->getUrlCustomParameters();

        return (object)[
            'id' => $data->getId(),
            'url' => $data->getUrl(),
            'type' => $data->getType(),
            'adType' => $data->getAdType(),
            'automated' => $data->getAutomated(),
            'finalUrls' => $data->getFinalUrls(),
            'displayUrl' => $data->getDisplayUrl(),
            'finalUrlSuffix' => $data->getFinalUrlSuffix(),
            'finalMobileUrls' => $data->getFinalMobileUrls(),
            'devicePreference' => $data->getDevicePreference(),
            'trackingUrlTemplate' => $data->getTrackingUrlTemplate(),
            'systemManagedEntitySource' => $data->getSystemManagedEntitySource(),

            'urlData' => $urlData,
            'finalAppUrls' => $finalAppUrls,
            
            'urlCustomParameters' => WrapperBridge::toObject($urlCustomParameters),
        ];
    }
}
