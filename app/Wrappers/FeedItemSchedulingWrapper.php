<?php

namespace App\Wrappers;

class FeedItemSchedulingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param object $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $items = null;

        $feedItemSchedules = $data->getFeedItemSchedules();

        if (is_array($feedItemSchedules)) {

            $items = [];

            foreach ($feedItemSchedules as $feed) {

                $items[] = WrapperBridge::toObject($feed);
            }
        }

        return (object)[
            'feedItemSchedules' => $items,
        ];
    }
}
