<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetCampaignFinalUrlSuffixRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'campaignId' => 'required|string',
            'finalUrlSuffix' => 'nullable|string',
        ];
    }
}
