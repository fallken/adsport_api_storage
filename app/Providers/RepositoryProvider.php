<?php

namespace App\Providers;


use App\interfaces\AdgroupAdInterface;
use App\interfaces\AdGroupInterface;
use App\interfaces\AdWordsSettingsInterface;
use App\interfaces\BiddingInterface;
use App\interfaces\CampaignsInterface;
use App\interfaces\CurrencyServiceInterface;
use App\interfaces\CustomerAccountInterface;
use App\interfaces\KeywordsInterface;
use App\repos\AdGroupAdRepository;
use App\repos\AdGroupRepository;
use App\repos\AdWordsSettingsRepository;
use App\repos\BiddingRepository;
use App\repos\CampaignsRepository;
use App\repos\CurrencyRepository;
use App\repos\CustomerAccountRepository;
use App\repos\KeywordsRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CampaignsInterface::class,CampaignsRepository::class);
        $this->app->singleton(KeywordsInterface::class,KeywordsRepository::class);
        $this->app->singleton(AdGroupInterface::class,AdGroupRepository::class);
        $this->app->singleton(CurrencyServiceInterface::class,CurrencyRepository::class);
        $this->app->singleton(AdgroupAdInterface::class,AdGroupAdRepository::class);
        $this->app->singleton(CustomerAccountInterface::class,CustomerAccountRepository::class);
        $this->app->singleton(AdWordsSettingsInterface::class,AdWordsSettingsRepository::class);
        $this->app->singleton(BiddingInterface::class,BiddingRepository::class);
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {

    }
}
