<?php

use App\repos\CampaignsRepository;
use Google\AdsApi\AdWords\v201809\cm\Operator;

class CampaignSettingTest extends TestCase
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepository;
    public $customerId = '541-403-9496';

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = resolve(CampaignsRepository::class);
    }

    /**
     * Set campaign network settings.
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testNetworkSetting()
    {
        $result = $this->campaignRepository->setNetworkSetting(
            '541-403-9496',
            '2045454855',
            true,
            true,
            null,
            null
        );

        $this->assertIsArray($result);

        $params = [
            'customerClientId' => '541-403-9496',
            'campaignId' => '2045454855',
            'contentNetwork' => true,
            'searchNetwork' => false,
        ];

        $response = $this->post('/set/setCampaignNetworkSetting', $params);

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }

    public function testGeoTargetting()
    {
        $data = [
            (object)[
                'locationId' => '21137L',//california
                'campaignId' => '2045454855',
                'operator' => 'ADD',
                'negative' => false
            ],
            (object)[
                'locationId' => '2484L',//mexico
                'campaignId' => '2045454855',
                'operator' => 'ADD',
                'negative' => true
            ]
        ];

        $customerId = '541-403-9496';

        $result = $this->campaignRepository->setGeoTarget($data, $customerId);

        print_r($result);

        $this->assertIsArray($result);
    }


    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGeoDistanceTargetting()
    {

        $data = [
            (object)[
                'type' => 'address',
                'streetAddress' => '38 avenue de l\'Opéra',
                'cityName' => 'Paris',
                'postalCode' => '75002',
                'countryCode' => 'FR',
                'campaignId' => '2045454855',
                'operator' => 'ADD',
                'negative' => false,
                'radius' => 10
            ],
            (object)[
                'type' => 'point',
                'latitude' => '35.778997',
                'longtitude' => '51.425698',
                'campaignId' => '2045454855',
                'operator' => 'ADD',
                'negative' => false,
                'radius' => 10
            ]
        ];

        $customerId = '541-403-9496';

        $result = $this->campaignRepository->setGeoDistanceTarget($data, $customerId);

        $this->assertIsArray($result);

        $response = $this->post(
            '/set/setGeoLocationDistance',
            ['customerClientId' => $customerId, 'data' => $data]
        );

        $response->assertResponseOk();

        $this->assertJson($response->response->getContent());

        print_r($response->response->getContent());
    }


    public function testLanguageTargetting()
    {
        $customerId = '541-403-9496';
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testDynamicSearchAdsSetting()
    {
        $customerId = '541-403-9496';

        $result = $this->campaignRepository->setDynamicSearchAdSetting($customerId, '2045454855', 'www.app.google.com', 'en');

        $this->assertIsArray($result);

        print_r($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testFinalUrlSuffix()
    {
        $result = $this->campaignRepository->setFinalUrlSuffix('541-403-9496', '2045454855', 'Test-final-url-suffix');

        $this->assertIsArray($result);

        print_r($result);
    }


    /**
     * Test campaign add language criterion setting.
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddLanguage()
    {
        $customerId = '541-403-9496';
        $campaignId = '2045454855';

        $settings = [
            (object)['id' => '1019', 'operator' => 'ADD', 'type' => 'language', 'isNegative' => true],
            (object)['id' => '1021', 'operator' => 'ADD', 'type' => 'language', 'isNegative' => false],
        ];

        $result = $this->campaignRepository->addCampaignSetting($customerId, $campaignId, $settings);

        $this->assertIsArray($result);

        print_r($result);

        $response = $this->post('/add/addCampaignSetting', [
            'customerId' => $customerId,
            'campaignId' => $campaignId,
            'settings' => $settings,
        ]);

        $response->assertResponseOk();

        print_r($response->response->getContent());
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetBudget()
    {
        $name = 'The new budget';
        $microAmount = '44000000';
        $deliveryMethod = 'STANDARD';
        $isExplicitlyShared = false;

//        $result = $this->campaignRepository->setBudget(
//            $this->customerId,
//            $name,
//            $microAmount,
//            $deliveryMethod,
//            $isExplicitlyShared
//        );
//
//        print_r($result);
//
//        $this->assertIsArray($result);

        $response = $this->post('/set/setCampaignBudget', [
                'customerClientId' => $this->customerId,
                'name' => $name,
                'microAmount' => $microAmount,
                'deliveryMethod' => $deliveryMethod,
                'isExplicitlyShared' => $isExplicitlyShared
            ]
        );

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }

    public function testSetBudgetDeliveryMethod()
    {
        # types  : STANDARD , ACCELERATED , UNKNOWN
        $data = (object)[
            'deliveryMethod' => 'ACCELERATED',
            'budgetId' => '2117405698'
        ];
        $customerId = $this->customerId;
        $result = $this->campaignRepository->setBudgetDeliveryMethod($customerId, $data);

        print_r($result);

        $this->assertIsObject($result);
    }


}
