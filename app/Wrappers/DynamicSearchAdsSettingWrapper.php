<?php

namespace App\Wrappers;

class DynamicSearchAdsSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        // TODO: Implementing PageFeedWrapper for this wrapper.

        return (object) [
            'domainName' => $data->getDomainName(),
            'languageCode' => $data->getLanguageCode(),
            'useSuppliedUrlsOnly' => $data->getUseSuppliedUrlsOnly(),
        ];
    }
}
