<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetAdGroupTrackingUrlTemplateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'adGroupId' => 'required|string',
            'trackingUrlTemplate' => 'nullable|string',
        ];
    }
}
