<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests\accountRequests;

use Anik\Form\FormRequest;

class SendInvitationLink extends FormRequest
{

    protected function rules()
    {
        return [
           'customerId'=>'required|string',
            'status'=>'nullable|string',
            'operator'=>'nullable|string'
        ];
    }

    protected function messages () {
        return [];
    }
}
