<?php

use Google\AdsApi\AdWords\v201809\cm\AppFeedItemAppStore;
use Illuminate\Support\Str;
use App\repos\AdGroupRepository;

class AdGroupTest extends TestCase
{
    /**
     * @var AdGroupRepository
     */
    private $adGroupRepo;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->adGroupRepo = resolve(AdGroupRepository::class);
    }

    public function testSetExpandedDynamicAdd()
    {
        $customerId = '541-403-9496';

        $data = (object)[
            'description1' => 'here is some description',
            'description2' => 'here is another description',
            'adgroupId' => '71237816126',
            'campaignId' => '2045454855'
        ];
        $this->adGroupRepo->setExpandedResponsiveAd($customerId, $data);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetTrackingUrlTemplate()
    {
        $sourceCustomer = '541-403-9496';
        $adGroupId = '71237816126';
        $trackingUrlTemplate = 'https://your-site.com/tracking-url-template-' . Str::random() . '{lpurl}?utm_campaign={campaignid}&adgroupid={adgroupid}&utm_content={creative}&utm_term={keyword}';

        $result = $this->adGroupRepo->setTrackingUrlTemplate(
            $sourceCustomer,
            $adGroupId,
            $trackingUrlTemplate
        );

        $this->assertIsArray($result);

        print_r($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testUrlCustomParameters()
    {
        $customerId = '541-403-9496';
        $adGroupId = '71237816126';

        $urlCustomParameters = [
            (object)[
                'key' => 'adgroupid',
                'value' => '2045454855',
                'isRemoved' => false
            ],
            (object)[
                'key' => 'test',
                'value' => 'sdfsdfsdfsdf',
                'isRemoved' => false
            ],
            (object)[
                'key' => 'danger',
                'value' => 'your-are-very-lucky',
                'isRemoved' => false
            ]
        ];

        $result = $this->adGroupRepo->setUrlCustomerParameters(
            $customerId,
            $adGroupId,
            $urlCustomParameters,
            false
        );

        print_r($result);

        $this->assertIsArray($result);
    }

    /**
     * Set ad group extensions.
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testSetExtensions()
    {
        $customerClientId = '261-911-0558';
        $adGroupId = '64912739685';

        $extensions = [
            (object)[
                'operator' => 'ADD',
                'type' => 'sitelink',
                'links' => ['text' => 'Test', 'url' => 'https://google.com/sdafsdf--fg-dfg-gdg-dfg-df-g']
            ],
            (object)[
                'type' => 'app',
                'operator' => 'ADD',
                'text' => 'Clone me!',
                'appId' => 'com.example.myapp',
                'store' => AppFeedItemAppStore::GOOGLE_PLAY,
                'link' => 'https://play.google.com/store/apps/details?id=com.example.myapp',
            ],
            (object)[
                'type' => 'callout',
                'operator' => 'ADD',
                'callouts' => [
                    ['value' => 'Test', 'feedId' => null],
                    ['value' => 'Test 1', 'feedId' => null],
                ]
            ],
        ];

        $result = $this->adGroupRepo->setExtensions($customerClientId, $adGroupId, $extensions);

        print_r($result);

        $this->assertIsArray($result);

        $response = $this->post('/set/setAdGroupExtensions', [
            'customerClientId' => $customerClientId,
            'adGroupId', $adGroupId,
            'extensions' => $extensions,
        ]);

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }
}
