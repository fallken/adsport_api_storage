<?php

namespace App\Http\Interfaces;

use App\Http\requests\addRequests\AddCampaignRequest;
use App\Http\Requests\addRequests\SetCampaignExtensionsRequest;
use App\Http\requests\SetGeoDistanceRequest;
use App\Http\requests\SetGeoLocationRequest;
use App\Http\requests\SetCampaignBiddingOption;
use App\Http\requests\setRequests\SetBudgetRequest;
use App\Http\requests\setRequests\SetCampaignNetworkSetting;
use App\Http\requests\setRequests\SetCampaignTrackingUrlTemplate;
use App\Http\requests\setRequests\SetCampaignFinalUrlSuffixRequest;
use App\Http\requests\setRequests\SetDynamicSearchAdsSettingRequest;
use App\Http\requests\setRequests\SetCampaignUrlCustomParametersRequest;
use App\Http\requests\setRequests\SetUniversalCampaignAppRequest;

interface CampaignControllerInterface
{
    /**
     * @OA\Post(
     *     path="/add/addCampaign",
     *     operationId="/addCampaign",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="261-911-0558"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="microAmount",
     *         in="query",
     *         description="amount of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="integer"
     *          ,default="10000"
     *          )
     *     ),
     *    @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="name set by the user for the campaign",
     *         required=true,
     *         @OA\Schema(
     *          type="string",default="کمپین تست")
     *     ),
     *    @OA\Parameter(
     *         name="budgetName",
     *         in="query",
     *         description="budgetName set by the user for the campaign",
     *         required=true,
     *         @OA\Schema(
     *          type="string",default="budget name")
     *     ),
     *     @OA\Parameter(
     *         name="startDay",
     *         in="query",
     *         description="startDate for the campaign should be like : 2016-6-1",
     *         required=false,
     *         @OA\Schema(
     *          type="string")
     *     ),
     *     @OA\Parameter(
     *         name="endDay",
     *         in="query",
     *         description="endDate for the campaign should be like : 2016-6-1",
     *         required=false,
     *         @OA\Schema(
     *          type="string")
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="status that the user is going to set for the campaign . PAUSE , ENABLED , REMOVED",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"ENABLED", "REMOVED","PAUSED"}
     *          ,default="ENABLED"
     *          )
     *     ),
     *    @OA\Parameter(
     *         name="deliveryMethod",
     *         in="query",
     *         description="deliveryMethod that the user is going to set for the campaign . STANDARD,ACCELERATED,UNKNOWN",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"STANDARD","ACCELERATED","UNKNOWN"}
     *          ,default="STANDARD"
     *          )
     *     ),
     *    @OA\Parameter(
     *         name="adsChannelType",
     *         in="query",
     *         description="adRotationMode that the user is going to set for the campaign . UNKNOWN,SEARCH,DISPLAY,SHOPPING,MULTI_CHANNEL",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={"MULTI_CHANNEL","SHOPPING","DISPLAY","SEARCH","UNKNOWN"}
     *          ,default="UNKNOWN"
     *          )
     *     ),
     *      @OA\Parameter(
     *         name="biddingStrategy",
     *         in="query",
     *         description="biddingStrategy that the user is going to set for the campaign .
     *          MANUAL_CPC,MANUAL_CPM,PAGE_ONE_PROMOTED,TARGET_SPEND,TARGET_CPA,
     *          TARGET_ROAS,MAXIMIZE_CONVERSIONS,MAXIMIZE_CONVERSION_VALUE,TARGET_OUTRANK_SHARE,NONE,UNKNOWN",
     *         required=true,
     *         @OA\Schema(
     *          type="string"
     *          ,enum={
     *          "MANUAL_CPC","MANUAL_CPM","PAGE_ONE_PROMOTED","TARGET_SPEND",
     *          "TARGET_CPA","TARGET_ROAS","MAXIMIZE_CONVERSIONS","MAXIMIZE_CONVERSION_VALUE"
     *           ,"TARGET_OUTRANK_SHARE","NONE","UNKNOWN"
     *          }
     *          ,default="MANUAL_CPC"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="finalUrlSuffix",
     *         in="query",
     *         description="Reference: /set/setCampaignFinalUrlSuffix",
     *         required=false,
     *         @OA\Schema(
     *          type="string",default="")
     *     ),
     *     @OA\Parameter(
     *         name="trackingUrlTemplate",
     *         in="query",
     *         description="Reference: /set/setCampaignTrackingUrlTemplate",
     *         required=false,
     *         @OA\Schema(
     *          type="string",default="")
     *     ),
     *     @OA\Parameter(
     *         name="urlCustomParameters",
     *         in="query",
     *         description="Reference: /set/setCampaignUrlCustomParameters",
     *         required=false,
     *         @OA\Schema(
     *          type="string",default="")
     *     ),
     *
     *     @OA\Response(
     *   @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="and structure of the response is : name,adgroupId,campaignName",
     *     ),
     * )
     *
     * Update or add new campaign.
     *
     * @param AddCampaignRequest $request
     *
     * @return array
     */
    public function addCampaign(AddCampaignRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setUniversalAppCampaign",
     *     operationId="/setUniversalAppCampaign",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="Identity of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="Name of universal app campaign.",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *    @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="Status of universal app campaign: For example: ENABLED, PAUSED, REMOVED",
     *         required=true,
     *         @OA\Schema(type="string", default="PAUSED", enum={"ENABLED", "PAUSED", "REMOVED"})
     *     ),
     *    @OA\Parameter(
     *         name="startDay",
     *         in="query",
     *         description="Start day for the campaign should be like: 2010-6-1",
     *         required=false,
     *         @OA\Schema(type="string",default="2010-6-1")
     *     ),
     *     @OA\Parameter(
     *         name="endDay",
     *         in="query",
     *         description="End day for the campaign should be like : 2015-6-1",
     *         required=false,
     *         @OA\Schema(type="string", default="2015-6-1")
     *     ),
     *     @OA\Parameter(
     *         name="microAmount",
     *         in="query",
     *         description="Micro amount for the campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="15500000")
     *     ),
     *    @OA\Parameter(
     *         name="budgetName",
     *         in="query",
     *         description="Budget name that the user is going to set for the campaign.",
     *         required=true,
     *         @OA\Schema(type="string", enum={"ENABLED", "REMOVED", "PAUSED"}, default="ENABLED")
     *     ),
     *    @OA\Parameter(
     *         name="deliveryMethod",
     *         in="query",
     *         description="Delivery method that the user is going to set for the campaign. For example STANDARD, ACCELERATED",
     *         required=true,
     *         @OA\Schema(type="string", enum={"STANDARD", "ACCELERATED", "UNKNOWN"}, default="STANDARD")
     *     ),
     *    @OA\Parameter(
     *         name="isExplicitlyShared",
     *         in="query",
     *         description="",
     *         required=true,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *      @OA\Parameter(
     *         name="description1",
     *         in="query",
     *         description="description1",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description2",
     *         in="query",
     *         description="description2",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description3",
     *         in="query",
     *         description="description3",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description4",
     *         in="query",
     *         description="description4",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="appId",
     *         in="query",
     *         description="appId",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="appMarket",
     *         in="query",
     *         description="appMarket",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="appBiddingStrategyGoalType",
     *         in="query",
     *         description="appBiddingStrategyGoalType",
     *         required=true,
     *         @OA\Schema(type="string",default="OPTIMIZE_FOR_INSTALL_CONVERSION_VOLUME", enum={"OPTIMIZE_FOR_RETURN_ON_ADVERTISING_SPEND", "OPTIMIZE_FOR_TARGET_IN_APP_CONVERSION", "OPTIMIZE_FOR_TOTAL_CONVERSION_VALUE", "OPTIMIZE_FOR_IN_APP_CONVERSION_VOLUME", "OPTIMIZE_FOR_INSTALL_CONVERSION_VOLUME"})
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="Operator: SET, ADD",
     *         required=false,
     *         @OA\Schema(type="string", default="ADD", enum={"SET", "ADD"})
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="Send campaign id for update specified unisersal app Campaign",
     *         required=false,
     *         @OA\Schema(
     *          type="string",default="")
     *     ),
     *     @OA\Response(
     *          @OA\MediaType(
     *          mediaType="application/json"),
     *          response="200",description="and structure of the response is : id, name"
     *     ),
     * )
     *
     * Update or add new universal app campaign.
     *
     * @param SetUniversalCampaignAppRequest $request
     *
     * @return array
     */
    public function setUniversalAppCampaign(SetUniversalCampaignAppRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setGeoLocation",
     *     operationId="/setGeoLocation",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="data",
     *         in="query",
     *         description="data used for for setGeoLocation",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the 200 and some data regarding the operation.",
     *     ),
     * )
     *
     * @param SetGeoLocationRequest $request
     *
     * @return mixed
     */
    public function setGeoLocation(SetGeoLocationRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setGeoLocationDistance",
     *     operationId="/setGeoLocationDistance",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="data",
     *         in="query",
     *         description="data used for for setGeoLocationDistance",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the 200 and some data regarding the operation.",
     *     ),
     * )
     *
     * Add site link extension to specified adGroup.
     *
     * @param SetGeoDistanceRequest $request
     *
     * @return mixed
     */
    public function setGeoLocationDistance(SetGeoDistanceRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignNetworkSetting",
     *     operationId="/setCampaignNetworkSetting",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="The id of campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="contentNetwork",
     *         in="query",
     *         description="The target content network status of campaign",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *     @OA\Parameter(
     *         name="partnerSearchNetwork",
     *         in="query",
     *         description="The target partner search network status of campaign",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *     @OA\Parameter(
     *         name="searchNetwork",
     *         in="query",
     *         description="The target search network status of campaign",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *     @OA\Parameter(
     *         name="googleSearch",
     *         in="query",
     *         description="The target google searck status of campaign",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign",
     *     ),
     * )
     *
     * Set campaign network settings.
     *
     * @param SetCampaignNetworkSetting $request
     *
     * @return mixed
     */
    public function setNetworkSettings(SetCampaignNetworkSetting $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignTrackingUrlTemplate",
     *     operationId="/setCampaignTrackingUrlTemplate",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="The id of campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="trackingUrlTemplate",
     *         in="query",
     *         description="The tracking url template",
     *         required=false,
     *         @OA\Schema(type="string", default="https://your-site.com/tracking-url-template{lpurl}?utm_campaign={campaignid}&adgroupid={adgroupid}&utm_content={creative}&utm_term={keyword}")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign",
     *     ),
     * )
     *
     * Set campaign tracking url template.
     *
     * @param SetCampaignTrackingUrlTemplate $request
     *
     * @return mixed
     */
    public function setTrackingUrlTemplate(SetCampaignTrackingUrlTemplate $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignUrlCustomParameters",
     *     operationId="/setCampaignUrlCustomParameters",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="The id of campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="urlCustomParameters",
     *         in="query",
     *         description="The url custom parameters of array of objects. Keys: key, value, isRemoved.",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign",
     *     ),
     * )
     *
     * Set campaign url custom parameters.
     *
     * @param SetCampaignUrlCustomParametersRequest $request
     *
     * @return mixed
     *
     */
    public function setUrlCustomParameters(SetCampaignUrlCustomParametersRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignDynamicSearchAdsSetting",
     *     operationId="/setCampaignDynamicSearchAdsSetting",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="The id of campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="domainName",
     *         in="query",
     *         description="The domain name.",
     *         required=true,
     *         @OA\Schema(type="string", default="google.com")
     *     ),
     *     @OA\Parameter(
     *         name="languageCode",
     *         in="query",
     *         description="The language code. For example: fa",
     *         required=true,
     *         @OA\Schema(type="string", default="fa")
     *     ),
     *     @OA\Parameter(
     *         name="pageFeed",
     *         in="query",
     *         description="The page feed numeber",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="suppliedUrlsOnly",
     *         in="query",
     *         description="The status of supplied urls only. For example: false",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign",
     *     ),
     * )
     *
     * Set dynamic search ads setting.
     *
     * @param SetDynamicSearchAdsSettingRequest $request
     *
     * @return array|mixed
     */
    public function setDynamicSearchAdsSetting(SetDynamicSearchAdsSettingRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignFinalUrlSuffix",
     *     operationId="/setCampaignFinalUrlSuffix",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="The id of campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="2045454855")
     *     ),
     *     @OA\Parameter(
     *         name="finalUrlSuffix",
     *         in="query",
     *         description="The final url suffix",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id, name and status of corresponding campaign",
     *     ),
     * )
     *
     * Set final url suffix.
     *
     * @param SetCampaignFinalUrlSuffixRequest $request
     *
     * @return array|mixed
     */
    public function setFinalUrlSuffix(SetCampaignFinalUrlSuffixRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignBiddingOption",
     *     operationId="/setCampaignBiddingOption",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="type",
     *         in="query",
     *         description="data for the setting containing the campaing id . ",
     *         required=true,
     *         @OA\Schema(type="string", default="",enum={"MANUAL_CPC","ENHANCED_MANUAL_CPC","TARGET_CPA","TARGET_SPEND","MAXIMIZE_CONVERSIONS","TARGET_ROAS"})
     *     ),
     *     @OA\Parameter(
     *         name="data",
     *         in="query",
     *         description="MANUAL_CPC=operator(nullable),campaignId,null|ENHANCED_MANUAL_CPC=operator(nullable),campaignId,enableEnhancedCpc|TARGET_CPA=operator(nullable),campaignId,maxCpcBidCeiling,maxCpcBidFloor,targetCpa,strategyId(for portfolio)|
     *         TARGET_SPEND=operator(nullable),campaignId,bidCeiling,spendTarget|MAXIMIZE_CONVERSIONS=operator(nullable),campaignId|TARGET_ROAS=strategyId(nullable),bidFloor,bidCeiling,targetRoas(a float number between 0 and 10)",
     *         required=true,
     * @OA\Schema(type="string", default="")
     *     ),
     * @OA\Response(
     *         @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign setting",
     *     ),
     * )
     *
     * Set campaign tracking url template.
     *
     * @param SetCampaignBiddingOption $request
     *
     * @return mixed
     */
    public function setCampaignBiddingOption(SetCampaignBiddingOption $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignBudget",
     *     operationId="/setCampaignBudget",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="id of the corresponding campaign.",
     *         required=true,
     *       @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="microAmount",
     *         in="query",
     *         description="",
     *         required=true,
     *      @OA\Schema(type="string", default="4500000")
     *     ),
     *     @OA\Parameter(
     *         name="deliveryMethod",
     *         in="query",
     *         description="deliveryMethod",
     *         required=true,
     *      @OA\Schema(type="string", default="STANDARD")
     *     ),
     *     @OA\Parameter(
     *         name="isExplicitlyShared",
     *         in="query",
     *         description="isExplicitlyShared",
     *         required=true,
     *      @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="budgetId",
     *         in="query",
     *         description="For update specified budget send budgetId.",
     *         required=false,
     *      @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="Set budget for specified camapign.",
     *         required=false,
     *      @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of corresponding campaign setting",
     *     ),
     * )
     *
     * Update or add new budget.
     *
     * @param SetBudgetRequest $request
     *
     * @return mixed
     */
    public function setBudget(SetBudgetRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setCampaignExtensions",
     *     operationId="/setCampaignExtensions",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign.",
     *         required=true,
     *       @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="extensions",
     *         in="query",
     *         description="A list of campaign extensions, Like sitelink, app and callout extensions.",
     *         required=true,
     *      @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Response(
     *         @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and type of corresponding campaign",
     *     ),
     * )
     *
     * Set campaign extensions.
     *
     * @param SetCampaignExtensionsRequest $request
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setExtensions(SetCampaignExtensionsRequest $request);
}
