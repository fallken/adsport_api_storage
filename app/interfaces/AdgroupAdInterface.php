<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 4/18/2019
 * Time: 10:06 AM
 */

namespace App\interfaces;

interface AdgroupAdInterface extends ResponseType
{
    /**
     * Add call only ad.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $countryCode
     * @param string $phone
     * @param string $businessName
     * @param string $desc1
     * @param string $desc2
     * @param mixed $callTracked
     * @param mixed $disableCallConversion
     * @param string $conversionTypeId
     * @param string $phoneNumberVerificationUrl
     *
     * @return mixed
     */
    public function addCallOnlyAd(string $customerClientId, string $adGroupId, string $countryCode, string $phone, string $businessName, string $desc1, string $desc2, $callTracked, $disableCallConversion, string $conversionTypeId, string $phoneNumberVerificationUrl);

    /**
     * Set ads dynamic search setting.
     *
     * @param string $customerId
     * @param string $adGroupId
     * @param string $adId
     * @param string $description
     * @param string $description2
     *
     * @return array|mixed
     */
    public function setExpandedDynamicSearchAd(string $customerId, string $adGroupId, string $adId, string $description, string $description2);

    /**
     * Update or add new Ad for the specific ad group.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $adType
     * @param string $finalUrl
     * @param string $path1
     * @param string $path2
     * @param object $headlines
     * @param object $descriptions
     * @param string $status
     * @param string $operator
     * @param string $adId
     *
     * @return mixed
     */
    public function setAd(string $customerClientId, string $adGroupId, string $adType, string $finalUrl, string $path1, string $path2, $headlines, $descriptions, string $operator, string $status, string $adId = '');

    /**
     * Get ad groups ads
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $responseType
     *
     * @return mixed
     */
    public function getAdGroupAdsV1(string $customerClientId, string $adGroupId, string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Will return ad group ads for the specific ad group using its id.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     */
    public function getAds(string $customerClientId, string $adGroupId, array $predicates, string $responseType = self::RESPONSE_TYPE_JSON);
}