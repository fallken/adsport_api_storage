<?php

namespace App\Wrappers;

class BudgetWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $amount = $data->getAmount();

        return (object)[
            'status' => $data->getStatus(),
            'budgetName' => $data->getName(),
            'budgetId' => $data->getBudgetId(),
            'deliveryMethod' => $data->getDeliveryMethod(),
            'referenceCount' => $data->getReferenceCount(),
            'isExplicitlyShared' => $data->getIsExplicitlyShared(),
            'amount' => (object)WrapperBridge::toObject($amount)
        ];
    }
}
