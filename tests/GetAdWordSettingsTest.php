<?php

use App\repos\AdWordsSettingsRepository;

class GetAdWordSettingsTest extends TestCase
{
    /**
     * @var AdWordsSettingsRepository
     */
    private $settingsRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->settingsRepository = resolve(AdWordsSettingsRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetCampaignExtensionsWithOutPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'campaign';
        $settingType = 'extensions';

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle([]);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetCampaignCriterionWithOutPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'campaign';
        $settingType = 'criterion';

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle([]);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAdGroupExtensionsWithOutPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'adGroup';
        $settingType = 'extensions';

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle([]);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAdGroupCriterionWithOutPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'adGroup';
        $settingType = 'criterion';

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle([]);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetCampaignExtensionsWithPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'campaign';
        $settingType = 'extensions';

        $predicates = [
            [
                'field' => 'CampaignId',
                'operator' => 'EQUALS',
                'values' => ['1703779813']
            ]
        ];

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle($predicates);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetCampaignCriterionWithPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'campaign';
        $settingType = 'criterion';

        $predicates = [
            [
                'field' => 'CampaignId',
                'operator' => 'EQUALS',
                'values' => ['1703779813']
            ]
        ];

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle($predicates);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAdGroupExtensionsWithPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'adGroup';
        $settingType = 'extensions';

        $predicates = [
            [
                'field' => 'AdGroupId',
                'operator' => 'EQUALS',
                'values' => ['64912739685']
            ]
        ];

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle($predicates);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAdGroupCriterionWithPredicates()
    {
        $customerClientId = '261-911-0558';
        $serviceType = 'adGroup';
        $settingType = 'criterion';

        $predicates = [
            [
                'field' => 'AdGroupId',
                'operator' => 'EQUALS',
                'values' => ['64912739685']
            ]
        ];

        $this->settingsRepository->initialize($customerClientId, $serviceType, $settingType);

        $results = $this->settingsRepository->handle($predicates);

        $this->assertNotNull($results);

        $response = $this->post('/get/settings', [
            'customerClientId' => $customerClientId,
            'serviceType' => $serviceType,
            'settingType' => $settingType,
        ]);

        $response->assertResponseStatus(200);
    }
}
