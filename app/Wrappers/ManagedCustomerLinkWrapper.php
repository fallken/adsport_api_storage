<?php

namespace App\Wrappers;

class ManagedCustomerLinkWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data)
    {
        return (object)[
            'isHidden' => $data->getIsHidden(),
            'linkStatus' => $data->getLinkStatus(),
            'clientCustomerId' => $data->getClientCustomerId(),
            'managerCustomerId' => $data->getManagerCustomerId(),
            'pendingDescriptiveName' => $data->getPendingDescriptiveName(),
        ];
    }
}
