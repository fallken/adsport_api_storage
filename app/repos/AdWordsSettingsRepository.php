<?php

namespace App\repos;

use App\Helpers\RetryCallTrait;
use App\Wrappers\WrapperBridge;
use App\Helpers\AdWordsSettingsAbstract;

use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\Selector;

class AdWordsSettingsRepository extends AdWordsSettingsAbstract
{
	use RetryCallTrait;

	/**
	 * Handle incoming get requests.
	 *
	 * @param array $predicates
	 * @param string $responseType
	 *
	 * @return array|mixed
	 *
	 * @throws \App\Exceptions\EndPointRequestException
	 * @throws \App\Exceptions\ThreadShutdownException
	 */
	public function handle(array $predicates = [], string $responseType = self::RESPONSE_TYPE_JSON)
	{
		$selector = new Selector();

		$selector->setFields($this->getDefaultFields());

		$selectorPredicates = [];

		/**
		 * Merge user predicates and default predicates.
		 */
		$predicates = array_merge($predicates, $this->generateDefaultPredicates());

		foreach ($predicates as $predicate)
		{
			$predicate = (object)$predicate;

			$selectorPredicates[] = new Predicate($predicate->field, $predicate->operator, $predicate->values);
		}

		$selector->setPredicates($selectorPredicates);

		$selector->setPaging(new Paging(0, static::PAGE_LIMIT));

		$entries = $this->call(function () use ($selector) {
			return $this->service->get($selector)->getEntries();
		}, 1);

		if ($responseType == static::RESPONSE_TYPE_PURE)
		{
			return $entries;
		}

		$results = [];

		foreach ($entries as $entry)
		{
			$results[] = WrapperBridge::toObject($entry);
		}

		return $results;
	}
}
