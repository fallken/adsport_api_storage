<?php

use App\repos\KeywordsRepository;

class GetKeyWordsTest extends TestCase
{
    protected $keywordRepo;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->keywordRepo = new KeywordsRepository();
    }

    public function testGetKeyWords()
    {
        $result = $this->keywordRepo->getKeywords('1042756431', '76914856015');

        print_r($result);

        $this->assertNotNull($result);

        $this->assertStringContainsString('PHRASE', $result);
    }
}
