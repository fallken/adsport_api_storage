<?php

namespace App\Http\Interfaces;

use App\Http\requests\Copier\HardCopierRequest;
use App\Http\requests\Copier\ShowCopierQueueRequest;
use App\Http\requests\Copier\StoreCopierQueueRequest;

use Illuminate\Http\Request;

interface AccountCopierControllerInterface
{
    /**
     * @OA\Post(
     *     path="/set/transportAccount",
     *     operationId="/transportAccount",
     *     tags={"Copier operation"},
     *     @OA\Parameter(
     *         name="sourceCustomerId",
     *         in="query",
     *         description="source id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="destinationCustomerId",
     *         in="query",
     *         description="destination id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="Response: adId, adGroupId, status",
     *     ),
     * )
     *
     * Hot copy of copier.
     *
     * @param HardCopierRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function hardStore(HardCopierRequest $request);

    /**
     * @OA\Post(
     *     path="/copiers",
     *     operationId="/copiers",
     *     tags={"Copier operation"},
     *     @OA\Parameter(
     *         name="sourceId",
     *         in="query",
     *         description="source id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="destinationId",
     *         in="query",
     *         description="destination id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="callbackUrl",
     *         in="query",
     *         description="Call back url",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="requestId",
     *         in="query",
     *         description="Request id use for resume an copier operation.",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *    @OA\Response(
     *         @OA\MediaType(mediaType="application/json"), response="200", description="Respose is: status, requestId"),
     *    )
     *
     * Set keyword setting.
     *
     * @param StoreCopierQueueRequest $request
     *
     * @return mixed
     */
    public function store(StoreCopierQueueRequest $request);

    /**
     * @OA\Get(
     *     path="/copiers/show",
     *     operationId="/copiers/show",
     *     tags={"Copier operation"},
     *     @OA\Parameter(
     *         name="requestId",
     *         in="query",
     *         description="id of the corresponding request",
     *         required=true,
     *         @OA\Schema(type="string", default="10")
     *     ),
     *    @OA\Response(
     *         @OA\MediaType(mediaType="application/json"), response="200", description="Respose is: status, requestId"),
     *    )
     *
     * Set keyword setting.
     *
     * @param ShowCopierQueueRequest $request
     *
     * @return mixed
     */
    public function show(ShowCopierQueueRequest $request);

    /**
     * @OA\Get(
     *     path="/copiers",
     *     operationId="/copiers",
     *     tags={"Copier operation"},
     *     @OA\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Number of rows",
     *         required=true,
     *         @OA\Schema(type="string", default="25")
     *     ),
     *    @OA\Response(
     *         @OA\MediaType(mediaType="application/json"), response="200", description="Respose is: id, source_id, destination_id, status, ..."),
     *    )
     *
     * Set keyword setting.
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function index(Request $request);
}
