<?php

namespace App\Wrappers;

class FeedItemAdGroupTargetingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'targetingAdGroupId' => $data->getTargetingAdGroupId(),
        ];
    }
}
