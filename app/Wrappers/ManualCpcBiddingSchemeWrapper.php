<?php

namespace App\Wrappers;

class ManualCpcBiddingSchemeWrapper implements AdWordsWrapperInterface
{
    use BiddingSchemeTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data)
    {
        return (object)array_merge(static::append($data),
            [
                'enhancedCpcEnabled' => $data->getEnhancedCpcEnabled(),
            ]
        );
    }
}
