<?php

namespace App\Wrappers;

class UniversalAppCampaignSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $adsPolicyDecisions = null;

        $decisions = $data->getAdsPolicyDecisions();

        if (is_array($decisions)) {

            $adsPolicyDecisions = [];

            foreach ($decisions as $decision) {

                $adsPolicyDecisions[] = WrapperBridge::toObject($decision);
            }
        }

        $youtubeVideoMediaIdsOps = $data->getYoutubeVideoMediaIdsOps();
        $imageMediaIdsOps = $data->getImageMediaIdsOps();

        return (object)[
            'appId' => $data->getAppId(),
            'appVendor' => $data->getAppVendor(),
            'description1' => $data->getDescription1(),
            'description2' => $data->getDescription2(),
            'description3' => $data->getDescription3(),
            'description4' => $data->getDescription4(),
            'imageMediaIds' => $data->getImageMediaIds(),
            'youtubeVideoMediaIds' => $data->getYoutubeVideoMediaIds(),
            'universalAppBiddingStrategyGoalType' => $data->getUniversalAppBiddingStrategyGoalType(),

            'adsPolicyDecisions' => $adsPolicyDecisions,
            'imageMediaIdsOps' => WrapperBridge::toObject($imageMediaIdsOps),
            'youtubeVideoMediaIdsOps' => WrapperBridge::toObject($youtubeVideoMediaIdsOps),
        ];
    }
}
