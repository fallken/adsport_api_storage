<?php

namespace App\interfaces;

use App\Exceptions\EndPointRequestException;

interface ConversionTrackerInterface extends ResponseType
{
    /**
     * Add or update specified conversion tracker.
     *
     * @param string $customerClientId
     * @param string $trackerType
     * @param string $operator
     * @param string $name
     * @param string $category
     * @param string $currencyCode
     * @param string $status
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function setConversionTracker(string $customerClientId, string $trackerType, string $operator, string $name, string $category, string $currencyCode, string $status);

    /**
     * Get conversion trackers.
     *
     * @param string $customerClientId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function getConversionTrackers(string $customerClientId, array $predicates, string $responseType = self::RESPONSE_TYPE_JSON);
}
