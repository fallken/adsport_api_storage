<?php

namespace App\Wrappers;

class CpcBidWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $money = $data->getBid();

        return (object)[
            'bid' => WrapperBridge::toObject($money),
            'cpcBidSource' => $data->getCpcBidSource(),
            'bidsType' => $data->getBidsType(),
        ];
    }
}
