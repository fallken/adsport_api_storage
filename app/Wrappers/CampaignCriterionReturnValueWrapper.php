<?php

namespace App\Wrappers;

class CampaignCriterionReturnValueWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $values = null;

        $response = $data->getValue();

        if (is_array($response) && count($response)) {

            $values = [];

            foreach ($response as $value) {

                $values[] = WrapperBridge::toObject($value);
            }
        }

        return (object)[
            'values' => $values,
        ];
    }
}
