<?php

namespace App\Wrappers;

class FeedItemScheduleWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'dayOfWeek' => $data->getDayOfWeek(),
            'startHour' => $data->getStartHour(),
            'startMinute' => $data->getStartMinute(),
            'endHour' => $data->getEndHour(),
            'endMinute' => $data->getEndMinute(),
        ];
    }
}
