<?php

namespace App\Wrappers;

trait BiddingSchemeTrait
{
    /**
     * @param object $data
     *
     * @return array
     *
     */
    public static function append(&$data)
    {
        return [
            'biddingSchemeType' => $data->getBiddingSchemeType(),
        ];
    }
}