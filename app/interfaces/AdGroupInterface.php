<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 11:52 AM
 */

namespace App\interfaces;

use Illuminate\Http\Request;

interface AdGroupInterface extends ResponseType
{
    /**
     * Set tracking url template.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $trackingUrlTemplate
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setTrackingUrlTemplate(string $customerClientId, string $adGroupId, string $trackingUrlTemplate);

    /**
     * Set url custom parameters.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $urlCustomParameters
     * @param null $doReplace
     * @param string $responseType
     *
     * @return mixed
     *
     */
    public function setUrlCustomerParameters(string $customerClientId, string $adGroupId, array $urlCustomParameters, $doReplace = null, string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add ad group callout extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $groupId
     * @param array $callouts
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     */
    public function addCalloutExtension(string $customerClientId, string $groupId, array $callouts, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add app extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $groupId
     * @param array $apps
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     */
    public function addAppExtension(string $customerClientId, string $groupId, array $apps, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add site link extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $groupId
     * @param array $links
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     */
    public function addSiteLinkExtension(string $customerClientId, string $groupId, array $links, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeAdgroupStatus(Request $request);


    /**
     * @param Request $request
     * @return mixed
     */
    public function changeAdgroupName(Request $request);


    /**
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $name
     * @param string $microAmount
     * @param string $status
     * @param string $criterionType
     * @param string $adRotationMode
     * @param string|null $trackingUrlTemplate
     * @param array $urlCustomParameters
     * @param bool $doReplace
     * @param string $operator
     * @param string $adGroupId
     *
     * @return array
     */
    public function setAdGroup(string $customerClientId, string $campaignId, string $name, string $microAmount, string $status, string $criterionType, string $adRotationMode, $trackingUrlTemplate, array $urlCustomParameters, bool $doReplace, string $operator, string $adGroupId = '');

    /**
     * @param $params
     * @return mixed
     * desc:will add adgroup call extension to the corresponding adgroup
     */
    public function addAdgroupCallExtension($params);

    /**
     * Get ad groups by campaign id.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     */
    public function getAdGroups(string $customerClientId, string $campaignId, array $predicates = [], string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $adGroupId
     * @param string $operator
     * @param string $platformId
     * @param float $bidModifier
     *
     * @return mixed
     */
    public function addPlatformBidModifier(string $customerClientId, string $campaignId, string $adGroupId, string $operator, string $platformId, float $bidModifier);

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     */
    public function setExpandedResponsiveAd($customerId, $data);
}
