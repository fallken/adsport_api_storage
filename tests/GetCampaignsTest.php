<?php

use App\repos\CampaignsRepository;

class GetCampaignsTest extends TestCase
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = resolve(CampaignsRepository::class);
    }

    public function testCampaigns()
    {
        $result = $this->campaignRepository->getCampaigns('541-403-9496');

        $this->assertNotNull($result);

        $this->assertStringContainsString('Id', $result);
    }
}
