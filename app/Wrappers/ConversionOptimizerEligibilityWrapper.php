<?php

namespace App\Wrappers;

class ConversionOptimizerEligibilityWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     */
    public static function toObject(&$data)
    {
        return (object)[
            'eligible' => $data->getEligible(),
            'rejectionReasons' => $data->getRejectionReasons(),
        ];
    }
}
