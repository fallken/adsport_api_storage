<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests;


use Anik\Form\FormRequest;

class ChangeAdgroupNameRequest extends FormRequest
{

    protected function rules()
    {
        return [
            'customerId'=>'required|string',
            'adgroupId'=>'required|string',
            'name'=>'required|string'
        ];
    }

    protected function messages () {
        return [];
    }
}
