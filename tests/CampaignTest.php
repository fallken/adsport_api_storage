<?php

use App\repos\CampaignsRepository;

use Illuminate\Support\Str;

class CampaignTest extends TestCase
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = new CampaignsRepository();
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testTrackingUrlTemplate()
    {
        $customerId = '541-403-9496';
        $campaignId = '2045454855';
        $trackingUrlTemplate = 'https://your-site.com/tracking-url-template-' . Str::random() . '{lpurl}?utm_campaign={campaignid}&adgroupid={adgroupid}&utm_content={creative}&utm_term={keyword}';

        $result = $this->campaignRepository->setTrackingUrlTemplate($customerId, $campaignId, $trackingUrlTemplate);

        print_r($result);

        $this->assertIsArray($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testUrlCustomParameters()
    {
        $customerId = '541-403-9496';
        $campaignId = '2045454855';

        $urlCustomParameters = [
            (object)[
                'key' => 'campaignid',
                'value' => '43535334543',
                'isRemoved' => false
            ],
            (object)[
                'key' => 'lpurl',
                'value' => 'dsfsdfsdfdsfdsf',
                'isRemoved' => false
            ],
            (object)[
                'key' => 'danger',
                'value' => '342423424234',
                'isRemoved' => false
            ]
        ];

        $result = $this->campaignRepository->setUrlCustomerParameters($customerId, $campaignId, $urlCustomParameters);

        print_r($result);

        $this->assertIsArray($result);

        $response = $this->post('set/setCampaignUrlCustomParameters', [
            'customerClientId' => '541-403-9496',
            'campaignId' => '71237816126',
            'urlCustomParameters' => $urlCustomParameters,
            'doReplace' => false,
        ]);

        print_r($response->response->getContent());

        $response->assertResponseOk();

    }
}
