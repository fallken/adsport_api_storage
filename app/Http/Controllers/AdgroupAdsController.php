<?php

namespace App\Http\Controllers;

use App\Http\Requests\getRequests\GetAdsRequest;
use App\repos\AdGroupAdRepository;
use App\interfaces\AdgroupAdInterface;
use App\Http\Interfaces\AdGroupAdControllerInterface;
use App\Http\Requests\addRequests\AddCallOnlyAdRequest;
use App\Http\requests\setRequests\setAdGroupAdExpandedDynamicSearchAdRequest;

class AdgroupAdsController extends Controller implements AdGroupAdControllerInterface
{
    /**
     * @var AdGroupAdRepository
     */
    protected $adGroupAdRepository;

    /**
     * AdgroupAdsController constructor.
     * @param AdgroupAdInterface $adGroupAd
     */
    public function __construct(AdgroupAdInterface $adGroupAd)
    {
        $this->adGroupAdRepository = $adGroupAd;
    }

    /**
     * Get ads.
     *
     * @param GetAdsRequest $request
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function getAds(GetAdsRequest $request)
    {
        return $this->adGroupAdRepository->getAds(
            $request->input('customerId'),
            $request->input('adGroupId'),
            $request->input('predicates')
        );
    }

    public function testify() {
        return 'its working like charm';
    }

    /**
     * @param AddCallOnlyAdRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCallOnly(AddCallOnlyAdRequest $request)
    {
        return $this->adGroupAdRepository->addCallOnlyAd(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('countryCode'),
            $request->input('phone'),
            $request->input('businessName'),
            $request->input('description1'),
            $request->input('description2'),
            $request->input('callTracked'),
            $request->input('disableCallConversion'),
            $request->input('conversionTypeId'),
            $request->input('phoneNumberVerificationUrl')
        );
    }

    /**
     * Set ad expanded dynamic search.
     *
     * @param setAdGroupAdExpandedDynamicSearchAdRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setExpandedDynamicSearchAd(setAdGroupAdExpandedDynamicSearchAdRequest $request)
    {
        return $this->adGroupAdRepository->setExpandedDynamicSearchAd(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('adId'),
            $request->input('description'),
            $request->input('description2')
        );
    }
}
