<?php

namespace App\Wrappers;

class LabelAttributeWrapper implements AdWordsWrapperInterface
{
    use AdGroupCriterionTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
         $attribute = $data->getAttribute();

        return (object)[
            'id' => $data->getId(),
            'nme' => $data->getName(),
            'status' => $data->getStatus(),
            'labelType' => $data->getLabelType(),

            'attribute' => WrapperBridge::toObject($attribute),
        ];
    }
}
