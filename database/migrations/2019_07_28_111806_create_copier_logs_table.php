<?php

use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCopierLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('copier_logs', function (Blueprint $table) {

            $table->engine = 'innodb';

            $table->bigIncrements('id');

            $table->bigInteger('copier_id')->index();

            $table->enum('status', ['success', 'failed']);

            $table->longText('message')->nullable();
            $table->longText('exception')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('copier_logs');
    }
}
