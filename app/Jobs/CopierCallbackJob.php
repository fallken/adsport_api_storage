<?php

namespace App\Jobs;

use App\Models\Copier;

use App\Models\CopierLogs;

use Exception;
use GuzzleHttp\Client;

class CopierCallbackJob extends Job
{
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 2;

    /**
     * @var Copier
     */
    private $copier;

    /**
     * Create a new job instance.
     *
     * @param int $copierId
     */
    public function __construct(int $copierId)
    {
        $this->copier = Copier::find($copierId);
    }

    /**
     * Execute the job.
     *
     * @return void
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle()
    {
        if (!$this->copier)
        {
            return;
        }

        $client = new Client();

        $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_SUCCESS, 'message' => 'Calling...']));

        try
        {
            $client->request('post', $this->copier->getAttribute('callback_url'), [
                'headers' => [
                    'Content-type' => 'application/json',
                    'Accept' => 'application/json'
                ],
                'body' => json_encode(['id' => $this->copier->id, 'results' => $this->copier->results])
            ]);

            $this->copier->logs()->save(new CopierLogs(['status' => CopierLogs::STATUS_SUCCESS, 'message' => 'Callback called successfully.']));

            if ($this->copier->status == Copier::STATUS_SUCCESS)
            {
                $this->copier->update(['status' => Copier::STATUS_DONE]);
            }
        }
        catch (Exception $exception)
        {
            $this->copier->logs()->save(new CopierLogs([
                'status' => CopierLogs::STATUS_FAILED,
                'message' => 'Copier callback job failed.',
                'exception' => ['code' => $exception->getCode(), 'message' => $exception->getMessage()]
            ]));
        }
    }
}
