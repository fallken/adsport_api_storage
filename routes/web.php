<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


//list of google related routes
$router->get('/campaigns', 'AdsController@getCampaignsAction');
$router->get('/keyword', 'AdsController@updateKeyword');

$router->get('/testify', function () {
    $class = new \App\repos\KeywordsRepository();

    return $class->getKeywordRecommendations(['keywords' => ['helicopter'], 'customerId' => '261-911-0558']);
});

$router->group(['prefix' => 'rename'], function () use ($router) {
    $router->post('/changeKeywordName', 'AdsController@changeKeywordName');
    $router->post('/changeAdgroupName', 'AdsController@changeAdgroupName');
    $router->post('/changeCampaignName', 'AdsController@changeCampaignName');
});

$router->group(['prefix' => 'add'], function () use ($router) {
    $router->post('/addKeyword', 'AdsController@addKeyword');
    $router->post('/addAdgroup', 'AdsController@setAdGroup');
    $router->post('/addCampaign', 'CampaignController@addCampaign');
    $router->post('/addCampaignSetting', 'AdsController@addCampaignSetting');
    $router->post('/addCustomer', 'AccountController@createCustomerAccount');
    $router->post('/addAdSchedule', 'AdsController@addAdSchedule');
    $router->post('/addAdgroupCallExtension', 'ExtensionController@addAdgroupCallExtension');
    $router->post('/sendLink', 'AccountController@sendJoinLinkToCutomerAccount');

    $router->post('/addSiteLinkExtension', 'CampaignController@addSiteLinkExtension');
    $router->post('/addCalloutExtension', 'CampaignController@addCalloutExtension');
    $router->post('/addAppExtension', 'CampaignController@addAppExtension');

    $router->post('/addAdGroupCalloutExtension', 'AdGroupController@addCalloutExtension');
    $router->post('/addAdGroupAppExtension', 'AdGroupController@addAppExtension');
    $router->post('/addAdGroupSiteLinkExtension', 'AdGroupController@addSiteLinkExtension');
    $router->post('/addAdGroupCallOnlyAd', 'AdgroupAdsController@addCallOnly');

    $router->post('/addAdGroupPlatformBidModifier', 'BidModifierController@addAdGroupPlatformBidModifier');
    $router->post('/addCampaignPlatformBidModifier', 'BidModifierController@addCampaignPlatformBidModifier');
    $router->post('/addCampaignLocationBidModifier', 'BidModifierController@addCampaignLocationBidModifier');
});

$router->group(['prefix' => 'status'], function () use ($router) {
    //campaign operations
    $router->post('/changeCampaignStatus', 'AdsController@changeCampaignStatus');

    //adgroup operations
    $router->post('/changeAdgroupStatus', 'AdsController@changeAdgroupStatus');

    //keyword operations
    $router->post('/changeKeywordStatus', 'AdsController@changeKeywordStatus');
});

$router->group(['prefix' => 'set'], function () use ($router) {
    $router->post('/setGeoLocation', 'CampaignController@setGeoLocation');
    $router->post('/setGeoLocationDistance', 'CampaignController@setGeoLocationDistance');

    $router->post('/setCampaignNetworkSetting', 'CampaignController@setNetworkSettings');
    $router->post('/setCampaignBiddingOption', 'CampaignController@setCampaignBiddingOption');
    $router->post('/setConversionTracking', 'ConversionTrackerController@setConversionTracking');
    $router->post('/setSharedBiddingStrategy', 'AccountController@setSharedBiddingStrategy');

    $router->post('/setCampaignTrackingUrlTemplate', 'CampaignController@setTrackingUrlTemplate');
    $router->post('/setAdGroupTrackingUrlTemplate', 'AdGroupController@setTrackingUrlTemplate');

    $router->post('/setAdGroupUrlCustomParameters', 'AdGroupController@setUrlCustomParameters');
    $router->post('/setCampaignUrlCustomParameters', 'CampaignController@setUrlCustomParameters');
    $router->post('/setCampaignDynamicSearchAdsSetting', 'CampaignController@setDynamicSearchAdsSetting');
    $router->post('/setAdGroupAdExpandedDynamicSearchAd', 'AdgroupAdsController@setExpandedDynamicSearchAd');
    $router->post('/setCampaignFinalUrlSuffix', 'CampaignController@setFinalUrlSuffix');

    $router->post('/setKeywordsSetting', 'KeywordsController@setKeywordsSetting');

    $router->post('/setCampaignBudget', 'CampaignController@setBudget');
    $router->post('/setCampaignExtensions', 'CampaignController@setExtensions');
    $router->post('/setAdGroupExtensions', 'AdGroupController@setExtensions');

    $router->post('/setUniversalAppCampaign', 'CampaignController@setUniversalAppCampaign');

    $router->post('/setAd', 'AdsController@setAd');
});

$router->group(['prefix' => 'get'], function () use ($router) {
    $router->post('/getKeywordRecommendations', 'AdsController@getKeywordRecommendations');
    $router->get('/getCurrencyPairs', 'CurrencyController@getCurrencyPairs');
    $router->post('/getKeywords', 'AdsController@getKeywords');

    $router->post('/getAdGroups', 'AdGroupController@getAdGroups');

    $router->post('/getCampaigns', 'CampaignController@getCampaigns');

    $router->post('/getSettings', 'AdWordsSettingsController@getSettings');
    $router->post('/getAdgroupAds', 'AdgroupAdsController@getAds');
});

$router->group(['prefix' => 'copiers', 'middleware' => 'copier'], function () use ($router) {

    $router->get('/', 'AccountCopierController@index');
    $router->get('/show', 'AccountCopierController@show');
    $router->post('/', 'AccountCopierController@store');

    $router->post('/hard', 'AccountCopierController@hardStore');
});