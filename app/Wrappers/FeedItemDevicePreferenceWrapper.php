<?php

namespace App\Wrappers;

class FeedItemDevicePreferenceWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'devicePreference' => $data->getDevicePreference(),
        ];
    }
}
