<?php

namespace App\Wrappers;

class CallOnlyAdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        return (object)array_merge((array)AdWrapper::toObject($data),
            [
                'countryCode' => $data->getCountryCode(),
                'phoneNumber' => $data->getPhoneNumber(),
                'businessName' => $data->getBusinessName(),
                'description1' => $data->getDescription1(),
                'description2' => $data->getDescription2(),
                'callTracked' => $data->getCallTracked(),
                'disableCallConversion' => $data->getDisableCallConversion(),
                'conversionTypeId' => $data->getConversionTypeId(),
                'phoneNumberVerificationUrl' => $data->getPhoneNumberVerificationUrl(),
            ]
        );
    }
}
