<?php

namespace App\Http\Controllers;

use App\repos\AdGroupRepository;
use App\interfaces\AdGroupInterface;
use App\Http\Interfaces\AdGroupControllerInterface;
use App\Http\Requests\getRequests\GetAdGroupsRequest;
use App\Http\Requests\addRequests\SetAdGroupExtensionsRequest;
use App\Http\Requests\addRequests\AddAdGroupAppExtensionRequest;
use App\Http\Requests\addRequests\AddAdGroupCalloutExtensionRequest;
use App\Http\Requests\addRequests\AddAdGroupSiteLinkExtensionRequest;
use App\Http\requests\setRequests\SetAdGroupUrlCustomParametersRequest;
use App\Http\requests\setRequests\SetAdGroupTrackingUrlTemplateRequest;

class AdGroupController extends Controller implements AdGroupControllerInterface
{
    /**
     * @var AdGroupRepository
     */
    protected $adGroupRepository;

    /**
     * AdGroupController constructor.
     * @param AdGroupInterface $adGroup
     */
    public function __construct(AdgroupInterface $adGroup)
    {
        $this->adGroupRepository = $adGroup;
    }

    /**
     * @OA\Post(
     *     path="/get/getAdGroups",
     *     operationId="/getAdGroups",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="1773041194")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the ad groups for the correspoding campaign using its id",
     *     ),
     * )
     * @param GetAdGroupsRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function getAdGroups(GetAdGroupsRequest $request)
    {
        return $this->adGroupRepository->getAdGroups(
            $request->input('customerClientId'),
            $request->input('campaignId')
        );
    }

    /**
     * Set tracking url template.
     *
     * @param SetAdGroupTrackingUrlTemplateRequest $request
     *
     * @return array|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setTrackingUrlTemplate(SetAdGroupTrackingUrlTemplateRequest $request)
    {
        return $this->adGroupRepository->setTrackingUrlTemplate(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('trackingUrlTemplate')
        );
    }

    /**
     * Set url custom parameters.
     *
     * @param SetAdGroupUrlCustomParametersRequest $request
     *
     * @return array|void
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setUrlCustomParameters(SetAdGroupUrlCustomParametersRequest $request)
    {
        return $this->adGroupRepository->setUrlCustomerParameters(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('urlCustomParameters')
        );
    }

    /**
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupCalloutExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addCalloutExtension(AddAdGroupCalloutExtensionRequest $request)
    {
        return $this->adGroupRepository->addCalloutExtension(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('callouts'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupSiteLinkExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addSiteLinkExtension(AddAdGroupSiteLinkExtensionRequest $request)
    {
        return $this->adGroupRepository->addSiteLinkExtension(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('links'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * Add site link extension to specified adGroup.
     *
     * @param AddAdGroupAppExtensionRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addAppExtension(AddAdGroupAppExtensionRequest $request)
    {
        return $this->adGroupRepository->addAppExtension(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('apps'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * Set ad group extensions.
     *
     * @param SetAdGroupExtensionsRequest $request
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setExtensions(SetAdGroupExtensionsRequest $request)
    {
        return $this->adGroupRepository->setExtensions(
            $request->input('customerClientId'),
            $request->input('adGroupId'),
            $request->input('extensions')
        );
    }
}
