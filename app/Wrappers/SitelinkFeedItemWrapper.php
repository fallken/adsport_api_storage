<?php

namespace App\Wrappers;

class SitelinkFeedItemWrapper implements AdWordsWrapperInterface
{
    use ExtensionFeedItemTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $sitelinkFinalUrls = $data->getSitelinkFinalUrls();
        $sitelinkFinalMobileUrls = $data->getSitelinkFinalMobileUrls();
        $sitelinkUrlCustomParameters = $data->getSitelinkUrlCustomParameters();

        return (object)array_merge(static::append($data),
            [
                'sitelinkText' => $data->getSitelinkText(),
                'sitelinkUrl' => $data->getSitelinkUrl(),
                'sitelinkLine2' => $data->getSitelinkLine2(),
                'sitelinkLine3' => $data->getSitelinkLine3(),
                'extensionFeedItemType' => $data->getExtensionFeedItemType(),
                'sitelinkFinalUrlSuffix' => $data->getSitelinkFinalUrlSuffix(),
                'sitelinkTrackingUrlTemplate' => $data->getSitelinkTrackingUrlTemplate(),

                'sitelinkFinalUrls' => WrapperBridge::toObject($sitelinkFinalUrls),
                'sitelinkFinalMobileUrls' => WrapperBridge::toObject($sitelinkFinalMobileUrls),
                'sitelinkUrlCustomParameters' => WrapperBridge::toObject($sitelinkUrlCustomParameters),
            ]
        );
    }
}
