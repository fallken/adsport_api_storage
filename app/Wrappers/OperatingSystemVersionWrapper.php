<?php

namespace App\Wrappers;

class OperatingSystemVersionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'id' => $data->getId(),
            'type' => $data->getType(),
            'name' => $data->getName(),
            'operatorType' => $data->getOperatorType(),
            'criterionType' => $data->getCriterionType(),
            'osMajorVersion' => $data->getOsMajorVersion(),
            'osMinorVersion' => $data->getOsMinorVersion(),
        ];
    }
}
