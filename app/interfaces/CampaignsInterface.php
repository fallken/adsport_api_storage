<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 9:13 AM
 */

namespace App\interfaces;

use Illuminate\Http\Request;
use App\Exceptions\EndPointRequestException;

interface CampaignsInterface extends ResponseType
{
    /**
     * Get campaigns of specified customer.
     *
     * @param string $customerClientId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed
     */
    public function getCampaigns(string $customerClientId, array $predicates = [], string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add site link extension to campaigns.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $links
     * @param string $operator
     * @param string $responseType
     *
     * @return array
     */
    public function addSiteLinkExtension(string $customerClientId, string $campaignId, array $links, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add callout extension to specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $callOuts
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     */
    public function addCallOutExtension(string $customerClientId, string $campaignId, array $callOuts, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add app extension to specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $apps
     * @param string $operator
     * @param string $responseType
     *
     * @return array
     */
    public function AddAppExtension(string $customerClientId, string $campaignId, array $apps, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Set campaign extensions.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $extensions
     * @param string $responseType
     *
     * @return array
     *
     * @throws EndPointRequestException
     */
    public function setExtensions(string $customerClientId, string $campaignId, array $extensions, string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $platformId
     * @param float $bidModifier
     *
     * @return mixed
     */
    public function addPlatformBidModifier(string $customerClientId, string $campaignId, string $platformId, float $bidModifier);

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $locationId
     * @param float $bidModifier
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     */
    public function addGeoBidModifier(string $customerClientId, string $campaignId, string $locationId, float $bidModifier);

    /**
     * Set network setting for specified campaign.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param bool|null $contentNetwork
     * @param bool|null $partnerSearchNetwork
     * @param bool|null $searchNetwork
     * @param bool|null $googleSearch
     *
     * @return mixed
     */
    public function setNetworkSetting(string $customerClientId, string $campaignId, $contentNetwork, $searchNetwork, $googleSearch, $partnerSearchNetwork);

    /**
     * Set tracking url template.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $trackingUrlTemplate
     *
     * @return array
     *
     * @throws EndPointRequestException
     */
    public function setTrackingUrlTemplate(string $customerClientId, string $campaignId, string $trackingUrlTemplate);

    /**
     * Set url custom parameters.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $urlCustomParameters
     * @param bool $doReplace
     * @param string $responseType
     *
     * @return mixed
     */
    public function setUrlCustomerParameters(string $customerClientId, string $campaignId, array $urlCustomParameters, $doReplace = false, string $responseType = self::RESPONSE_TYPE_JSON);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeCampaignStatus(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function changeCampaignName(Request $request);

    /**
     * @param $data
     * @return mixed
     */
    public function addCampaign($data);

    /**
     * Set universal app campaign.
     *
     * @param string $customerClientId
     * @param string $name
     * @param string $status
     * @param string $startDay
     * @param string $endDay
     * @param string $microAmount
     * @param string $budgetName
     * @param string $deliveryMethod
     * @param bool $isExplicitlyShared
     * @param string $description1
     * @param string $description2
     * @param string $description3
     * @param string $description4
     * @param string $appId
     * @param string $appMarket
     * @param string $appBiddingStrategyGoalType
     * @param string $operator
     * @param string $campaignId
     *
     * @return mixed
     */
    public function setUniversalAppCampaign(string $customerClientId,
                                            string $name, string $status, string $startDay, string $endDay,
                                            string $microAmount, string $budgetName, string $deliveryMethod, bool $isExplicitlyShared,
                                            string $description1, string $description2, string $description3, string $description4,
                                            string $appId, string $appMarket, string $appBiddingStrategyGoalType, string $operator, string $campaignId = '');

    /**
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $settings
     *
     * @return array
     */
    public function addCampaignSetting(string $customerClientId, string $campaignId, array $settings);

    /**
     * @param $data
     * @return mixed
     * desc: will add timing schedule on the campaign for the user to for example set the days for it to work and function in the project
     */
    public function addCampaignSchedule($data);

    /**
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function setGeoTarget($data, $customerId);

    /**
     * @param $data
     * @param $customerId
     * @return mixed
     */
    public function setGeoDistanceTarget($data, $customerId);

    /**
     * Set ads dynamic search setting.
     *
     * @param string $customerId
     * @param string $campaignId
     * @param string $domainName
     * @param string $languageCode
     * @param string $pageFeed
     * @param string $suppliedUrlsOnly
     * @return array|mixed
     */
    public function setDynamicSearchAdSetting(string $customerId, string $campaignId, string $domainName, string $languageCode = '', string $pageFeed = '', $suppliedUrlsOnly = '');

    /**
     * Set bidding strategy for portfolio.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $finalUrlSuffix
     *
     * @return array
     *
     * @throws EndPointRequestException
     */
    public function setFinalUrlSuffix(string $customerClientId, string $campaignId, string $finalUrlSuffix);

    /**
     * @param string $customerClientId
     * @param string $name
     * @param string $microAmount
     * @param string $deliveryMethod
     * @param bool $isExplicitlyShared
     * @param string $budgetId
     * @param string $campaignId
     */
    public function setBudget(string $customerClientId, string $name, string $microAmount, string $deliveryMethod, bool $isExplicitlyShared, string $budgetId = '', string $campaignId = '');

    /**
     * Set campaign budget.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $budgetId
     *
     * @return array
     *
     * @throws EndPointRequestException
     */
    public function setCampaignBudget(string $customerClientId, string $campaignId, string $budgetId);

    /**
     * Will set the delivery method for the budget . types  : STANDARD, ACCELERATED, UNKNOWN.
     *
     * @param string $customerId
     * @param $data
     */
    public function setBudgetDeliveryMethod(string $customerId, $data);
}
