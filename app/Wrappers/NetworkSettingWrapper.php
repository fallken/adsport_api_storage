<?php

namespace App\Wrappers;

class NetworkSettingWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'targetGoogleSearch' => $data->getTargetGoogleSearch(),
            'targetSearchNetwork' => $data->getTargetSearchNetwork(),
            'targetContentNetwork' => $data->getTargetContentNetwork(),
            'targetPartnerSearchNetwork' => $data->getTargetPartnerSearchNetwork(),
        ];
    }
}
