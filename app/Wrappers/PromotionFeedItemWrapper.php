<?php

namespace App\Wrappers;

class PromotionFeedItemWrapper implements AdWordsWrapperInterface
{
    use ExtensionFeedItemTrait;

    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $moneyAmountOff = $data->getMoneyAmountOff();
        $ordersOverAmount = $data->getOrdersOverAmount();

        $parameters = null;
        $promotionUrlCustomParameters = $data->getPromotionUrlCustomParameters();

        if (is_array($promotionUrlCustomParameters)) {

            foreach ($promotionUrlCustomParameters as $parameter) {

                $parameters[] = WrapperBridge::toObject($parameter);
            }
        }

        $finalUrls = $data->getFinalUrls();
        $finalMobileUrls = $data->getFinalMobileUrls();

        return (object)array_merge(static::append($data),
            [
                'promotionUrlCustomParameters' => $parameters,

                'occasion' => $data->getOccasion(),
                'language' => $data->getLanguage(),
                'percentOff' => $data->getPercentOff(),
                'promotionEnd' => $data->getPromotionEnd(),
                'promotionCode' => $data->getPromotionCode(),
                'promotionStart' => $data->getPromotionStart(),
                'finalUrlSuffix' => $data->getFinalUrlSuffix(),
                'promotionTarget' => $data->getPromotionTarget(),
                'discountModifier' => $data->getDiscountModifier(),
                'trackingUrlTemplate' => $data->getTrackingUrlTemplate(),

                'finalUrls' => WrapperBridge::toObject($finalUrls),
                'moneyAmountOff' => WrapperBridge::toObject($moneyAmountOff),
                'finalMobileUrls' => WrapperBridge::toObject($finalMobileUrls),
                'ordersOverAmount' => WrapperBridge::toObject($ordersOverAmount),
            ]
        );
    }
}
