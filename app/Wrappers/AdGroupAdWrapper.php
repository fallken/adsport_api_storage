<?php

namespace App\Wrappers;

class AdGroupAdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        $labels = null;
        $labelsOriginal = $data->getLabels();

        if (is_array($labelsOriginal)) {

            $labels = [];

            foreach ($labelsOriginal as $item) {

                $labels[] = WrapperBridge::toObject($item);
            }
        }

        $ad = $data->getAd();
        $policySummary = $data->getPolicySummary();
        $adStrengthInfo = $data->getAdStrengthInfo();

        return (object)[
            'status' => $data->getStatus(),
            'adGroupId' => $data->getAdGroupId(),
            'baseAdGroupId' => $data->getBaseAdGroupId(),
            'baseCampaignId' => $data->getBaseCampaignId(),

            'labels' => $labels,

            'ad' => WrapperBridge::toObject($ad),
            'adStrengthInfo' => WrapperBridge::toObject($adStrengthInfo),
            'policySummary' => WrapperBridge::toObject($policySummary),
        ];
    }
}
