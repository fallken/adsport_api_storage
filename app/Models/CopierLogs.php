<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CopierLogs extends Model
{
    const STATUS_FAILED = 'failed';
    const STATUS_SUCCESS = 'success';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'copier_id',
        'status',
        'message',
        'exception'
    ];

    protected $casts = [
        'exception' => 'object',
    ];
}
