<?php

namespace App\Http\requests\setRequests;

use Anik\Form\FormRequest;

class SetCampaignTrackingUrlTemplate extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerClientId' => 'required|string',
            'campaignId' => 'required|string',
            'trackingUrlTemplate' => 'nullable|string',
        ];
    }
}
