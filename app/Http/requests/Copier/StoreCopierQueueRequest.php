<?php

namespace App\Http\requests\Copier;

use Anik\Form\FormRequest;

class StoreCopierQueueRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'sourceId' => 'required|string',
            'callbackUrl' => 'required|url',
            'requestId' => 'nullable|string',
            'destinationId' => 'required|string',
        ];
    }
}
