<?php

namespace App\Http\Controllers;

use App\Http\requests\setRequests\SetUniversalCampaignAppRequest;
use App\interfaces\BiddingInterface;
use App\interfaces\CampaignsInterface;
use App\Http\requests\SetGeoDistanceRequest;
use App\Http\requests\SetGeoLocationRequest;
use App\Http\requests\SetCampaignBiddingOption;
use App\Http\requests\setRequests\SetBudgetRequest;
use App\Http\Interfaces\CampaignControllerInterface;
use App\Http\requests\addRequests\AddCampaignRequest;
use App\Http\Requests\getRequests\GetCampaignsRequest;
use App\Http\Requests\addRequests\AddAppExtensionRequest;
use App\Http\requests\setRequests\SetCampaignNetworkSetting;
use App\Http\Requests\addRequests\AddCalloutExtensionRequest;
use App\Http\Requests\addRequests\AddSiteLinkExtensionRequest;
use App\Http\Requests\addRequests\SetCampaignExtensionsRequest;
use App\Http\requests\setRequests\SetCampaignTrackingUrlTemplate;
use App\Http\requests\setRequests\SetCampaignFinalUrlSuffixRequest;
use App\Http\requests\setRequests\SetDynamicSearchAdsSettingRequest;
use App\Http\requests\setRequests\SetCampaignUrlCustomParametersRequest;

class CampaignController extends Controller implements CampaignControllerInterface
{
    protected $campaignRepository, $biddingRepo;

    /**
     * @param CampaignsInterface $campaignRepository
     * @param BiddingInterface $bidding
     */
    public function __construct(CampaignsInterface $campaignRepository, BiddingInterface $bidding)
    {
        $this->campaignRepository = $campaignRepository;
        $this->biddingRepo = $bidding;
    }

    /**
     * Update or add new campaign.
     *
     * @param AddCampaignRequest $request
     *
     * @return array
     */
    public function addCampaign(AddCampaignRequest $request)
    {
        return $this->campaignRepository->addCampaign([
            'customerId' => $request->input('customerId'),
            'name' => $request->input('name'),
            'microAmount' => $request->input('microAmount'),
            'deliveryMethod' => $request->input('deliveryMethod'),
            'status' => $request->input('status'),
            'adsChannelType' => $request->input('adsChannelType'),
            'biddingStrategy' => $request->input('biddingStrategy'),
            'budgetName' => $request->input('budgetName'),
            'endDay' => $request->input('endDay') ?? null,
            'startDay' => $request->input('startDay') ?? null,
            'finalUrlSuffix' => $request->input('finalUrlSuffix') ?? null,
            'trackingUrlTemplate' => $request->input('trackingUrlTemplate') ?? null,
            'urlCustomParameters' => $request->input('urlCustomParameters') ?? null,
            'doReplace' => $request->input('doReplace'),
        ]);
    }

    /**
     * Update or add new universal app campaign.
     *
     * @param SetUniversalCampaignAppRequest $request
     *
     * @return array
     */
    public function setUniversalAppCampaign(SetUniversalCampaignAppRequest $request)
    {
        return $this->campaignRepository->setUniversalAppCampaign(
            $request->input('customerClientId'),
            $request->input('name'),
            $request->input('status'),
            $request->input('startDay'),
            $request->input('endDay'),
            $request->input('microAmount'),
            $request->input('budgetName'),
            $request->input('deliveryMethod'),
            $request->input('isExplicitlyShared'),
            $request->input('description1'),
            $request->input('description2'),
            $request->input('description3'),
            $request->input('description4'),
            $request->input('appId'),
            $request->input('appMarket'),
            $request->input('appBiddingStrategyGoalType'),
            $request->input('operator'),
            $request->input('$campaignId') ?? ''
        );
    }

    /**
     * @OA\Post(
     *     path="/get/getCampaigns",
     *     operationId="/getCampaigns",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the customer campaigns.",
     *     ),
     * )
     * @param GetCampaignsRequest $request
     *
     * @return mixed
     */
    public function getCampaigns(GetCampaignsRequest $request)
    {
        return $this->campaignRepository->getCampaigns(
            $request->input('customerClientId')
        );
    }

    /**
     * @OA\Post(
     *     path="/add/addSiteLinkExtension",
     *     operationId="/addSiteLinkExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="1773041194")
     *     ),
     *     @OA\Parameter(
     *         name="links",
     *         in="query",
     *         description="The site link extension urls and text. [{text: text 1, url: https://google.com/test-link-extension}]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the campaignId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified campaign.
     *
     * @param AddSiteLinkExtensionRequest $request
     *
     * @return mixed
     */
    public function addSiteLinkExtension(AddSiteLinkExtensionRequest $request)
    {
        return $this->campaignRepository->addSiteLinkExtension(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('links'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * @OA\Post(
     *     path="/add/addCalloutExtension",
     *     operationId="/addCalloutExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="1773041194")
     *     ),
     *     @OA\Parameter(
     *         name="callouts",
     *         in="query",
     *         description="The array of callout titles. For example: [{feedId: 123, value: Text}, ...]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the campaignId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified campaign.
     *
     * @param AddCalloutExtensionRequest $request
     *
     * @return mixed
     */
    public function addCalloutExtension(AddCalloutExtensionRequest $request)
    {
        return $this->campaignRepository->addCalloutExtension(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('callouts'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * Set campaign extensions.
     *
     * @param SetCampaignExtensionsRequest $request
     *
     * @return array
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setExtensions(SetCampaignExtensionsRequest $request)
    {
        return $this->campaignRepository->setExtensions(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('extensions')
        );
    }

    /**
     * @OA\Post(
     *     path="/add/addAppExtension",
     *     operationId="/addAppExtension",
     *     tags={"Extension add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="campaignId",
     *         in="query",
     *         description="id of the corresponding campaign",
     *         required=true,
     *         @OA\Schema(type="string", default="1773041194")
     *     ),
     *     @OA\Parameter(
     *         name="apps",
     *         in="query",
     *         description="The array of apps. store must be 1 for google play and 2 for apple, For example: [appId: com.example.myapp, store: 1, text: Clone me!, link: https://play.google.com/store/apps/details?id=com.example.myapp, ... ]",
     *         required=true,
     *         @OA\Items(type="string"),
     *         @OA\Schema(type="array"),
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the campaignId and extensionType.",
     *     ),
     * )
     *
     * Add site link extension to specified campaign.
     *
     * @param AddAppExtensionRequest $request
     *
     * @return mixed
     */
    public function addAppExtension(AddAppExtensionRequest $request)
    {
        return $this->campaignRepository->addAppExtension(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('apps'),
            $request->input('operator') ?? 'ADD'
        );
    }

    /**
     * Set campaign network settings.
     *
     * @param SetCampaignNetworkSetting $request
     *
     * @return mixed
     */
    public function setNetworkSettings(SetCampaignNetworkSetting $request)
    {
        return $this->campaignRepository->setNetworkSetting(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('contentNetwork'),
            $request->input('searchNetwork'),
            $request->input('googleSearch'),
            $request->input('partnerSearchNetwork')
        );
    }

    /**
     * Set campaign network settings.
     *
     * @param SetCampaignTrackingUrlTemplate $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setTrackingUrlTemplate(SetCampaignTrackingUrlTemplate $request)
    {
        return $this->campaignRepository->setTrackingUrlTemplate(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('trackingUrlTemplate')
        );
    }

    /**
     * @param SetGeoLocationRequest $request
     *
     * @return mixed
     */
    public function setGeoLocation(SetGeoLocationRequest $request)
    {
        return $this->campaignRepository->setGeoTarget(
            $request->input('data'),
            $request->input('customerClientId')
        );
    }

    /**
     * @param SetGeoDistanceRequest $request
     *
     * @return mixed
     */
    public function setGeoLocationDistance(SetGeoDistanceRequest $request)
    {
        return $this->campaignRepository->setGeoDistanceTarget(
            $request->input('data'),
            $request->input('customerClientId')
        );
    }

    /**
     * Set dynamic search ads setting.
     *
     * @param SetDynamicSearchAdsSettingRequest $request
     *
     * @return array|mixed
     */
    public function setDynamicSearchAdsSetting(SetDynamicSearchAdsSettingRequest $request)
    {
        return $this->campaignRepository->setDynamicSearchAdSetting(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('domainName'),
            $request->input('languageCode'),
            $request->input('pageFeed') ?? '',
            $request->input('suppliedUrlsOnly') ?? null
        );
    }

    /**
     * Set final url suffix.
     *
     * @param SetCampaignFinalUrlSuffixRequest $request
     *
     * @return array|mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function setFinalUrlSuffix(SetCampaignFinalUrlSuffixRequest $request)
    {
        return $this->campaignRepository->setFinalUrlSuffix(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('finalUrlSuffix') ?? ''
        );
    }

    /**
     * Set url custom parameters.
     *
     * @param SetCampaignUrlCustomParametersRequest $request
     *
     * @return mixed
     */
    public function setUrlCustomParameters(SetCampaignUrlCustomParametersRequest $request)
    {
        return $this->campaignRepository->setUrlCustomerParameters(
            $request->input('customerClientId'),
            $request->input('campaignId'),
            $request->input('urlCustomParameters') ?? [],
            $request->input('doReplace')
        );
    }

    /**
     * Update or add new budget.
     *
     * @param SetBudgetRequest $request
     *
     * @return mixed
     */
    public function setBudget(SetBudgetRequest $request)
    {
        return $this->campaignRepository->setBudget(
            $request->input('customerClientId'),
            $request->input('name'),
            $request->input('microAmount'),
            $request->input('deliveryMethod'),
            $request->input('isExplicitlyShared'),
            $request->input('budgetId') ?? '',
            $request->input('campaignId') ?? ''
        );
    }

    public function setCampaignBiddingOption(SetCampaignBiddingOption $request)
    {

    }
}
