<?php

namespace App\Http\Controllers;

use App\interfaces\AdWordsSettingsInterface;
use App\Http\Requests\getRequests\GetAdWordsSettingsRequest;

class AdWordsSettingsController extends Controller
{
    /**
     * @var AdWordsSettingsInterface
     */
    private $adWordsSettings;

    /**
     * @var GetAdWordsSettingsRequest
     */
    private $request;

    /**
     * AdGroupController constructor.
     *
     * @param AdWordsSettingsInterface $adWordsSettings
     *
     * @param GetAdWordsSettingsRequest $request
     */
    public function __construct(AdWordsSettingsInterface $adWordsSettings, GetAdWordsSettingsRequest $request)
    {
        $this->adWordsSettings = $adWordsSettings;

        $this->adWordsSettings->initialize(
            $request->input('customerClientId'),
            $request->input('serviceType'),
            $request->input('settingType')
        );

        $this->adWordsSettings->setCurrentObjectId($request->input('objectId'));

        $this->request = $request;
    }

    /**
     * @OA\Post(
     *     path="/get/getSettings",
     *     operationId="/getSettings",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="objectId",
     *         in="query",
     *         description="id of the corresponding object, Based on service type.",
     *         required=true,
     *         @OA\Schema(type="string", default="1773041194")
     *     ),
     *     @OA\Parameter(
     *         name="serviceType",
     *         in="query",
     *         description="Service type. For example: campaign or adGroup",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              default="campaign",
     *              enum={"campaign","adGroup"},
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="settingType",
     *         in="query",
     *         description="Setting type. For example: criterion or extensions",
     *         required=true,
     *         @OA\Schema(
     *              type="string",
     *              default="criterion",
     *              enum={"extensions","criterion"},
     *         )
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the array of data",
     *     ),
     * )
     *
     * @return mixed
     */
    public function getSettings()
    {
        return $this->adWordsSettings->handle($this->request->input('predicates'));
    }
}
