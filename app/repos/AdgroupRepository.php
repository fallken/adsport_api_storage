<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/7/2019
 * Time: 11:52 AM
 */

namespace App\repos;

use App\Exceptions\EndPointRequestException;
use App\Helpers\RetryCallTrait;
use App\Wrappers\WrapperBridge;
use App\interfaces\AdGroupInterface;

use Exception;
use Google\AdsApi\AdWords\v201809\cm\AdGroup;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAd;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdRotationMode;
use Google\AdsApi\AdWords\v201809\cm\AdGroupAdService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupBidModifier;
use Google\AdsApi\AdWords\v201809\cm\AdGroupBidModifierOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupBidModifierService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSetting;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSettingOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupExtensionSettingService;
use Google\AdsApi\AdWords\v201809\cm\AdGroupOperation;
use Google\AdsApi\AdWords\v201809\cm\AdGroupService;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItem;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItemAppStore;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyConfiguration;
use Google\AdsApi\AdWords\v201809\cm\BiddingStrategyType;
use Google\AdsApi\AdWords\v201809\cm\CallFeedItem;
use Google\AdsApi\AdWords\v201809\cm\CalloutFeedItem;
use Google\AdsApi\AdWords\v201809\cm\CpaBid;
use Google\AdsApi\AdWords\v201809\cm\CpcBid;
use Google\AdsApi\AdWords\v201809\cm\CustomParameter;
use Google\AdsApi\AdWords\v201809\cm\CustomParameters;
use Google\AdsApi\AdWords\v201809\cm\ExpandedDynamicSearchAd;
use Google\AdsApi\AdWords\v201809\cm\ExtensionSetting;
use Google\AdsApi\AdWords\v201809\cm\FeedType;
use Google\AdsApi\AdWords\v201809\cm\Money;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\OrderBy;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\cm\Platform;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\SitelinkFeedItem;
use Google\AdsApi\AdWords\v201809\cm\SortOrder;
use Google\AdsApi\AdWords\v201809\cm\TargetingSetting;
use Google\AdsApi\AdWords\v201809\cm\TargetingSettingDetail;
use Google\AdsApi\AdWords\v201809\cm\TargetRoasBiddingScheme;
use Google\AdsApi\AdWords\v201809\cm\UrlList;
use Illuminate\Http\Request;
use LaravelGoogleAds\Services\AdWordsService;

class AdGroupRepository implements AdGroupInterface
{
    use RetryCallTrait;

    /**
     * @var AdWordsService AdWordsService
     */
    protected $adWordsService;

    const PAGE_LIMIT = 500;

    public function __construct(AdWordsService $adWordsService)
    {
        $this->adWordsService = $adWordsService;
    }

    /**
     * Add platform bid modifier.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $adGroupId
     * @param string $operator
     * @param string $platformId
     * @param float $bidModifier
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addPlatformBidModifier(string $customerClientId, string $campaignId, string $adGroupId, string $operator, string $platformId, float $bidModifier)
    {
        $service = $this->adWordsService->getService(AdGroupBidModifierService::class, $customerClientId);

        $adGroupBidModifier = new AdGroupBidModifier();
        $adGroupBidModifier->setAdGroupId($adGroupId);
        $adGroupBidModifier->setCampaignId($campaignId ? $campaignId : null);
        $adGroupBidModifier->setCriterion(new Platform($platformId));
        $adGroupBidModifier->setBidModifier($bidModifier);

        $operation = new AdGroupBidModifierOperation();
        $operation->setOperand($adGroupBidModifier);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation]);
        }, 1)->getValue()[0];

        return [
            'campaignId' => $result->getCampaignId(),
            'adGroupId' => $result->getAdGroupId(),
        ];
    }

    /**
     * Add callout extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $callouts
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addCalloutExtension(string $customerClientId, string $adGroupId, array $callouts, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $feedCallouts = [];

        foreach ($callouts as $callout) {

            $feed = new CalloutFeedItem();
            $feed->setCalloutText($callout->value);

            if ($callout->feedId) {
                $feed->setFeedId($callout->feedId);
            }

            $feedCallouts[] = $feed;
        }

        $adGroupExtensionSetting = new AdGroupExtensionSetting();
        $adGroupExtensionSetting->setAdGroupId($adGroupId);
        $adGroupExtensionSetting->setExtensionType(FeedType::CALLOUT);

        $extensionSetting = new ExtensionSetting();
        $extensionSetting->setExtensions($feedCallouts);
        $adGroupExtensionSetting->setExtensionSetting($extensionSetting);

        $operation = new AdGroupExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($adGroupExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $extensionService = $this->adWordsService->getService(AdGroupExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operation) {
            return $extensionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'adGroupId' => $result->getAdGroupId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * Add callout extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $apps
     * @param string $operator
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addAppExtension(string $customerClientId, string $adGroupId, array $apps, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $appsItems = [];

        foreach ($apps as $app) {

            $app = (object)$app;

            try {
                if (!$app->appId || !$app->store || !$app->text || !$app->link) {

                    throw new EndPointRequestException('Bad site link exception.', 400);
                }
            }
            catch (Exception $exception) {

                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $app->store = (int)$app->store;

            if ($app->store === 1) {

                $app->store = AppFeedItemAppStore::GOOGLE_PLAY;
            }
            else if ($app->store === 2) {

                $app->store = AppFeedItemAppStore::APPLE_ITUNES;
            }
            else {
                throw new EndPointRequestException('Invalid app store exception.', 400);
            }

            $appFeedItem = new AppFeedItem();
            $appFeedItem->setAppId($app->appId);
            $appFeedItem->setAppStore($app->store);
            $appFeedItem->setAppLinkText($app->text);

            try {
                $appFeedItem->setAppFinalUrls(new UrlList([$app->link]));
            }
            catch (Exception $exception) {
                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $appsItems[] = $appFeedItem;
        }

        $adGroupExtensionSetting = new AdGroupExtensionSetting();
        $adGroupExtensionSetting->setAdGroupId($adGroupId);
        $adGroupExtensionSetting->setExtensionType(FeedType::APP);

        $extensionSetting = new ExtensionSetting();
        $extensionSetting->setExtensions($appsItems);
        $adGroupExtensionSetting->setExtensionSetting($extensionSetting);

        $operation = new AdGroupExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($adGroupExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $extensionService = $this->adWordsService->getService(AdGroupExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operation) {
            return $extensionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'adGroupId' => $result->getAdGroupId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * Add site link extension to specified adGroup.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $links
     * @param string $operator
     *
     * @param string $responseType
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function addSiteLinkExtension(string $customerClientId, string $adGroupId, array $links, string $operator = 'ADD', string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $siteLinks = [];

        foreach ($links as $link) {

            $link = (object)$link;

            try {
                if (!$link->url || !$link->text) {

                    throw new EndPointRequestException("Bad link text exception.", 400);
                }
            }
            catch (Exception $exception) {
                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $siteLink = new SitelinkFeedItem();
            $siteLink->setSitelinkText($link->text);

            try {
                $siteLink->setSitelinkFinalUrls(new UrlList([$link->url]));
            }
            catch (Exception $exception) {

                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            $siteLinks[] = $siteLink;
        }

        $adGroupExtensionSetting = new AdGroupExtensionSetting();
        $adGroupExtensionSetting->setAdGroupId($adGroupId);
        $adGroupExtensionSetting->setExtensionType(FeedType::SITELINK);
        $adGroupExtensionSetting->setExtensionSetting(new ExtensionSetting());
        $adGroupExtensionSetting->getExtensionSetting()->setExtensions($siteLinks);

        $operation = new AdGroupExtensionSettingOperation();
        $operation->setOperator($operator);
        $operation->setOperand($adGroupExtensionSetting);

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operation;
        }

        $extensionService = $this->adWordsService->getService(AdGroupExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operation) {
            return $extensionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'adGroupId' => $result->getAdGroupId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * * Set ad group extensions.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $extensions
     * @param string $responseType
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setExtensions(string $customerClientId, string $adGroupId, array $extensions, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $operations = [];

        foreach ($extensions as $extension) {

            try {
                $extension = (object)$extension;

                if (!$extension->type) {
                    throw new EndPointRequestException('Bad extension type exception.', 400);
                }

                if (!$extension->operator) {
                    $extension->operator = Operator::ADD;
                }
            }
            catch (Exception $exception) {
                throw new EndPointRequestException($exception->getMessage(), 400);
            }

            switch ($extension->type) {
                case 'app':
                    $operations[] = $this->addAppExtension('', $adGroupId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
                case 'callout':
                    $operations[] = $this->addCalloutExtension('', $$adGroupId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
                case 'sitelink':
                    $operations[] = $this->addSiteLinkExtension('', $$adGroupId, [$extension], $extension->operator, static::RESPONSE_TYPE_OPERATION);
                    break;
            }
        }

        if ($responseType == static::RESPONSE_TYPE_OPERATION) {
            return $operations;
        }

        $extensionService = $this->adWordsService->getService(AdGroupExtensionSettingService::class, $customerClientId);

        $result = $this->call(function () use ($extensionService, $operations) {
            return $extensionService->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'adGroupId' => $result->getAdGroupId(),
            'extensionType' => $result->getExtensionType(),
        ];
    }

    /**
     * Get ad groups by campaign id.
     *
     * @param string $customerClientId
     * @param string $campaignId
     * @param array $predicates
     * @param string $responseType
     *
     * @return mixed|void
     *
     * @throws EndPointRequestException
     */
    public function getAdGroups(string $customerClientId, string $campaignId, array $predicates = [], string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $adGroupService = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        $selectorFields = [
            'Id',
            'Name',
            'Status',
            'Labels',
            'Settings',
            'CampaignId',
            'AdGroupType',
            'CampaignName',
            'BaseAdGroupId',
            'BaseCampaignId',
            'FinalUrlSuffix',
            'AdRotationMode',
            'UrlCustomParameters',
            'TrackingUrlTemplate',

            'ContentBidCriterionTypeGroup',

            'TargetCpaBid',
            'TargetCpaBidSource',
            'CpcBid',
            'CpmBid',

            'BiddingStrategy',
            'BiddingStrategyId',
            'BiddingStrategyName',
            'BiddingStrategyType',
            'BiddingStrategySource',
            'TargetRoasOverride',
        ];

        $selector = new Selector();

        $selector->setFields($selectorFields);

        $selector->setOrdering([new OrderBy('Name', SortOrder::ASCENDING)]);

        $selectorPredicates = [new Predicate('CampaignId', PredicateOperator::IN, [$campaignId])];

        foreach ($predicates as $predicate) {

            $predicate = (object)$predicate;

            $selectorPredicates[] = new Predicate($predicate->field, $predicate->operator, $predicate->values);
        }

        $selector->setPredicates($selectorPredicates);

        $selector->setPaging(new Paging(0, static::PAGE_LIMIT));

        $page = $adGroupService->get($selector);

        $results = [];

        $entries = $page->getEntries() ?? [];

        if ($responseType == static::RESPONSE_TYPE_PURE) {
            return $entries;
        }

        foreach ($entries as $index => $adGroup) {

            $results[] = WrapperBridge::toObject($adGroup);
        }

        return $results;
    }

    /**
     * Set tracking url template.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param string $trackingUrlTemplate
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setTrackingUrlTemplate(string $customerClientId, string $adGroupId, string $trackingUrlTemplate)
    {
        $service = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        $adGroup = new AdGroup();
        $adGroup->setId($adGroupId);
        $adGroup->setTrackingUrlTemplate($trackingUrlTemplate);

        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operation->setOperator(Operator::SET);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        });

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    /**
     * Set url custom parameters.
     *
     * @param string $customerClientId
     * @param string $adGroupId
     * @param array $urlCustomParameters
     * @param null $doReplace
     * @param string $responseType
     *
     * @return mixed
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setUrlCustomerParameters(string $customerClientId, string $adGroupId, array $urlCustomParameters, $doReplace = null, string $responseType = self::RESPONSE_TYPE_JSON)
    {
        $parameters = [];

        foreach ($urlCustomParameters as $parameter) {

            $customParameter = new CustomParameter();

            $customParameter->setKey($parameter->key);
            $customParameter->setValue($parameter->value);
            $customParameter->setValue($parameter->isRemoved ?? false);

            $parameters[] = $customParameter;
        }

        $urlCustomParameters = new CustomParameters($parameters);

        $urlCustomParameters->setDoReplace($doReplace);

        if ($responseType == static::RESPONSE_TYPE_INSTANCE) {
            return $urlCustomParameters;
        }

        $adGroup = new AdGroup();
        $adGroup->setId($adGroupId);
        $adGroup->setUrlCustomParameters($urlCustomParameters);

        $operation = new AdGroupOperation();
        $operation->setOperator(Operator::SET);
        $operation->setOperand($adGroup);

        $service = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        $result = $this->call(function () use ($service, $operation) {
            return $service->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'id' => $result->getId(),
            'name' => $result->getName(),
        ];
    }

    public function changeAdgroupStatus(Request $request)
    {
        $customerClientId = $request->customerId;
        $adGroupId = $request->adgroupId;

        $adGroupCriterionService = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        $operations = [];
        // Create ad group with the specified ID.
        $adGroup = new AdGroup();
        $adGroup->setId($adGroupId);
        $adGroup->setStatus($request->status);

        // Update final URL.
        /*$adGroupCriterion->setFinalUrls(
            new UrlList(['http://www.example.com/new'])
        );*/

        // Create ad group operation and add it to the list.
        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operator = $request->status == 'REMOVED' ? 'REMOVE' : 'SET';
        $operation->setOperator($operator);
        $operations[] = $operation;

        // Update the ad group on the server.
        $result = $adGroupCriterionService->mutate($operations);

        $adGroup = $result->getValue()[0];


        // Create ad group criterion operation and add it to the list.
        $operation->setOperator($operator);

        // Update the keyword on the server.
        try {
            $result = $adGroupCriterionService->mutate($operations);
            $adGroup = $result->getValue()[0];
            $resultArr = [
                'status' => $adGroup->getStatus(),
                'adgroupId' => $adGroup->getId(),
                'campaignName' => $adGroup->getCampaignName(),
                'campaignId' => $adGroup->getCampaignId()
            ];

            return response()->json($resultArr, 200);
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    public function changeAdgroupName(Request $request)
    {
        $customerClientId = $request->customerId;
        $adGroupId = $request->adgroupId;
        $name = $request->name;

        $adGroupCriterionService = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        $operations = [];
        // Create ad group with the specified ID.
        $adGroup = new AdGroup();
        $adGroup->setId($adGroupId);
        $adGroup->setName($name);

        // Create ad group operation and add it to the list.
        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operation->setOperator(Operator::SET);
        $operations[] = $operation;

        // Update the ad group on the server.
        $result = $adGroupCriterionService->mutate($operations);

        // Create ad group criterion operation and add it to the list.
        $operation->setOperator(Operator::SET);

        // Update the keyword on the server.
        try {
            $result = $adGroupCriterionService->mutate($operations);
            $adGroup = $result->getValue()[0];
            $resultArr = [
                'name' => $adGroup->getName(),
                'adgroupId' => $adGroup->getId(),
                'campaignName' => $adGroup->getCampaignName()
            ];
            return response()->json($resultArr, 200);
        }
        catch (Exception $e) {
            return response()->json($e->getMessage(), 430);
        }
    }

    /**
     * @param string $customerClientId
     * @param string $campaignId
     * @param string $name
     * @param string $microAmount
     * @param string $status
     * @param string $criterionType
     * @param string $adRotationMode
     * @param string|null $trackingUrlTemplate
     * @param array $urlCustomParameters
     * @param bool $doReplace
     * @param string $operator
     * @param string $adGroupId
     *
     * @return array
     *
     * @throws EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function setAdGroup(string $customerClientId, string $campaignId, string $name, string $microAmount, string $status, string $criterionType, string $adRotationMode, $trackingUrlTemplate, array $urlCustomParameters, bool $doReplace, string $operator, string $adGroupId = '')
    {
        $adGroupCriterionService = $this->adWordsService->getService(AdGroupService::class, $customerClientId);

        // Create ad group with the specified ID.
        $adGroup = new AdGroup();

        if ($adGroupId) {
            $adGroup->setId($adGroupId);
        }

        $adGroup->setCampaignId($campaignId);
        $adGroup->setName($name);

        $money = new Money();
        $money->setMicroAmount($microAmount);

        // Set bids (required).
        $bid = new CpcBid();
        $bid->setBid($money);
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();
        $biddingStrategyConfiguration->setBids([$bid]);
        $adGroup->setBiddingStrategyConfiguration($biddingStrategyConfiguration);

        $adGroup->setStatus($status); //AdGroupStatus::ENABLED

        // Search campaigns targeting using a remarketing list.
        $targetingSetting = new TargetingSetting();
        $detail = new TargetingSettingDetail($criterionType, true);

        $targetingSetting->setDetails([$detail]);
        $adGroup->setSettings([$targetingSetting]);

        // Set the rotation mode.
        $rotationMode = new AdGroupAdRotationMode($adRotationMode); // AdRotationMode::OPTIMIZE
        $adGroup->setAdGroupAdRotationMode($rotationMode);

        $adGroup->setUrlCustomParameters(
            $this->setUrlCustomerParameters('', '', $urlCustomParameters, $doReplace, static::RESPONSE_TYPE_INSTANCE)
        );

        $adGroup->setTrackingUrlTemplate($trackingUrlTemplate);

        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operation->setOperator($operator);

        $result = $this->call(function () use ($adGroupCriterionService, $operation) {
            return $adGroupCriterionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'name' => $result->getName(),
            'adgroupId' => $result->getId(),
            'campaignName' => $result->getCampaignName()
        ];
    }

    /**
     * @param $params
     * @return mixed
     * desc:will add adgroup ad extension to the corresponding adgroup
     * parameters:adgroupId,countryCode, phoneNumber,
     *
     * @throws EndPointRequestException
     */
    public function addAdgroupCallExtension($params)
    {
        //params
        $adGroupId = $params['adGroupId'];
        $phoneNumber = $params['phoneNumber'];
        $countryCode = $params['countryCode'] ?? 'IR';
        $customerClientId = $params['customerClientId'];

        $adGroupExtensionSetting = $this->adWordsService->getService(AdGroupExtensionSettingService::class, $customerClientId);

        $callExtension = new CallFeedItem();
        $callExtension->setCallCountryCode($countryCode);
        $callExtension->setCallPhoneNumber($phoneNumber);
        $callExtensions[] = $callExtension;


        $adGroup = new AdGroupExtensionSetting();
        $adGroup->setAdGroupId($adGroupId);
        $adGroup->setExtensionType(FeedType::CALL);
        $adGroup->setExtensionSetting(new ExtensionSetting());
        $adGroup->getExtensionSetting()->setExtensions($callExtensions);

        //operation
        $operation = new AdGroupExtensionSettingOperation();
        $operation->setOperator(Operator::ADD);
        $operation->setOperand($adGroup);
        $operations = [$operation];

        $result = $this->call(function () use ($adGroupExtensionSetting, $operations) {
            return $adGroupExtensionSetting->mutate($operations)->getValue()[0];
        }, 1);

        return [
            'adgGroupId' => $result->getAdgroupId(),
            'extensionType' => $result->getExtensionType()
        ];
    }

    public function setExpandedResponsiveAd($customerId, $data)
    {
        $adgroupService = $this->adWordsService->getService(AdGroupAdService::class, $customerId);
        $expandedResponsiveAd = new ExpandedDynamicSearchAd();

        $expandedResponsiveAd->setDescription($data->description1);
        $expandedResponsiveAd->setDescription2($data->description2);

        // Create ad group ad.
        $adGroupAd = new AdGroupAd();
        $adGroupAd->setAdGroupId($data->adgroupId);
        $adGroupAd->setAd($expandedResponsiveAd);

//        / Optional: Set additional settings.
//        $adGroupAd->setStatus(AdGroupAdStatus::PAUSED);

        // Create ad group ad operation and add it to the list.
        $operation = new AdGroupAdOperation();
        $operation->setOperand($adGroupAd);
        $operation->setOperator(Operator::REMOVE);
        $result = $this->call(function () use ($adgroupService, $operation) {
            return $adgroupService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            'adgroupId' => $result->getAdGroupId(),
        ];
    }

    /**
     * @param $customerId
     * @param $data
     * @return mixed
     * decs: will set the max cpc for the adgroup
     */
    public function setAdgroupSetting($customerId, $data)
    {
        $campaignId = $data->campaignId;
        $name = $data->name ?? null;
        $status = $data->status ?? null;
        $CriterionTypeGroup = $data->criterionType ?? null;
        $AdRotationMode = $data->adRotationMode ?? null;

        $adGroupCriterionService = $this->adWordsService->getService(AdGroupService::class, $customerId);
        $operations = [];
        // Create ad group with the specified ID.
        $adGroup = new AdGroup();
        $adGroup->setCampaignId($campaignId);
        $adGroup->setId($data->adgroupId);
        if (isset($name)):
            $adGroup->setName($name);
        endif;
        $biddingStrategyConfiguration = new BiddingStrategyConfiguration();

        if (isset($data->bid)):
            switch ($data->bid):
                case 'cpc':
                    $money = new Money();
                    $money->setMicroAmount($data->amount);
                    // Set bids (required).
                    $bid = new CpcBid();
                    $bid->setBid($money);
                    $biddingStrategyConfiguration->setBids([$bid]);
                    break;
                case 'cpa':
                    $money = new Money();
                    $money->setMicroAmount($data->amount);
                    $bid = new CpaBid();
                    $bid->setBid($money);
                    $biddingStrategyConfiguration->setBids([$bid]);
                    break;
                case 'roas':
                    $biddingStrategyConfiguration->setBiddingStrategyType(
                        BiddingStrategyType::TARGET_ROAS
                    );//BiddingStrategyType::MANUAL_CPC
                    $roasScheme = new TargetRoasBiddingScheme();

                    $bidFloor = new Money();
                    $bidFloor->setMicroAmount($data->bidFloor);

                    $bidCeiling = new Money();
                    $bidCeiling->setMicroAmount($data->bidCeiling);

                    $roasScheme->setTargetRoas($data->targetRoas);//will be a float number
                    $roasScheme->setBidCeiling($bidCeiling);
                    $roasScheme->setBidFloor($bidFloor);
                    $biddingStrategyConfiguration->setBiddingScheme($roasScheme);
                    break;
            endswitch;
            $adGroup->setBiddingStrategyConfiguration($biddingStrategyConfiguration);
        endif;
        if (isset($data->status)):
            $adGroup->setStatus($status);//AdGroupStatus::ENABLED
        endif;
        if (isset($data->targetingSetting)):
            $targetingSetting = new TargetingSetting();
            $details = [];
            $details[] = new TargetingSettingDetail($CriterionTypeGroup, true);
            $targetingSetting->setDetails($details);
            $adGroup->setSettings([$targetingSetting]);
        endif;
        if (isset($data->rotationMode)):
            $rotationMode = new AdGroupAdRotationMode($data->rotationMode);//AdRotationMode::OPTIMIZE
            // $rotationMode = new AdGroupAdRotationMode(AdRotationMode::OPTIMIZE);
            $adGroup->setAdGroupAdRotationMode($rotationMode);
        endif;
        // Create an ad group operation and add it to the operations list.
        $operation = new AdGroupOperation();
        $operation->setOperand($adGroup);
        $operation->setOperator($data->operator ?? Operator::SET);
        $operations[] = $operation;

        $result = $this->call(function () use ($adGroupCriterionService, $operation) {
            return $adGroupCriterionService->mutate([$operation])->getValue()[0];
        }, 1);

        return [
            $result
        ];
    }
}
