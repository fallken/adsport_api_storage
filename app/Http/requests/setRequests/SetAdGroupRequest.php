<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests\setRequests;

use App\Exceptions\EndPointRequestException;

use stdClass;
use Anik\Form\FormRequest;

class SetAdGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     *
     * @throws EndPointRequestException
     */
    public function authorize()
    {
        if ($this->has('doReplace')) {
            $this->merge(['doReplace' => $this->input('doReplace') === "true"]);
        }

        $urlCustomParametersHolder = @json_decode($this->input('urlCustomParameters'));

        if (!$urlCustomParametersHolder) {
            $urlCustomParametersHolder = $this->input('urlCustomParameters');
        }

        if (!is_array($urlCustomParametersHolder)) {
            $urlCustomParametersHolder = [];
        }

        $urlCustomParameters = [];

        foreach ($urlCustomParametersHolder as $parameter) {

            if (!($parameter instanceof stdClass)) {
                if (is_array($parameter)) {
                    $parameter = (object)$parameter;
                }
                else {
                    $parameter = json_decode($parameter);
                }
            }

            if (!isset($parameter->key) || !isset($parameter->value)) {
                throw new EndPointRequestException("Bad url custom parameter exception. Payload is: " . json_encode($parameter), 422);
            }

            $urlCustomParameters[] = $parameter;
        }

        $this->merge(['urlCustomParameters' => $urlCustomParameters]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'customerId' => 'required|string',
            'campaignId' => 'required',
            'name' => 'required|string',
            'status' => 'required|in:ENABLED,REMOVED,PAUSED',
            'amount' => 'required|string',
            'operator' => 'nullable|string',
            'urlCustomParameters' => 'nullable|array',
            'trackingUrlTemplate' => 'nullable|string',
            'doReplace' => 'nullable|bool',
            'criterionType' => 'required|in:KEYWORD,USER_INTEREST_AND_LIST,VERTICAL,GENDER,AGE_RANGE,PLACEMENT,PARENT,INCOME_RANGE,NONE,UNKNOWN',
            'adGroupId' => 'nullable|string',
            'adRotationMode' => 'required|in:UNKNOWN,OPTIMIZE,ROTATE_FOREVER'
        ];
    }
}
