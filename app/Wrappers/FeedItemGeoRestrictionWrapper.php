<?php

namespace App\Wrappers;

class FeedItemGeoRestrictionWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'geoRestriction' => $data->getGeoRestriction(),
        ];
    }
}
