<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/9/2019
 * Time: 11:59 AM
 */

namespace App\Http\requests\addRequests;


use Anik\Form\FormRequest;

class AddBudgetOrderRequest extends FormRequest
{

    protected function rules()
    {
        return [
            'customerId'=>'required',
            'startTime'=>'string',
            'endTime'=>'string',
            'microAmount'=>'required'
        ];
    }

    protected function messages () {
        return [];
    }
}
