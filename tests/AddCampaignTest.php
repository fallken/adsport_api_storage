<?php

use App\repos\CampaignsRepository;

use Illuminate\Support\Str;
use Google\AdsApi\AdWords\v201809\cm\Operator;
use Google\AdsApi\AdWords\v201809\cm\CampaignStatus;
use Google\AdsApi\AdWords\v201809\cm\MobileApplicationVendor;
use Google\AdsApi\AdWords\v201809\cm\BudgetBudgetDeliveryMethod;
use Google\AdsApi\AdWords\v201809\cm\UniversalAppBiddingStrategyGoalType;

class AddCampaignTest extends TestCase
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = new CampaignsRepository();
    }

    /**
     * Test for add universal app campaign.
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddUniversalAppCampaign()
    {
        $universalApp = (object)[
            'customerClientId' => '541-403-9496',
            'campaignId' => '2045454855',
            'name' => 'The first app - ' . Str::random(),
            'status' => CampaignStatus::PAUSED,
            'startDay' => date('ymd'),
            'endDay' => date('ymd'),
            'microAmount' => '1400000',
            'budgetName' => 'Budget - ' . Str::random(),
            'deliveryMethod' => BudgetBudgetDeliveryMethod::STANDARD,
            'isExplicitlyShared' => false,
            'description1' => '1 desc',
            'description2' => '2 desc',
            'description3' => '3 desc',
            'description4' => '4 desc',
            'appId' => 'com.android.chrome',
            'appMarket' => MobileApplicationVendor::VENDOR_GOOGLE_MARKET,
            'appBiddingStrategyGoalType' => UniversalAppBiddingStrategyGoalType::OPTIMIZE_FOR_INSTALL_CONVERSION_VOLUME,
            'operator' => Operator::ADD
        ];

        $result = $this->campaignRepository->setUniversalAppCampaign(
            $universalApp->customerClientId,
            $universalApp->name,
            $universalApp->status,
            $universalApp->startDay,
            $universalApp->endDay,
            $universalApp->microAmount,
            $universalApp->budgetName,
            $universalApp->deliveryMethod,
            $universalApp->isExplicitlyShared,
            $universalApp->description1,
            $universalApp->description2,
            $universalApp->description3,
            $universalApp->description4,
            $universalApp->appId,
            $universalApp->appMarket,
            $universalApp->appBiddingStrategyGoalType,
            $universalApp->operator
        );

        print_r($result);

        $this->assertIsArray($result);

        $universalApp->campaignId = '';
        $universalApp->name = Str::random();
        $universalApp->budgetName = Str::random();

        $response = $this->post('/set/setUniversalAppCampaign', (array)$universalApp);

        print_r($response->response->getContent());

        $response->assertResponseOk();
    }

    /**
     * Test for add universal app campaign.
     *
     * @throws \App\Exceptions\EndPointRequestException
     * @throws \App\Exceptions\ThreadShutdownException
     */
    public function testAddSettingsForUniversalAppCampaign()
    {
        $universalApp = (object)[
            'customerClientId' => '541-403-9496',
            'campaignId' => '2069026480',
        ];

        $result = $this->campaignRepository->addCampaignSetting(
            $universalApp->customerClientId,
            $universalApp->campaignId, [
                (object)['operator' =>'ADD', 'type' => 'location', 'isNegative' => null, 'id' => "1019"],
                (object)['operator' =>'ADD', 'type' => 'location', 'isNegative' => null, 'id' => "2484L"]
            ]
        );

        print_r($result);

        $this->assertIsArray($result);
    }
}
