<?php

namespace App\Http\Interfaces;

use App\Http\Requests\getRequests\GetAdsRequest;
use App\Http\Requests\addRequests\AddCallOnlyAdRequest;
use App\Http\requests\setRequests\setAdGroupAdExpandedDynamicSearchAdRequest;

interface AdGroupAdControllerInterface
{

    /**
     * @OA\Post(
     *     path="/add/addAdGroupCallOnlyAd",
     *     operationId="/addAdGroupCallOnlyAd",
     *     tags={"add operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="id of the corresponding ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="countryCode",
     *         in="query",
     *         description="code of the corresponding country",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="phone",
     *         in="query",
     *         description="The phone number",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="businessName",
     *         in="query",
     *         description="The business name",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description1",
     *         in="query",
     *         description="The description1 text",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description2",
     *         in="query",
     *         description="The description2 text",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="callTracked",
     *         in="query",
     *         description="The callTracked status",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *     @OA\Parameter(
     *         name="conversionTypeId",
     *         in="query",
     *         description="The conversionTypeId",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="disableCallConversion",
     *         in="query",
     *         description="The disableCallConversion",
     *         required=false,
     *         @OA\Schema(type="boolean", default="")
     *     ),
     *     @OA\Parameter(
     *         name="phoneNumberVerificationUrl",
     *         in="query",
     *         description="The phoneNumberVerificationUrl",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the call only id: {Id: 123123}",
     *     ),
     * )
     *
     * @param AddCallOnlyAdRequest $request
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function addCallOnly(AddCallOnlyAdRequest $request);

    /**
     * @OA\Post(
     *     path="/set/setAdGroupAdExpandedDynamicSearchAd",
     *     operationId="/setAdGroupAdExpandedDynamicSearchAd",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="261-911-0558")
     *     ),
     *     @OA\Parameter(
     *         name="adGroupId",
     *         in="query",
     *         description="The id of ad group",
     *         required=true,
     *         @OA\Schema(type="string", default="66844116696")
     *     ),
     *     @OA\Parameter(
     *         name="adId",
     *         in="query",
     *         description="The id of ad",
     *         required=true,
     *         @OA\Schema(type="string", default="332591138289")
     *     ),
     *     @OA\Parameter(
     *         name="description",
     *         in="query",
     *         description="Description",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="description2",
     *         in="query",
     *         description="Description 2",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="Response: adId, adGroupId, status",
     *     ),
     * )
     *
     * Set url custom parameters.
     *
     * @param setAdGroupAdExpandedDynamicSearchAdRequest $request
     *
     * @return mixed
     */
    public function setExpandedDynamicSearchAd(setAdGroupAdExpandedDynamicSearchAdRequest $request);

    /**
     * @OA\Post(
     *     path="/get/getAdgroupAds",
     *     operationId="/getAdgroupAds",
     *     tags={"get operation"},
     *     @OA\Parameter(
     *         name="customerId",
     *         in="query",
     *         description="id of the corresponding customer",
     *         required=true,
     *         @OA\Schema(type="string"
     *          ,default="261-911-0558"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="adgroupId",
     *         in="query",
     *         description="id of the corresponding ad group",
     *         required=false,
     *         @OA\Schema(type="string"
     *          ,default="66844116696"
     *          )
     *     ),
     *     @OA\Parameter(
     *         name="predicates",
     *         in="query",
     *         description="Predicates must be array of predicate. Keys: field, operator, values[]",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the adgroup ads for the correspoding adgroup using its id",
     *     ),
     * )
     *
     * Get account level or ad group level ads.
     *
     * @param GetAdsRequest $request
     *
     * @return array
     */
    public function getAds(GetAdsRequest $request);

}
