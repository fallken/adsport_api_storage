<?php

namespace App\Wrappers;

class AdScheduleWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'id' => $data->getId(),
            'type' => $data->getType(),
            'criterionType' => $data->getCriterionType(),

            'endHour' => $data->getEndHour(),
            'dayOfWeek' => $data->getDayOfWeek(),
            'startHour' => $data->getStartHour(),
            'endMinute' => $data->getEndMinute(),
            'startMinute' => $data->getStartMinute(),
        ];
    }
}
