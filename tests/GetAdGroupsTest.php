<?php

use App\repos\AdGroupRepository;

class GetAdGroupsTest extends TestCase
{
    /**
     * @var AdGroupRepository
     */
    private $adGroupRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adGroupRepository = resolve(AdGroupRepository::class);
    }

    /**
     * Get ad groups from google ad words.
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testGetAdGroups()
    {
        $result = $this->adGroupRepository->getAdGroups('541-403-9496', '1773041194');

        $this->assertNotNull($result);

        $this->assertStringContainsString('Id', $result);
    }
}
