<?php

namespace App\Http\Interfaces;

use App\Http\requests\setRequests\SetSharedBiddingStrategyRequest;

interface CustomerControllerInterface
{
    /**
     * @OA\Post(
     *     path="/set/setSharedBiddingStrategy",
     *     operationId="/setSharedBiddingStrategy",
     *     tags={"set operation"},
     *     @OA\Parameter(
     *         name="customerClientId",
     *         in="query",
     *         description="id of the corresponding customer client.",
     *         required=true,
     *         @OA\Schema(type="string", default="541-403-9496")
     *     ),
     *     @OA\Parameter(
     *         name="strategyId",
     *         in="query",
     *         description="The id of shared bidding strategy",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="operator",
     *         in="query",
     *         description="The acction operator",
     *         required=true,
     *         @OA\Schema(type="string", default="ADD", enum={"ADD", "SET"})
     *     ),
     *     @OA\Parameter(
     *         name="name",
     *         in="query",
     *         description="The name of shared bidding strategy",
     *         required=true,
     *         @OA\Schema(type="string", default="I am shared bidding strategy")
     *     ),
     *     @OA\Parameter(
     *         name="status",
     *         in="query",
     *         description="The status of shared bidding strategy",
     *         required=true,
     *         @OA\Schema(type="string", default="ENABLED", enum={"ENABLED", "REMOVED"})
     *     ),
     *     @OA\Parameter(
     *         name="strategyType",
     *         in="query",
     *         description="The type of shared bidding strategy",
     *         required=true,
     *         @OA\Schema(type="string", default="TARGET_ROAS", enum={"TARGET_ROAS", "TARGET_SPEND", "TARGET_CPA"})
     *     ),
     *     @OA\Parameter(
     *         name="microAmount",
     *         in="query",
     *         description="The micro amount",
     *         required=true,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="bidCeilingAmount",
     *         in="query",
     *         description="The bid ceiling amount",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *     @OA\Parameter(
     *         name="bidFloorAmount",
     *         in="query",
     *         description="The bid floor amount",
     *         required=false,
     *         @OA\Schema(type="string", default="")
     *     ),
     *  @OA\Response(
     *    @OA\MediaType(mediaType="application/json"),
     *         response="200",
     *         description="will return the id and name of created strategy.",
     *     ),
     * )
     *
     * Set shared bidding strategy.
     *
     * @param SetSharedBiddingStrategyRequest $request
     *
     * @return mixed
     */
    public function setSharedBiddingStrategy(SetSharedBiddingStrategyRequest $request);
}
