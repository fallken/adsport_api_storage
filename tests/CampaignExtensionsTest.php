<?php

use App\repos\CampaignsRepository;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItemAppStore;

class CampaignExtensionsTest extends TestCase
{
    /**
     * @var CampaignsRepository
     */
    private $campaignRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->campaignRepository = resolve(CampaignsRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddSiteLinkExtension()
    {
        $customerClientId = '541-403-9496';
        $campaignId = '2045454855';

        $links = [
            ['text' => 'Test', 'url' => 'https://google.com/sdafsdf']
        ];

        $result = $this->campaignRepository->addSiteLinkExtension($customerClientId, $campaignId, $links, 'SET');

        $this->assertIsArray($result);

        print_r($result);

        $response = $this->post('/add/addSiteLinkExtension', [
            'customerClientId' => $customerClientId,
            'campaignId' => $campaignId,
            'links' => $links,
        ]);

        $response->assertResponseStatus(200);

        print_r($response->response->getContent());

        $links = [
            ['text' => 'Test', 'url' => 'bad link!']
        ];

        $response = $this->post('/add/addSiteLinkExtension', [
            'customerClientId' => $customerClientId,
            'campaignId' => $campaignId,
            'links' => $links,
        ]);

        $response->assertResponseStatus(400);

        print_r($response->response->getContent());
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddCalloutExtension()
    {
        $result = $this->campaignRepository->addCalloutExtension('541-403-9496', '1773041194', ['Test', 'asda a ']);

        $this->assertIsArray($result);

        print_r($result);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddAppExtension()
    {
        $customerClientId = '541-403-9496';
        $campaignId = '2045454855';

        $apps = [
            [
                'text' => 'Clone me!',
                'appId' => 'com.example.myapp',
                'store' => AppFeedItemAppStore::GOOGLE_PLAY,
                'link' => 'https://play.google.com/store/apps/details?id=com.example.myapp',
            ],
        ];

        $result = $this->campaignRepository->addAppExtension($customerClientId, $campaignId, $apps, 'ADD');

        $this->assertIsArray($result);

        print_r($result);

        $response = $this->post('/add/addAppExtension', [
            'apps' => $apps,
            'campaignId' => $campaignId,
            'customerClientId' => $customerClientId,
        ]);

        $response->assertResponseStatus(200);

        print_r($response->response->getContent());
    }
}
