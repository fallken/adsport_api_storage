<?php

use App\repos\AdGroupRepository;
use Google\AdsApi\AdWords\v201809\cm\AppFeedItemAppStore;

class AddAdGroupAppExtensionTest extends TestCase
{
    /**
     * @var AdGroupRepository
     */
    private $adGroupRepository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->adGroupRepository = resolve(AdGroupRepository::class);
    }

    /**
     * @throws \App\Exceptions\EndPointRequestException
     */
    public function testAddAppExtension()
    {
        $customerClientId = '261-911-0558';
        $adGroupId = '64912739685';

        $apps = [
            [
                'text' => 'Clone me!',
                'appId' => 'com.example.myapp',
                'store' => AppFeedItemAppStore::GOOGLE_PLAY,
                'link' => 'https://play.google.com/store/apps/details?id=com.example.myapp',
            ],
        ];

        $result = $this->adGroupRepository->addAppExtension($customerClientId, $adGroupId, $apps);

        $this->assertNotNull($result);

        $this->assertIsArray($result);

        $response = $this->post('/add/addAdGroupAppExtension', [
            'apps' => $apps,
            'adGroupId' => $adGroupId,
            'customerClientId' => $customerClientId,
        ]);

        $response->assertResponseStatus(200);
    }
}
