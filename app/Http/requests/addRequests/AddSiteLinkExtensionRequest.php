<?php

namespace App\Http\Requests\addRequests;

use Anik\Form\FormRequest;

class AddSiteLinkExtensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $links = @json_decode($this->input('links'));

        if (!$links) {
            $links = $this->input('links');
        }

        $this->merge(['links' => $links]);

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerClientId' => ['required', 'string'],
            'campaignId' => ['required', 'string'],
            'links' => ['required', 'array', 'max:4']
        ];
    }
}
