<?php

namespace App\Wrappers;

class AssetWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to object.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     */
    public static function toObject(&$data)
    {
        return (object)[
            'getAssetId' => $data->getAssetId(),
            'getAssetName' => $data->getAssetName(),
            'getAssetType' => $data->getAssetType(),
            'getAssetStatus' => $data->getAssetStatus(),
            'getAssetSubtype' => $data->getAssetSubtype(),
        ];
    }
}
