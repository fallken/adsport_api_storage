<?php

namespace App\Wrappers;

class UniversalAppAdWrapper implements AdWordsWrapperInterface
{
    /**
     * Convert data to array.
     *
     * @param mixed $data
     *
     * @return mixed
     *
     * @throws \App\Exceptions\EndPointRequestException
     */
    public static function toObject(&$data)
    {
        // Headlines
        $headlines = null;
        $headlinesOriginal = $data->getHeadlines();

        if (is_array($headlinesOriginal)) {

            $headlines = [];

            foreach ($headlinesOriginal as $item) {

                $headlines[] = WrapperBridge::toObject($item);
            }
        }

        // Descriptions
        $descriptions = null;
        $descriptionsOriginal = $data->getDescriptions();

        if (is_array($descriptionsOriginal)) {

            $descriptions = [];

            foreach ($descriptions as $item) {

                $descriptions[] = WrapperBridge::toObject($item);
            }
        }

        // Images
        $images = null;
        $imagesOriginal = $data->getImages();

        if (is_array($imagesOriginal)) {

            $images = [];

            foreach ($imagesOriginal as $item) {

                $images[] = WrapperBridge::toObject($item);
            }
        }

        // Videos
        $videos = null;
        $videosOriginal = $data->getVideos();

        if (is_array($videosOriginal)) {

            $videos = [];

            foreach ($videosOriginal as $item) {

                $videos[] = WrapperBridge::toObject($item);
            }
        }

        // Html5MediaBundles
        $html5MediaBundles = null;
        $html5MediaBundlesOriginal = $data->getHtml5MediaBundles();

        if (is_array($html5MediaBundlesOriginal)) {

            $html5MediaBundles = [];

            foreach ($html5MediaBundlesOriginal as $item) {

                $html5MediaBundles[] = WrapperBridge::toObject($item);
            }
        }

        $mandatoryAdText = $data->getMandatoryAdText();

        $ad = AdWrapper::toObject($data);

        $ad->images = $images;
        $ad->videos = $videos;
        $ad->bidCeiling = $headlines;
        $ad->descriptions = $descriptions;
        $ad->html5MediaBundles = $html5MediaBundles;

        $ad->mandatoryAdText = WrapperBridge::toObject($mandatoryAdText);

        return $ad;
    }
}
