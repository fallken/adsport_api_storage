<?php
/**
 * Created by PhpStorm.
 * User: Arsalani
 * Date: 3/26/2019
 * Time: 3:06 PM
 */

namespace App\repos;

use GuzzleHttp\Client;
use App\interfaces\CurrencyServiceInterface;

class CurrencyRepository implements CurrencyServiceInterface {

    protected $guzzle;
    protected $header = ['Content-type' => 'application/json-patch+json', 'accept' => 'application/json'];

    public function __construct()
    {
        $this->guzzle = new Client();
    }

    /**
     * @return array|\Illuminate\Http\JsonResponse|mixed
     * desc:return the current price of the currencies
     */
    public function getCurrencies() {
        $today=date('YmdH');
        $now=date('YmdHis',time());
        $random_str=str_random(20);
        $url = "http://call2.tgju.org/ajax.json?$today-$now-$random_str";
        $response = $this->guzzle->get($url);

        if ($response->getStatusCode() == 200):
            $jsonData = json_decode($response->getBody()->getContents(),true);
            $data =  $this->filterCurrencyResults($jsonData);
            return $data;
        else:
            return response()->json('the requesting end point has been resetted',500);
        endif;
    }


    /**
     * private function used for sending post requests to the end point using the guzzle\http
     *
     * @param $uri
     * @param $params
     * @param string $method
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendRequest($uri, $params, $method = 'Post') {
        $response = $this->guzzle->request($method, $uri, $params);
        return $response;
    }

    /**
     * @param $jsonArray
     * @return array
     */
    private function filterCurrencyResults($jsonArray) {//filtering the array so that the result will be shorter be easier to handle in the client side.
        $keys=array_keys($jsonArray['current']);
        $result = array_filter($keys,function($key){
            if(stripos($key,'sana_sell') !== false){
                return $key;
            }
        });
        $response =  array_intersect_key($jsonArray['current'],array_flip($result));
        $finalResult=[];
           foreach($response as $key=>$value)://removing the extra sana_sell from the begining of the key
               $finalResult[strtoupper(substr($key,10,10))]=$value['p'];
               endforeach;
        return $finalResult;
    }
}